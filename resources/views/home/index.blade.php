<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    @include('layouts.header')

    <body class="home page-template-default page page-id-2274 theme-engitech woocommerce-no-js royal_preloader woocommerce-active elementor-default elementor-kit-2821 elementor-page elementor-page-2274 engitech-theme-ver-1.0.6.2 wordpress-version-5.5.3">
        <div id="royal_preloader" data-width="185" data-height="90" data-url="http://127.0.0.1:8000/wp-content/uploads/sites/logo/Logo_671x3634_final.svg" data-color="#0a0f2b" data-bgcolor="#fff"></div>
        <div id="page" class="site">
            
            @include('layouts.header-menus')

            @include('layouts.site-panel')            

            <div id="content" class="site-content">
                <div data-elementor-type="wp-page" data-elementor-id="2274" class="elementor elementor-2274" data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-367e59d ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="367e59d"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-19b873b ot-flex-column-vertical" data-id="19b873b" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-a50a81d elementor-widget elementor-widget-slider_revolution"
                                                        data-id="a50a81d"
                                                        data-element_type="widget"
                                                        data-widget_type="slider_revolution.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="wp-block-themepunch-revslider">
                                                                <!-- START Home 1 REVOLUTION SLIDER 6.2.23 -->
                                                                <p class="rs-p-wp-fix"></p>
                                                                <rs-module-wrap id="rev_slider_2_1_wrapper" data-source="gallery" style="background: transparent; padding: 0; margin: 0px auto; margin-top: 0; margin-bottom: 0;">
                                                                    <rs-module id="rev_slider_2_1" style="" data-version="6.2.23">
                                                                        <rs-slides>
                                                                            <rs-slide
                                                                                data-key="rs-4"
                                                                                data-title="Slide"
                                                                                data-thumb="//wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/slide1-home1-50x100.jpg"
                                                                                data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;"
                                                                            >
                                                                                <img
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201920%20809'%3E%3C/svg%3E"
                                                                                    title="slide1-home1"
                                                                                    width="1920"
                                                                                    height="809"
                                                                                    class="rev-slidebg"
                                                                                    data-no-retina
                                                                                    data-lazy-src="wp-content/uploads/sites/4/2020/03/slide1-home1.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img src="wp-content/uploads/sites/4/2020/03/slide1-home1.jpg" title="slide1-home1" width="1920" height="809" class="rev-slidebg" data-no-retina />
                                                                                </noscript>
                                                                                <!--
							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-4-layer-0"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:240px,140px,155px,145px;"
                                                                                    data-text="w:normal;s:72,60,48,30;l:80,62,52,42;fw:900;"
                                                                                    data-frame_0="o:1;"
                                                                                    data-frame_0_chars="x:105%;o:1;rY:45deg;rZ:90deg;"
                                                                                    data-frame_0_mask="u:t;"
                                                                                    data-frame_1="st:500;sp:1000;"
                                                                                    data-frame_1_chars="e:power4.inOut;d:10;rZ:0deg;"
                                                                                    data-frame_1_mask="u:t;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7500;"
                                                                                    style="z-index: 9; font-family: Montserrat;"
                                                                                >
                                                                                    FROM IDEA<br />
                                                                                    TO PRODUCT
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-4-layer-1"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:420px,279px,271px,235px;"
                                                                                    data-text="w:normal;s:18,18,22,16;l:30,34,32,28;"
                                                                                    data-frame_0="x:50;"
                                                                                    data-frame_1="st:2900;sp:1000;sR:1000;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7000;"
                                                                                    style="z-index: 10; font-family: Nunito Sans;"
                                                                                >
                                                                                    We are 100+ professional software engineers with more than <br />
                                                                                    10 years of experience in delivering superior products.
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-4-layer-9"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:195px,105px,127px,133px;"
                                                                                    data-text="w:normal;s:24,20,15,15;l:36,30,22,13;"
                                                                                    data-vbility="t,t,f,f"
                                                                                    data-frame_0="x:50,42,31,19;"
                                                                                    data-frame_1="sp:1000;"
                                                                                    data-frame_999="o:0;st:w;sR:8000;"
                                                                                    style="z-index: 8; font-family: Nunito Sans;"
                                                                                >
                                                                                    // Full Cycle Software Development
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <a
                                                                                    id="slider-2-slide-4-layer-10"
                                                                                    class="rs-layer rev-btn"
                                                                                    href="#"
                                                                                    target="_self"
                                                                                    rel="nofollow"
                                                                                    data-type="button"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:525px,385px,370px,320px;"
                                                                                    data-text="w:normal;s:14,14,14,12;l:18,18,18,16;fw:700;"
                                                                                    data-dim="minh:0px,none,none,none;"
                                                                                    data-padding="t:20,17,15,13;r:35,30,27,22;b:20,17,15,13;l:35,30,27,22;"
                                                                                    data-frame_0="x:50,42,31,19;"
                                                                                    data-frame_1="st:3400;sR:1500;"
                                                                                    data-frame_999="o:0;st:w;sR:6500;"
                                                                                    data-frame_hover="bgc:#7141b1;bor:0px,0px,0px,0px;sp:200;e:power1.inOut;"
                                                                                    style="z-index: 11; background-color: #43baff; font-family: Nunito Sans;"
                                                                                >
                                                                                    LEARN MORE
                                                                                </a>
                                                                                <!--
-->
                                                                            </rs-slide>
                                                                            <rs-slide
                                                                                data-key="rs-5"
                                                                                data-title="Slide"
                                                                                data-thumb="//wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/slide2-home1-50x100.jpg"
                                                                                data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;"
                                                                            >
                                                                                <img
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201920%20809'%3E%3C/svg%3E"
                                                                                    title="slide2-home1"
                                                                                    width="1920"
                                                                                    height="809"
                                                                                    class="rev-slidebg"
                                                                                    data-no-retina
                                                                                    data-lazy-src="wp-content/uploads/sites/4/2020/03/slide2-home1.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img src="wp-content/uploads/sites/4/2020/03/slide2-home1.jpg" title="slide2-home1" width="1920" height="809" class="rev-slidebg" data-no-retina />
                                                                                </noscript>
                                                                                <!--
							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-5-layer-0"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:240px,140px,155px,145px;"
                                                                                    data-text="w:normal;s:72,60,48,30;l:80,62,52,42;fw:900;"
                                                                                    data-frame_0="o:1;"
                                                                                    data-frame_0_chars="y:100%;o:0;rZ:-35deg;"
                                                                                    data-frame_0_mask="u:t;"
                                                                                    data-frame_1="st:500;sp:1200;"
                                                                                    data-frame_1_chars="e:power4.inOut;d:10;rZ:0deg;"
                                                                                    data-frame_1_mask="u:t;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7500;"
                                                                                    style="z-index: 9; font-family: Montserrat;"
                                                                                >
                                                                                    END-TO-END <br />
                                                                                    DEVELOPMENT
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-5-layer-1"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:420px,279px,272px,235px;"
                                                                                    data-text="w:normal;s:18,18,22,16;l:30,34,32,28;"
                                                                                    data-frame_0="y:50;"
                                                                                    data-frame_1="st:2900;sp:1000;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7000;"
                                                                                    style="z-index: 10; font-family: Nunito Sans;"
                                                                                >
                                                                                    We are 100+ professional software engineers with more than <br />
                                                                                    10 years of experience in delivering superior products.
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-5-layer-9"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:195px,105px,127px,133px;"
                                                                                    data-text="w:normal;s:24,20,15,15;l:36,30,22,13;"
                                                                                    data-vbility="t,t,f,f"
                                                                                    data-frame_0="y:50,42,31,19;"
                                                                                    data-frame_1="sp:1000;"
                                                                                    data-frame_999="o:0;st:w;sR:8000;"
                                                                                    style="z-index: 8; font-family: Nunito Sans;"
                                                                                >
                                                                                    // We Create Leading Digital Products
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <a
                                                                                    id="slider-2-slide-5-layer-10"
                                                                                    class="rs-layer rev-btn"
                                                                                    href="#"
                                                                                    target="_self"
                                                                                    rel="nofollow"
                                                                                    data-type="button"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:525px,385px,370px,320px;"
                                                                                    data-text="w:normal;s:14,14,14,12;l:18,18,18,16;fw:700;"
                                                                                    data-dim="minh:0px,none,none,none;"
                                                                                    data-padding="t:20,17,15,13;r:35,30,27,22;b:20,17,15,13;l:35,30,27,22;"
                                                                                    data-frame_0="y:50,42,31,19;"
                                                                                    data-frame_1="st:3400;sp:500;"
                                                                                    data-frame_999="o:0;st:w;sR:6500;"
                                                                                    data-frame_hover="bgc:#7141b1;bor:0px,0px,0px,0px;sp:200;e:power1.inOut;"
                                                                                    style="z-index: 11; background-color: #43baff; font-family: Nunito Sans;"
                                                                                >
                                                                                    LEARN MORE
                                                                                </a>
                                                                                <!--
-->
                                                                            </rs-slide>
                                                                            <rs-slide
                                                                                data-key="rs-6"
                                                                                data-title="Slide"
                                                                                data-thumb="//wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/slide3-home1-50x100.jpg"
                                                                                data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;"
                                                                            >
                                                                                <img
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201920%20809'%3E%3C/svg%3E"
                                                                                    title="slide3-home1"
                                                                                    width="1920"
                                                                                    height="809"
                                                                                    class="rev-slidebg"
                                                                                    data-no-retina
                                                                                    data-lazy-src="wp-content/uploads/sites/4/2020/03/slide3-home1.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img src="wp-content/uploads/sites/4/2020/03/slide3-home1.jpg" title="slide3-home1" width="1920" height="809" class="rev-slidebg" data-no-retina />
                                                                                </noscript>
                                                                                <!--
							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-6-layer-0"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:240px,140px,155px,145px;"
                                                                                    data-text="w:normal;s:72,60,48,30;l:80,62,52,42;fw:900;"
                                                                                    data-frame_0="o:1;"
                                                                                    data-frame_0_chars="y:-100%;o:0;rZ:35deg;"
                                                                                    data-frame_0_mask="u:t;"
                                                                                    data-frame_1="st:500;sp:1200;"
                                                                                    data-frame_1_chars="e:power4.inOut;d:10;rZ:0deg;"
                                                                                    data-frame_1_mask="u:t;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7500;"
                                                                                    style="z-index: 9; font-family: Montserrat;"
                                                                                >
                                                                                    SOFTWARE IT <br />
                                                                                    OUTSOURCING
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-6-layer-1"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:420px,279px,271px,235px;"
                                                                                    data-text="w:normal;s:18,18,22,16;l:30,34,32,28;"
                                                                                    data-frame_0="y:-50,-42,-31,-19;"
                                                                                    data-frame_1="st:2900;sp:1000;"
                                                                                    data-frame_999="o:0;e:power4.inOut;st:w;sp:1000;sR:7000;"
                                                                                    style="z-index: 10; font-family: Nunito Sans;"
                                                                                >
                                                                                    We are 100+ professional software engineers with more than <br />
                                                                                    10 years of experience in delivering superior products.
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <rs-layer
                                                                                    id="slider-2-slide-6-layer-9"
                                                                                    data-type="text"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:195px,105px,127px,133px;"
                                                                                    data-text="w:normal;s:24,20,15,15;l:36,30,22,13;"
                                                                                    data-vbility="t,t,f,f"
                                                                                    data-frame_0="y:-50,-42,-31,-19;"
                                                                                    data-frame_1="st:500;sp:1000;"
                                                                                    data-frame_999="o:0;st:w;sR:8000;"
                                                                                    style="z-index: 8; font-family: Nunito Sans;"
                                                                                >
                                                                                    // Only High-Quality Services<br />
                                                                                </rs-layer>
                                                                                <!--

							-->
                                                                                <a
                                                                                    id="slider-2-slide-6-layer-10"
                                                                                    class="rs-layer rev-btn"
                                                                                    href="#"
                                                                                    target="_self"
                                                                                    rel="nofollow"
                                                                                    data-type="button"
                                                                                    data-rsp_ch="on"
                                                                                    data-xy="xo:15px;yo:525px,385px,370px,320px;"
                                                                                    data-text="w:normal;s:14,14,14,12;l:18,18,18,16;fw:700;"
                                                                                    data-dim="minh:0px,none,none,none;"
                                                                                    data-padding="t:20,20,15,13;r:35,35,27,22;b:20,20,15,13;l:35,35,27,22;"
                                                                                    data-frame_0="y:-50;"
                                                                                    data-frame_1="st:3400;sp:700;"
                                                                                    data-frame_999="o:0;st:w;sR:6500;"
                                                                                    data-frame_hover="bgc:#7141b1;bor:0px,0px,0px,0px;sp:200;e:power1.inOut;"
                                                                                    style="z-index: 11; background-color: #43baff; font-family: Nunito Sans;"
                                                                                >
                                                                                    LEARN MORE
                                                                                </a>
                                                                                <!--
-->
                                                                            </rs-slide>
                                                                        </rs-slides>
                                                                        <rs-static-layers>
                                                                            <!--
					-->
                                                                        </rs-static-layers>
                                                                    </rs-module>
                                                                    <script type="text/javascript">
                                                                        setREVStartSize({
                                                                            c: "rev_slider_2_1",
                                                                            rl: [1240, 1024, 778, 480],
                                                                            el: [809, 600, 630, 600],
                                                                            gw: [1200, 1024, 768, 480],
                                                                            gh: [809, 600, 630, 600],
                                                                            type: "standard",
                                                                            justify: "",
                                                                            layout: "fullwidth",
                                                                            mh: "0",
                                                                        });
                                                                        var revapi2, tpj;
                                                                        function revinit_revslider21() {
                                                                            jQuery(function () {
                                                                                tpj = jQuery;
                                                                                revapi2 = tpj("#rev_slider_2_1");
                                                                                if (revapi2 == undefined || revapi2.revolution == undefined) {
                                                                                    revslider_showDoubleJqueryError("rev_slider_2_1");
                                                                                } else {
                                                                                    revapi2.revolution({
                                                                                        sliderLayout: "fullwidth",
                                                                                        visibilityLevels: "1240,1024,778,480",
                                                                                        gridwidth: "1200,1024,768,480",
                                                                                        gridheight: "809,600,630,600",
                                                                                        spinner: "spinner0",
                                                                                        perspective: 600,
                                                                                        perspectiveType: "local",
                                                                                        editorheight: "809,600,630,600",
                                                                                        responsiveLevels: "1240,1024,778,480",
                                                                                        progressBar: { disableProgressBar: true },
                                                                                        navigation: {
                                                                                            onHoverStop: false,
                                                                                            arrows: {
                                                                                                enable: true,
                                                                                                style: "nav-home-1",
                                                                                                left: {
                                                                                                    container: "layergrid",
                                                                                                    v_align: "bottom",
                                                                                                    h_offset: 0,
                                                                                                    v_offset: 60,
                                                                                                },
                                                                                                right: {
                                                                                                    container: "layergrid",
                                                                                                    h_align: "left",
                                                                                                    v_align: "bottom",
                                                                                                    h_offset: 100,
                                                                                                    v_offset: 60,
                                                                                                },
                                                                                            },
                                                                                            bullets: {
                                                                                                enable: true,
                                                                                                tmp: "",
                                                                                                style: "bullet-home-1",
                                                                                                h_align: "left",
                                                                                                h_offset: 45,
                                                                                                v_offset: 70,
                                                                                                container: "layergrid",
                                                                                            },
                                                                                        },
                                                                                        fallbacks: {
                                                                                            allowHTML5AutoPlayOnAndroid: true,
                                                                                        },
                                                                                    });
                                                                                }
                                                                            });
                                                                        } // End of RevInitScript
                                                                        var once_revslider21 = false;
                                                                        if (document.readyState === "loading") {
                                                                            document.addEventListener("readystatechange", function () {
                                                                                if ((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider21) {
                                                                                    once_revslider21 = true;
                                                                                    revinit_revslider21();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            once_revslider21 = true;
                                                                            revinit_revslider21();
                                                                        }
                                                                    </script>
                                                                </rs-module-wrap>
                                                                <!-- END REVOLUTION SLIDER -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-9f835f7 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="9f835f7"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28a108b ot-flex-column-vertical" data-id="28a108b" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-f0ff748 partners elementor-widget elementor-widget-image-carousel"
                                                        data-id="f0ff748"
                                                        data-element_type="widget"
                                                        data-settings='{"slides_to_show":"6","slides_to_show_tablet":"4","slides_to_show_mobile":"2","slides_to_scroll":"1","navigation":"none","autoplay_speed":6000,"image_spacing_custom":{"unit":"px","size":80,"sizes":[]},"autoplay":"yes","pause_on_hover":"yes","pause_on_interaction":"yes","infinite":"yes","speed":500}'
                                                        data-widget_type="image-carousel.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
                                                                <div class="elementor-image-carousel swiper-wrapper">
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client1"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client1"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client1.svg"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client1.svg" alt="client1" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client2"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client2"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client2.svg"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client2.svg" alt="client2" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client3"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client3"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client3.svg"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client3.svg" alt="client3" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client4"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client4"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client4.png"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client4.png" alt="client4" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client5"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client5"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client5.svg"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client5.svg" alt="client5" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="swiper-slide">
                                                                        <a
                                                                            data-elementor-open-lightbox="yes"
                                                                            data-elementor-lightbox-slideshow="f0ff748"
                                                                            data-elementor-lightbox-title="client6"
                                                                            href="http://themeforest.net/user/OceanThemes/portfolio"
                                                                            target="_blank"
                                                                        >
                                                                            <figure class="swiper-slide-inner">
                                                                                <img
                                                                                    class="swiper-slide-image"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                    alt="client6"
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/client6.svg"
                                                                                />
                                                                                <noscript><img class="swiper-slide-image" src="wp-content/uploads/sites/4/2020/03/client6.svg" alt="client6" /></noscript>
                                                                            </figure>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-aa4b47d ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="aa4b47d"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5e7b92b ot-flex-column-vertical" data-id="5e7b92b" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-55cca91 elementor-widget elementor-widget-iheading" data-id="55cca91" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// about company</span>
                                                                <h2 class="main-heading">
                                                                    Your Partner for <br />
                                                                    Software Innovation
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-72485af elementor-widget elementor-widget-text-editor" data-id="72485af" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                <p>
                                                                    Engitech is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses elevate their value through custom
                                                                    software development, product design, QA and consultancy services.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-787e48b elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="787e48b"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-092cc00 ot-flex-column-vertical"
                                                                    data-id="092cc00"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-c32c536 elementor-widget elementor-widget-iiconbox1"
                                                                                data-id="c32c536"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-medal"></span>
                                                                                        </div>
                                                                                        <h5>Experience</h5>
                                                                                        <div class="line-box"></div>
                                                                                        <p>Our great team of more than 1400 software experts.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-86fb9c9 ot-flex-column-vertical"
                                                                    data-id="86fb9c9"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-21549ac elementor-widget elementor-widget-iiconbox1"
                                                                                data-id="21549ac"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-gear"></span>
                                                                                        </div>
                                                                                        <h5>Quick Support</h5>
                                                                                        <div class="line-box"></div>
                                                                                        <p>We’ll help you test bold new ideas while sharing your.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <div
                                                        class="elementor-element elementor-element-b7a6382 elementor-hidden-desktop elementor-widget elementor-widget-image"
                                                        data-id="b7a6382"
                                                        data-element_type="widget"
                                                        data-widget_type="image.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="952"
                                                                    height="571"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20952%20571'%3E%3C/svg%3E"
                                                                    class="attachment-full size-full"
                                                                    alt=""
                                                                    data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png 952w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-300x180.png 300w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-768x461.png 768w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-720x432.png 720w"
                                                                    data-lazy-sizes="(max-width: 952px) 100vw, 952px"
                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png"
                                                                />
                                                                <noscript>
                                                                    <img
                                                                        width="952"
                                                                        height="571"
                                                                        src="wp-content/uploads/sites/4/2020/02/image1-home1.png"
                                                                        class="attachment-full size-full"
                                                                        alt=""
                                                                        srcset="
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png         952w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-300x180.png 300w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-768x461.png 768w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-720x432.png 720w
                                                                        "
                                                                        sizes="(max-width: 952px) 100vw, 952px"
                                                                    />
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-8243aef elementor-hidden-tablet elementor-hidden-phone ot-flex-column-vertical"
                                            data-id="8243aef"
                                            data-element_type="column"
                                            data-settings='{"background_background":"classic"}'
                                        >
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-0f677d2 elementor-widget elementor-widget-image" data-id="0f677d2" data-element_type="widget" data-widget_type="image.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="952"
                                                                    height="571"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20952%20571'%3E%3C/svg%3E"
                                                                    class="attachment-full size-full"
                                                                    alt=""
                                                                    data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png 952w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-300x180.png 300w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-768x461.png 768w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-720x432.png 720w"
                                                                    data-lazy-sizes="(max-width: 952px) 100vw, 952px"
                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png"
                                                                />
                                                                <noscript>
                                                                    <img
                                                                        width="952"
                                                                        height="571"
                                                                        src="wp-content/uploads/sites/4/2020/02/image1-home1.png"
                                                                        class="attachment-full size-full"
                                                                        alt=""
                                                                        srcset="
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1.png         952w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-300x180.png 300w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-768x461.png 768w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-home1-720x432.png 720w
                                                                        "
                                                                        sizes="(max-width: 952px) 100vw, 952px"
                                                                    />
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-c22dee4 elementor-align-left elementor-widget elementor-widget-ibutton"
                                                        data-id="c22dee4"
                                                        data-element_type="widget"
                                                        data-widget_type="ibutton.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-button">
                                                                <a href="about-us/index.html" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEANR MORE ABOUT US</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-9a4c200 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="9a4c200"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3fcfb9c ot-flex-column-vertical" data-id="3fcfb9c" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-8e77825 elementor-widget elementor-widget-iheading" data-id="8e77825" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// why choose us</span>
                                                                <h2 class="main-heading">
                                                                    Design the Concept <br />
                                                                    of Your Business Idea Now
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-8da485e elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="8da485e"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-0996c0e ot-flex-column-vertical"
                                                                    data-id="0996c0e"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-a66114a elementor-widget elementor-widget-iserv_box_2"
                                                                                data-id="a66114a"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iserv_box_2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="serv-box-2 s2">
                                                                                        <span class="big-number">01</span>
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-tablet"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Product Design</h5>
                                                                                            <div>Our product design service lets you prototype, test and validate your ideas.</div>
                                                                                            <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-1e53f2a elementor-hidden-desktop elementor-widget elementor-widget-spacer"
                                                                                data-id="1e53f2a"
                                                                                data-element_type="widget"
                                                                                data-widget_type="spacer.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-spacer">
                                                                                        <div class="elementor-spacer-inner"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8317b24 ot-flex-column-vertical"
                                                                    data-id="8317b24"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-bca1ad3 elementor-widget elementor-widget-iserv_box_2"
                                                                                data-id="bca1ad3"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iserv_box_2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="serv-box-2 s2">
                                                                                        <span class="big-number">02</span>
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-ui"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Development</h5>
                                                                                            <div>Our product design service lets you prototype, test and validate your ideas.</div>
                                                                                            <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-6f8a155 elementor-hidden-desktop elementor-widget elementor-widget-spacer"
                                                                                data-id="6f8a155"
                                                                                data-element_type="widget"
                                                                                data-widget_type="spacer.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-spacer">
                                                                                        <div class="elementor-spacer-inner"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-ede4b1a ot-flex-column-vertical"
                                                                    data-id="ede4b1a"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-a48349e elementor-widget elementor-widget-iserv_box_2"
                                                                                data-id="a48349e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iserv_box_2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="serv-box-2 s2">
                                                                                        <span class="big-number">03</span>
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-report"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Data Analytics</h5>
                                                                                            <div>Our product design service lets you prototype, test and validate your ideas.</div>
                                                                                            <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-3645c63 elementor-hidden-desktop elementor-hidden-tablet elementor-widget elementor-widget-spacer"
                                                                                data-id="3645c63"
                                                                                data-element_type="widget"
                                                                                data-widget_type="spacer.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-spacer">
                                                                                        <div class="elementor-spacer-inner"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-62714c5 ot-flex-column-vertical"
                                                                    data-id="62714c5"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-5f1c553 elementor-widget elementor-widget-iserv_box_2"
                                                                                data-id="5f1c553"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iserv_box_2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="serv-box-2 s2">
                                                                                        <span class="big-number">04</span>
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-shield"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Cyber Security</h5>
                                                                                            <div>Our product design service lets you prototype, test and validate your ideas.</div>
                                                                                            <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-c298a16 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="c298a16"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5071529 ot-flex-column-vertical" data-id="5071529" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-9a579c8 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="9a579c8"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-60959ce ot-flex-column-vertical"
                                                                    data-id="60959ce"
                                                                    data-element_type="column"
                                                                    data-settings='{"background_background":"classic"}'
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-144263d elementor-widget elementor-widget-icounter"
                                                                                data-id="144263d"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="15" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-e2b4872 elementor-widget elementor-widget-heading"
                                                                                data-id="e2b4872"
                                                                                data-element_type="widget"
                                                                                data-widget_type="heading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <h5 class="elementor-heading-title elementor-size-default">Countries Worldwide</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-461aff6 elementor-widget elementor-widget-text-editor"
                                                                                data-id="461aff6"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        <p>To succeed, every software solution must be deeply integrated into the existing tech environment..</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-726795a ot-flex-column-vertical"
                                                                    data-id="726795a"
                                                                    data-element_type="column"
                                                                    data-settings='{"background_background":"classic"}'
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-6e25e6b elementor-widget elementor-widget-icounter"
                                                                                data-id="6e25e6b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="23" data-time="2000"></span>
                                                                                            <span>k</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-6a1620e elementor-widget elementor-widget-heading"
                                                                                data-id="6a1620e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="heading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <h5 class="elementor-heading-title elementor-size-default">Happy Customers</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-82731d0 elementor-widget elementor-widget-text-editor"
                                                                                data-id="82731d0"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        <p>To succeed, every software solution must be deeply integrated into the existing tech environment..</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-cfe2524 elementor-section-full_width elementor-section-content-bottom ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="cfe2524"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-default">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d8098f2 ot-flex-column-vertical"
                                                                    data-id="d8098f2"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-b7fef36 elementor-widget elementor-widget-iheading"
                                                                                data-id="b7fef36"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>// our service</span>
                                                                                        <h2 class="main-heading">
                                                                                            We Offer a Wide <br />
                                                                                            Variety of IT Services
                                                                                        </h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-db1c739 ot-flex-column-vertical"
                                                                    data-id="db1c739"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3302e8b elementor-align-right elementor-mobile-align-left elementor-widget elementor-widget-ibutton"
                                                                                data-id="3302e8b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="ibutton.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-button">
                                                                                        <a href="it-services/index.html" class="octf-btn octf-btn-primary">all sevices</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-76a3b4e elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="76a3b4e"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-80c07a4 ot-flex-column-vertical"
                                                                    data-id="80c07a4"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-80a0c6e elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="80a0c6e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-code"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Web Development</h5>
                                                                                            <p>We carry more than just good coding skills. Our experience makes us stand out from other web development.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-176403e elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="176403e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-browser"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>QA & Testing</h5>
                                                                                            <p>Turn to our experts to perform comprehensive, multi-stage testing and auditing of your software.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-374e026 ot-flex-column-vertical"
                                                                    data-id="374e026"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-c5bb4ce elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="c5bb4ce"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-app"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Mobile Development</h5>
                                                                                            <p>Create complex enterprise software, ensure reliable software integration, modernise your legacy system.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-22ab363 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="22ab363"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-report-1"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>IT Counsultancy</h5>
                                                                                            <p>Trust our top minds to eliminate workflow pain points, implement new tech, and consolidate app portfolios.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5b36c35 elementor-hidden-tablet ot-flex-column-vertical"
                                                                    data-id="5b36c35"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-bfb0c64 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="bfb0c64"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-monitor"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>UI/UX Design</h5>
                                                                                            <p>Build the product you need on time with an experienced team that uses a clear and effective design process.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-6ff21fa elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="6ff21fa"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-best"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Dedicated Team</h5>
                                                                                            <p>Over the past decade, our customers succeeded by leveraging Intellectsoft’s process of building, motivating.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-afa05b9 elementor-section-full_width elementor-hidden-desktop elementor-hidden-phone ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="afa05b9"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-283a38e ot-flex-column-vertical"
                                                                    data-id="283a38e"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-7f3432e elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="7f3432e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-monitor"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>UI/UX Design</h5>
                                                                                            <p>Build the product you need on time with an experienced team that uses a clear and effective design process.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9a4df92 ot-flex-column-vertical"
                                                                    data-id="9a4df92"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-6b3ac17 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="6b3ac17"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-best"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Dedicated Team</h5>
                                                                                            <p>Build the product you need on time with an experienced team that uses a clear and effective design process.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-6bcc921 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="6bcc921"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5dac1ca ot-flex-column-vertical" data-id="5dac1ca" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-f8ce257 elementor-section-full_width elementor-section-content-bottom ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="f8ce257"
                                                        data-element_type="section"
                                                        data-settings='{"background_background":"classic"}'
                                                    >
                                                        <div class="elementor-container elementor-column-gap-default">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-21c209f ot-flex-column-vertical"
                                                                    data-id="21c209f"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-1213f99 elementor-widget elementor-widget-iheading"
                                                                                data-id="1213f99"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>// We Carry more Than Just Good Coding Skills</span>
                                                                                        <h2 class="main-heading">Let's Build Your Website!</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0ad2d07 ot-flex-column-vertical"
                                                                    data-id="0ad2d07"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-2e315e6 elementor-align-right elementor-mobile-align-left elementor-widget elementor-widget-button"
                                                                                data-id="2e315e6"
                                                                                data-element_type="widget"
                                                                                data-widget_type="button.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-button-wrapper">
                                                                                        <a href="contacts/index.html" class="elementor-button-link elementor-button elementor-size-md" role="button">
                                                                                            <span class="elementor-button-content-wrapper">
                                                                                                <span class="elementor-button-text">contact us</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-7d3a849 elementor-section-full_width elementor-section-content-bottom ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="7d3a849"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5ef010f ot-flex-column-vertical"
                                                                    data-id="5ef010f"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3f51e83 elementor-widget elementor-widget-iheading"
                                                                                data-id="3f51e83"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>// latest case studies</span>
                                                                                        <h2 class="main-heading">Introduce Our Projects</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2b9ac69 ot-flex-column-vertical"
                                                                    data-id="2b9ac69"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-cd92efb elementor-widget elementor-widget-text-editor"
                                                                                data-id="cd92efb"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        Software development outsourcing is just a tool to achieve business goals. But there is no way to get worthwhile results without cooperation and trust
                                                                                        between a client company.
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-8ccf77c elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                data-id="8ccf77c"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6f2d0db ot-flex-column-vertical" data-id="6f2d0db" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-9f67c6f elementor-widget elementor-widget-irprojects" data-id="9f67c6f" data-element_type="widget" data-widget_type="irprojects.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="project-slider" data-center="true" data-show="2" data-scroll="2" data-arrow="false" data-dots="true">
                                                                <div class="project-item projects-style-2">
                                                                    <div class="projects-box">
                                                                        <div class="projects-thumbnail">
                                                                            <a href="portfolio/app-for-virtual-reality/index.html">
                                                                                <img
                                                                                    width="720"
                                                                                    height="520"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                    class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                    alt=""
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-720x520.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img
                                                                                        width="720"
                                                                                        height="520"
                                                                                        src="wp-content/uploads/sites/4/2019/11/project1-720x520.jpg"
                                                                                        class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                        alt=""
                                                                                    />
                                                                                </noscript>
                                                                                <span class="overlay"></span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="portfolio-info">
                                                                            <div class="portfolio-info-inner">
                                                                                <a class="btn-link" href="portfolio/app-for-virtual-reality/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                <h5><a href="portfolio/app-for-virtual-reality/index.html">App for Virtual Reality</a></h5>
                                                                                <p class="portfolio-cates">
                                                                                    <a href="portfolio-cat/design/index.html">Design</a><span>/</span><a href="portfolio-cat/ideas/index.html">Ideas</a><span>/</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="project-item projects-style-2">
                                                                    <div class="projects-box">
                                                                        <div class="projects-thumbnail">
                                                                            <a href="portfolio/analysis-of-security/index.html">
                                                                                <img
                                                                                    width="720"
                                                                                    height="520"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                    class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                    alt=""
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project3-720x520.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img
                                                                                        width="720"
                                                                                        height="520"
                                                                                        src="wp-content/uploads/sites/4/2019/11/project3-720x520.jpg"
                                                                                        class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                        alt=""
                                                                                    />
                                                                                </noscript>
                                                                                <span class="overlay"></span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="portfolio-info">
                                                                            <div class="portfolio-info-inner">
                                                                                <a class="btn-link" href="portfolio/analysis-of-security/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                <h5><a href="portfolio/analysis-of-security/index.html">Analysis of Security</a></h5>
                                                                                <p class="portfolio-cates">
                                                                                    <a href="portfolio-cat/ideas/index.html">Ideas</a><span>/</span><a href="portfolio-cat/technology/index.html">Technology</a><span>/</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="project-item projects-style-2">
                                                                    <div class="projects-box">
                                                                        <div class="projects-thumbnail">
                                                                            <a href="portfolio/ecommerce-website/index.html">
                                                                                <img
                                                                                    width="720"
                                                                                    height="520"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                    class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                    alt=""
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-720x520.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img
                                                                                        width="720"
                                                                                        height="520"
                                                                                        src="wp-content/uploads/sites/4/2019/11/project4-720x520.jpg"
                                                                                        class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                        alt=""
                                                                                    />
                                                                                </noscript>
                                                                                <span class="overlay"></span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="portfolio-info">
                                                                            <div class="portfolio-info-inner">
                                                                                <a class="btn-link" href="portfolio/ecommerce-website/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                <h5><a href="portfolio/ecommerce-website/index.html">eCommerce Website</a></h5>
                                                                                <p class="portfolio-cates">
                                                                                    <a href="portfolio-cat/design/index.html">Design</a><span>/</span><a href="portfolio-cat/ideas/index.html">Ideas</a><span>/</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="project-item projects-style-2">
                                                                    <div class="projects-box">
                                                                        <div class="projects-thumbnail">
                                                                            <a href="portfolio/basics-project/index.html">
                                                                                <img
                                                                                    width="720"
                                                                                    height="520"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                    class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                    alt=""
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project8-720x520.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img
                                                                                        width="720"
                                                                                        height="520"
                                                                                        src="wp-content/uploads/sites/4/2019/11/project8-720x520.jpg"
                                                                                        class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                        alt=""
                                                                                    />
                                                                                </noscript>
                                                                                <span class="overlay"></span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="portfolio-info">
                                                                            <div class="portfolio-info-inner">
                                                                                <a class="btn-link" href="portfolio/basics-project/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                <h5><a href="portfolio/basics-project/index.html">Basics Project</a></h5>
                                                                                <p class="portfolio-cates">
                                                                                    <a href="portfolio-cat/design/index.html">Design</a><span>/</span><a href="portfolio-cat/development/index.html">Development</a><span>/</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="project-item projects-style-2">
                                                                    <div class="projects-box">
                                                                        <div class="projects-thumbnail">
                                                                            <a href="portfolio/social-media-app/index.html">
                                                                                <img
                                                                                    width="720"
                                                                                    height="520"
                                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                    class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                    alt=""
                                                                                    data-lazy-src="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project7-720x520.jpg"
                                                                                />
                                                                                <noscript>
                                                                                    <img
                                                                                        width="720"
                                                                                        height="520"
                                                                                        src="wp-content/uploads/sites/4/2019/11/project7-720x520.jpg"
                                                                                        class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                        alt=""
                                                                                    />
                                                                                </noscript>
                                                                                <span class="overlay"></span>
                                                                            </a>
                                                                        </div>
                                                                        <div class="portfolio-info">
                                                                            <div class="portfolio-info-inner">
                                                                                <a class="btn-link" href="portfolio/social-media-app/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                <h5><a href="portfolio/social-media-app/index.html">Social Media App</a></h5>
                                                                                <p class="portfolio-cates">
                                                                                    <a href="portfolio-cat/design/index.html">Design</a><span>/</span><a href="portfolio-cat/technology/index.html">Technology</a><span>/</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-1cd5b68 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="1cd5b68"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cbe9230 ot-flex-column-vertical" data-id="cbe9230" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-f51dec4 elementor-widget elementor-widget-iheading" data-id="f51dec4" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// TECHNOLOGY INDEX</span>
                                                                <h2 class="main-heading">
                                                                    We Deliver Solution with <br />
                                                                    the Goal of Trusting Relationships
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-af7edd2 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="af7edd2"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-b417c7d ot-flex-column-vertical"
                                                                    data-id="b417c7d"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-5e9ae08 elementor-widget elementor-widget-itechbox"
                                                                                data-id="5e9ae08"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#" target="_blank">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-code-1"></span>
                                                                                        </div>
                                                                                        <h5>WEB</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-0732a3d ot-flex-column-vertical"
                                                                    data-id="0732a3d"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-12b7dc9 elementor-widget elementor-widget-itechbox"
                                                                                data-id="12b7dc9"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-android"></span>
                                                                                        </div>
                                                                                        <h5>Android</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-14aaae0 ot-flex-column-vertical"
                                                                    data-id="14aaae0"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-9abd6b3 elementor-widget elementor-widget-itechbox"
                                                                                data-id="9abd6b3"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-apple"></span>
                                                                                        </div>
                                                                                        <h5>IOS</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-6f5c6bc ot-flex-column-vertical"
                                                                    data-id="6f5c6bc"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-ef02003 elementor-widget elementor-widget-itechbox"
                                                                                data-id="ef02003"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-iot"></span>
                                                                                        </div>
                                                                                        <h5>IOT</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-fa8400f ot-flex-column-vertical"
                                                                    data-id="fa8400f"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-2744146 elementor-widget elementor-widget-itechbox"
                                                                                data-id="2744146"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-time-and-date"></span>
                                                                                        </div>
                                                                                        <h5>Wearalables</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-3bcd13c ot-flex-column-vertical"
                                                                    data-id="3bcd13c"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-5a5d648 elementor-widget elementor-widget-itechbox"
                                                                                data-id="5a5d648"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-tv"></span>
                                                                                        </div>
                                                                                        <h5>TV</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-ec6ab31 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="ec6ab31"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c0d5ae5 ot-flex-column-vertical" data-id="c0d5ae5" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-0837d04 elementor-widget elementor-widget-iheading" data-id="0837d04" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// our clients</span>
                                                                <h2 class="main-heading">
                                                                    We are Trusted <br />
                                                                    15+ Countries Worldwide
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-e1304a5 elementor-widget elementor-widget-itestimonials"
                                                        data-id="e1304a5"
                                                        data-element_type="widget"
                                                        data-widget_type="itestimonials.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-testimonials">
                                                                <div class="testimonial-inner ot-testimonials-slider" data-show="2" data-scroll="1" data-dots="false" data-arrow="true">
                                                                    <div>
                                                                        <div class="testi-item">
                                                                            <div class="layer1"></div>
                                                                            <div class="layer2">
                                                                                <div class="t-head flex-middle">
                                                                                    <img
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                        alt="Emilia Clarke"
                                                                                        data-lazy-src="https://engitech.s3.amazonaws.com/images/testi1.png"
                                                                                    />
                                                                                    <noscript><img src="../../engitech.s3.amazonaws.com/images/testi1.png" alt="Emilia Clarke" /></noscript>
                                                                                    <div class="tinfo">
                                                                                        <h6>Moonkle LTD,</h6>
                                                                                        <span>Client of Company</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ttext">
                                                                                    "Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Patience. Infinite patience. No shortcuts. Even if the
                                                                                    client is being careless. The best part...always solving problems with great original ideas!."
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="testi-item">
                                                                            <div class="layer1"></div>
                                                                            <div class="layer2">
                                                                                <div class="t-head flex-middle">
                                                                                    <img
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                        alt="Emilia Clarke"
                                                                                        data-lazy-src="https://engitech.s3.amazonaws.com/images/testi2.png"
                                                                                    />
                                                                                    <noscript><img src="../../engitech.s3.amazonaws.com/images/testi2.png" alt="Emilia Clarke" /></noscript>
                                                                                    <div class="tinfo">
                                                                                        <h6>SoftTech,</h6>
                                                                                        <span>Manager of Company</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ttext">
                                                                                    "Patience. Infinite patience. No shortcuts. Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Even if the
                                                                                    client is being careless. The best part...always solving problems with great original ideas!."
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="testi-item">
                                                                            <div class="layer1"></div>
                                                                            <div class="layer2">
                                                                                <div class="t-head flex-middle">
                                                                                    <img
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                        alt="Emilia Clarke"
                                                                                        data-lazy-src="https://engitech.s3.amazonaws.com/images/testi1.png"
                                                                                    />
                                                                                    <noscript><img src="../../engitech.s3.amazonaws.com/images/testi1.png" alt="Emilia Clarke" /></noscript>
                                                                                    <div class="tinfo">
                                                                                        <h6>Moonkle LTD,</h6>
                                                                                        <span>Client of Company</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ttext">
                                                                                    "Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Patience. Infinite patience. No shortcuts. Even if the
                                                                                    client is being careless. The best part...always solving problems with great original ideas!."
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="testi-item">
                                                                            <div class="layer1"></div>
                                                                            <div class="layer2">
                                                                                <div class="t-head flex-middle">
                                                                                    <img
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                        alt="Emilia Clarke"
                                                                                        data-lazy-src="https://engitech.s3.amazonaws.com/images/testi2.png"
                                                                                    />
                                                                                    <noscript><img src="../../engitech.s3.amazonaws.com/images/testi2.png" alt="Emilia Clarke" /></noscript>
                                                                                    <div class="tinfo">
                                                                                        <h6>SoftTech,</h6>
                                                                                        <span>Manager of Company</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ttext">
                                                                                    "Patience. Infinite patience. No shortcuts. Very well thought out and articulate communication. Clear milestones, deadlines and fast work. Even if the
                                                                                    client is being careless. The best part...always solving problems with great original ideas!."
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #content -->
            @include('layouts.footer')
        </div>
        <!-- #page -->

        <a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>

        <script type="text/javascript" id="wc-add-to-cart-js-extra">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "View cart",
                cart_url: "http:\/\/wpdemo.archiwp.com\/engitech\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="woocommerce-js-extra">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/engitech\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>

        <script type="text/javascript" id="wc-cart-fragments-js-extra">
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                cart_hash_key: "wc_cart_hash_c8d57f1cd4add6020c9933c2e3c64aa9",
                fragment_name: "wc_fragments_c8d57f1cd4add6020c9933c2e3c64aa9",
                request_timeout: "5000",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                version: "3.0.13",
                is_static: false,
                legacyMode: { elementWrappers: true },
                urls: { assets: "http:\/\/wpdemo.archiwp.com\/engitech\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                },
                post: { id: 2274, title: "Engitech%20%E2%80%93%20IT%20Solutions%20Demo%20WordPress%20Theme", excerpt: "", featuredImage: false },
            };
        </script>

        <script>
            window.lazyLoadOptions = {
                elements_selector: "img[data-lazy-src],.rocket-lazyload",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_loaded: function (element) {
                    if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                        if (element.classList.contains("lazyloaded")) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                },
            };
            window.addEventListener(
                "LazyLoad::Initialized",
                function (e) {
                    var lazyLoadInstance = e.detail.instance;
                    if (window.MutationObserver) {
                        var observer = new MutationObserver(function (mutations) {
                            var image_count = 0;
                            var iframe_count = 0;
                            var rocketlazy_count = 0;
                            mutations.forEach(function (mutation) {
                                for (i = 0; i < mutation.addedNodes.length; i++) {
                                    if (typeof mutation.addedNodes[i].getElementsByTagName !== "function") {
                                        return;
                                    }
                                    if (typeof mutation.addedNodes[i].getElementsByClassName !== "function") {
                                        return;
                                    }
                                    images = mutation.addedNodes[i].getElementsByTagName("img");
                                    is_image = mutation.addedNodes[i].tagName == "IMG";
                                    iframes = mutation.addedNodes[i].getElementsByTagName("iframe");
                                    is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                                    rocket_lazy = mutation.addedNodes[i].getElementsByClassName("rocket-lazyload");
                                    image_count += images.length;
                                    iframe_count += iframes.length;
                                    rocketlazy_count += rocket_lazy.length;
                                    if (is_image) {
                                        image_count += 1;
                                    }
                                    if (is_iframe) {
                                        iframe_count += 1;
                                    }
                                }
                            });
                            if (image_count > 0 || iframe_count > 0 || rocketlazy_count > 0) {
                                lazyLoadInstance.update();
                            }
                        });
                        var b = document.getElementsByTagName("body")[0];
                        var config = { childList: !0, subtree: !0 };
                        observer.observe(b, config);
                    }
                },
                !1
            );
        </script>
        <script data-no-minify="1" async src="wp-content/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js"></script>
        <script>
            "use strict";
            var wprRemoveCPCSS = function wprRemoveCPCSS() {
                var elem;
                document.querySelector('link[data-rocket-async="style"][rel="preload"]') ? setTimeout(wprRemoveCPCSS, 200) : (elem = document.getElementById("rocket-critical-css")) && "remove" in elem && elem.remove();
            };
            window.addEventListener ? window.addEventListener("load", wprRemoveCPCSS) : window.attachEvent && window.attachEvent("onload", wprRemoveCPCSS);
        </script>
        <script src="wp-content/cache/min/4/e7fccc91f665b689cbcce686747ff182.js" data-minify="1" defer></script>
        <noscript>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100i%2C200%2C200i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CNunito%20Sans%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%20Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A900%7CNunito%20Sans%3A400%2C700%7CRoboto%3A400&amp;subset=latin%2Clatin-ext&amp;display=swap"
            />
            <link rel="stylesheet" href="wp-content/cache/min/4/c1a7eca309264addf3b314ff3f98e830.css" media="all" data-minify="1" />
            <link rel="stylesheet" id="woocommerce-smallscreen-css" href="wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen1c9b.css?ver=4.6.1" type="text/css" media="only screen and (max-width: 768px)" />
        </noscript>
    </body>
</html>
