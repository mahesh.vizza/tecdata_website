<!-- #site-header-open -->
<header id="site-header" class="site-header header-static">
    <!-- #header-desktop-open -->
    <div class="header-desktop">
        <div data-elementor-type="wp-post" data-elementor-id="2840" class="elementor elementor-2840" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-65c14b10 elementor-section-content-middle ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="65c14b10"
                        data-element_type="section"
                        data-settings='{"background_background":"classic"}'
                    >
                        <div class="elementor-container elementor-column-gap-extended">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-581decff ot-flex-column-vertical" data-id="581decff" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-6c62b145 elementor-shape-square elementor-grid-4 elementor-widget elementor-widget-social-icons"
                                                data-id="6c62b145"
                                                data-element_type="widget"
                                                data-widget_type="social-icons.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-social-icons-wrapper elementor-grid">
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-4bf2890" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Twitter</span>
                                                                <i class="fab fa-twitter"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-repeater-item-a79d483" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Facebook-f</span>
                                                                <i class="fab fa-facebook-f"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-linkedin-in elementor-repeater-item-e6c93ca" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Linkedin-in</span>
                                                                <i class="fab fa-linkedin-in"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-repeater-item-2faf370" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Instagram</span>
                                                                <i class="fab fa-instagram"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-1ca041d7 ot-column-items-center ot-flex-column-vertical" data-id="1ca041d7" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-28dcb03 elementor-icon-list--layout-inline elementor-align-right elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list"
                                                data-id="28dcb03"
                                                data-element_type="widget"
                                                data-widget_type="icon-list.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <ul class="elementor-icon-list-items elementor-inline-items">
                                                        <li class="elementor-icon-list-item elementor-inline-item">
                                                            <span class="elementor-icon-list-text"></span>
                                                        </li>
                                                        <li class="elementor-icon-list-item elementor-inline-item">
                                                            <a href="tel:+1-800-456-478-23">
                                                                <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="fas fa-phone-alt"></i> </span>
                                                                <span class="elementor-icon-list-text">+1-800-456-478-23</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item elementor-inline-item">
                                                            <a href="mailto:engitech@mail.com">
                                                                <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="fas fa-envelope"></i> </span>
                                                                <span class="elementor-icon-list-text">tecdata@mail.com</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-4994a43 is-fixed ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="4994a43"
                        data-element_type="section"
                    >
                        <div class="elementor-container elementor-column-gap-extended">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-2a40db59 ot-column-items-center ot-flex-column-vertical" data-id="2a40db59" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-7b08551e elementor-widget elementor-widget-ilogo" data-id="7b08551e" data-element_type="widget" data-widget_type="ilogo.default">
                                                <div class="elementor-widget-container">
                                                    <div class="the-logo">
                                                        <a href="{{route('home.index')}}">
                                                            @include('layouts.logo')
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-57f4f99d ot-flex-column-vertical" data-id="57f4f99d" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-1ab58a34 elementor-widget elementor-widget-imenu" data-id="1ab58a34" data-element_type="widget" data-widget_type="imenu.default">
                                                <div class="elementor-widget-container">
                                                    <nav id="site-navigation" class="main-navigation">
                                                        <ul id="primary-menu" class="menu">
                                                            <li id="menu-item-2017" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="{{route('home.index')}}">Home</a></li>
                                                            <li id="menu-item-2030" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2030">
                                                                <a href="#">Company</a>
                                                                <ul class="sub-menu">
                                                                    <li id="menu-item-2015" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2015"><a href="{{route('company.aboutus')}}">About Us</a></li>
                                                                    <li id="menu-item-2026" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2026">
                                                                        <a href="{{route('company.whytecdata')}}">Why TecData</a>
                                                                    </li>
                                                                    <!-- <li id="menu-item-2021" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021"><a href="our-team/index.html">Our Team</a></li>
                                                                    <li id="menu-item-2458" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2458">
                                                                        <a href="our-team/single-team/index.html">Single Team</a>
                                                                    </li>
                                                                    <li id="menu-item-2698" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2698">
                                                                        <a href="shop/index.html">Shop</a>
                                                                        <ul class="sub-menu">
                                                                            <li id="menu-item-2965" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2965"><a href="shop/index.html">Catelog Page</a></li>
                                                                            <li id="menu-item-2700" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-2700">
                                                                                <a href="product/t-shirt-with-logo/index.html">Product Details</a>
                                                                            </li>
                                                                            <li id="menu-item-2696" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2696">
                                                                                <a href="checkout/index.html">Checkout Page</a>
                                                                            </li>
                                                                            <li id="menu-item-2697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2697"><a href="cart/index.html">Cart Page</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li id="menu-item-2024" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2024"><a href="typography/index.html">Typography</a></li>
                                                                    <li id="menu-item-2018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="elements/index.html">Elements</a></li>
                                                                    <li id="menu-item-2019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2019"><a href="faqs/index.html">FAQs</a></li>
                                                                    <li id="menu-item-2034" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2034"><a href="404.html">404 Error</a></li>
                                                                    <li id="menu-item-2016" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016"><a href="coming-soon/index.html">Coming Soon</a></li> -->
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-2424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2424">
                                                                <a href="it-services/index.html">Services</a>
                                                                <ul class="sub-menu">
                                                                    <li id="menu-item-2425" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2425"><a href="it-services/index.html">IT Services</a></li>
                                                                    <li id="menu-item-2025" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2025">
                                                                        <a href="it-services/web-development/index.html">Web Development</a>
                                                                    </li>
                                                                    <li id="menu-item-2276" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2276">
                                                                        <a href="it-services/mobile-development/index.html">Mobile Development</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-2022" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2022">
                                                                <a href="portfolio-grid/index.html">Projects</a>
                                                                <ul class="sub-menu">
                                                                    <li id="menu-item-2023" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2023">
                                                                        <a href="portfolio-masonry/index.html">Portfolio Masonry</a>
                                                                    </li>
                                                                    <li id="menu-item-2074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2074">
                                                                        <a href="portfolio-slider/index.html">Portfolio Carousel</a>
                                                                    </li>
                                                                    <li id="menu-item-2029" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2029">
                                                                        <a href="portfolio-grid/index.html">Portfolio Grid</a>
                                                                        <ul class="sub-menu">
                                                                            <li id="menu-item-2449" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2449">
                                                                                <a href="portfolio-grid/index.html">Portfolio 3 Columns</a>
                                                                            </li>
                                                                            <li id="menu-item-2448" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2448">
                                                                                <a href="portfolio-full-width/index.html">Portfolio 4 Columns</a>
                                                                            </li>
                                                                            <li id="menu-item-2450" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2450">
                                                                                <a href="portfolio-no-gap/index.html">Portfolio No Gap</a>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li id="menu-item-2033" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-has-children menu-item-2033">
                                                                        <a href="portfolio/ecommerce-website/index.html">Portfolio Details</a>
                                                                        <ul class="sub-menu">
                                                                            <li id="menu-item-2028" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2028">
                                                                                <a href="portfolio/ecommerce-website/index.html">Single Layout 1</a>
                                                                            </li>
                                                                            <li id="menu-item-2027" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2027">
                                                                                <a href="portfolio/mobile-coin-view-app/index.html">Single Layout 2</a>
                                                                            </li>
                                                                            <li id="menu-item-2032" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2032">
                                                                                <a href="portfolio/corporate-website/index.html">Single Layout 3</a>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-2014" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2014">
                                                                <a href="blog/index.html">Blog</a>
                                                                <ul class="sub-menu">
                                                                    <li id="menu-item-2405" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2405"><a href="blog/index.html">Blog List</a></li>
                                                                    <li id="menu-item-2404" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2404">
                                                                        <a href="plan-your-project-with-your-software/index.html">Blog Single</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-2017" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="{{route('contacts.index')}}">Contacts</a></li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-336223fd ot-flex-column-horizontal" data-id="336223fd" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
<!--                                             <div class="elementor-element elementor-element-53ed08c2 elementor-widget elementor-widget-icart" data-id="53ed08c2" data-element_type="widget" data-widget_type="icart.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-cart octf-cta-header">
                                                        <a class="cart-contents ot-minicart" href="cart/index.html" title="View your shopping cart"><i class="flaticon-shopper"></i> <span class="count">0</span> </a>
                                                        <div class="site-header-cart">
                                                            <div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="elementor-element elementor-element-4c593 elementor-widget elementor-widget-isearch" data-id="4c593" data-element_type="widget" data-widget_type="isearch.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-search octf-btn-cta">
                                                        <div class="toggle-search octf-cta-icons">
                                                            <i class="flaticon-search"></i>
                                                        </div>
                                                        <!-- Form Search on Header -->
                                                        <div class="h-search-form-field">
                                                            <div class="h-search-form-inner">
                                                                <form role="search" method="get" class="search-form" action="http://wpdemo.archiwp.com/engitech/">
                                                                    <label><span class="screen-reader-text">Search for:</span> <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" /></label>
                                                                    <button type="submit" class="search-submit"><i class="flaticon-search"></i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-f995aa2 elementor-widget elementor-widget-isidepanel" data-id="f995aa2" data-element_type="widget" data-widget_type="isidepanel.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-sidepanel octf-cta-header">
                                                        <div class="site-overlay panel-overlay"></div>
                                                        <div id="panel-btn" class="panel-btn octf-cta-icons">
                                                            <svg
                                                                version="1.1"
                                                                id="Capa_1"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                x="0px"
                                                                y="0px"
                                                                viewBox="0 0 270 270"
                                                                style="enable-background: new 0 0 270 270;"
                                                                xml:space="preserve"
                                                            >
                                                                <g>
                                                                    <g>
                                                                        <path
                                                                            d="M114,0H10C4.5,0,0,4.5,0,10v104c0,5.5,4.5,10,10,10h104c5.5,0,10-4.5,10-10V10C124,4.5,119.5,0,114,0z M104,104H20V20h84
								V104z"
                                                                        />
                                                                    </g>
                                                                </g>
                                                                <g>
                                                                    <g>
                                                                        <path
                                                                            d="M260,0H156c-5.5,0-10,4.5-10,10v104c0,5.5,4.5,10,10,10h104c5.5,0,10-4.5,10-10V10C270,4.5,265.5,0,260,0z M250,104h-84
								V20h84V104z"
                                                                        />
                                                                    </g>
                                                                </g>
                                                                <g>
                                                                    <g>
                                                                        <path
                                                                            d="M114,146H10c-5.5,0-10,4.5-10,10v104c0,5.5,4.5,10,10,10h104c5.5,0,10-4.5,10-10V156C124,150.5,119.5,146,114,146z
								 M104,250H20v-84h84V250z"
                                                                        />
                                                                    </g>
                                                                </g>
                                                                <g>
                                                                    <g>
                                                                        <path
                                                                            d="M260,146H156c-5.5,0-10,4.5-10,10v104c0,5.5,4.5,10,10,10h104c5.5,0,10-4.5,10-10V156C270,150.5,265.5,146,260,146z
								 M250,250h-84v-84h84V250z"
                                                                        />
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- #header-desktop-close -->

    <!-- #header-mobile-open -->
    <div class="header-mobile">
        <div data-elementor-type="wp-post" data-elementor-id="2854" class="elementor elementor-2854" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-55212198 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="55212198"
                        data-element_type="section"
                        data-settings='{"background_background":"classic"}'
                    >
                        <div class="elementor-container elementor-column-gap-extended">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-4cdb5f0b ot-flex-column-vertical" data-id="4cdb5f0b" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-524c330e elementor-widget elementor-widget-ilogo" data-id="524c330e" data-element_type="widget" data-widget_type="ilogo.default">
                                                <div class="elementor-widget-container">
                                                    <div class="the-logo">
                                                        <a href="index.html">
                                                            <img
                                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                alt="Engitech"
                                                                data-lazy-src="wp-content/uploads/sites/4/2020/09/logo.svg"
                                                            />
                                                            <noscript><img src="wp-content/uploads/sites/4/2020/09/logo.svg" alt="Engitech" /></noscript>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2df0dc0f ot-flex-column-horizontal" data-id="2df0dc0f" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-17478cf2 elementor-widget elementor-widget-icart" data-id="17478cf2" data-element_type="widget" data-widget_type="icart.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-cart octf-cta-header">
                                                        <a class="cart-contents ot-minicart" href="cart/index.html" title="View your shopping cart"><i class="flaticon-shopper"></i> <span class="count">0</span> </a>
                                                        <div class="site-header-cart">
                                                            <div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-6b4d4c31 elementor-widget elementor-widget-isearch" data-id="6b4d4c31" data-element_type="widget" data-widget_type="isearch.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-search octf-btn-cta">
                                                        <div class="toggle-search octf-cta-icons">
                                                            <i class="flaticon-search"></i>
                                                        </div>
                                                        <!-- Form Search on Header -->
                                                        <div class="h-search-form-field">
                                                            <div class="h-search-form-inner">
                                                                <form role="search" method="get" class="search-form" action="http://wpdemo.archiwp.com/engitech/">
                                                                    <label><span class="screen-reader-text">Search for:</span> <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" /></label>
                                                                    <button type="submit" class="search-submit"><i class="flaticon-search"></i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-38196644 elementor-widget elementor-widget-imenu_mobile" data-id="38196644" data-element_type="widget" data-widget_type="imenu_mobile.default">
                                                <div class="elementor-widget-container">
                                                    <div class="octf-menu-mobile octf-cta-header">
                                                        <div id="mmenu-toggle" class="mmenu-toggle">
                                                            <button></button>
                                                        </div>
                                                        <div class="site-overlay mmenu-overlay"></div>
                                                        <div id="mmenu-wrapper" class="mmenu-wrapper on-right">
                                                            <div class="mmenu-inner">
                                                                <a class="mmenu-close" href="#"><i class="flaticon-right-arrow-1"></i></a>
                                                                <div class="mobile-nav">
                                                                    <ul id="menu-main-menu" class="mobile_mainmenu none-style">
                                                                        <li
                                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2274 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-2315"
                                                                        >
                                                                            <a href="index.html" aria-current="page">Home</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2274 current_page_item menu-item-2702">
                                                                                    <a href="index.html" aria-current="page">Home 1</a>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2695"><a href="home-2/index.html">Home 2</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="home-3/index.html">Home 3</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2693"><a href="home-4/index.html">Home 4</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2692"><a href="home-5/index.html">Home 5</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2030">
                                                                            <a href="#">Company</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2015"><a href="about-us/index.html">About Us</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2026"><a href="why-choose-us/index.html">Why Choose Us</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021"><a href="our-team/index.html">Our Team</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2458"><a href="our-team/single-team/index.html">Single Team</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2698">
                                                                                    <a href="shop/index.html">Shop</a>
                                                                                    <ul class="sub-menu">
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2965"><a href="shop/index.html">Catelog Page</a></li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-2700">
                                                                                            <a href="product/t-shirt-with-logo/index.html">Product Details</a>
                                                                                        </li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2696"><a href="checkout/index.html">Checkout Page</a></li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2697"><a href="cart/index.html">Cart Page</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2024"><a href="typography/index.html">Typography</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="elements/index.html">Elements</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2019"><a href="faqs/index.html">FAQs</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2034"><a href="404.html">404 Error</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016"><a href="coming-soon/index.html">Coming Soon</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2424">
                                                                            <a href="it-services/index.html">Services</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2425"><a href="it-services/index.html">IT Services</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2025">
                                                                                    <a href="it-services/web-development/index.html">Web Development</a>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2276">
                                                                                    <a href="it-services/mobile-development/index.html">Mobile Development</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2022">
                                                                            <a href="portfolio-grid/index.html">Projects</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2023">
                                                                                    <a href="portfolio-masonry/index.html">Portfolio Masonry</a>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2074">
                                                                                    <a href="portfolio-slider/index.html">Portfolio Carousel</a>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2029">
                                                                                    <a href="portfolio-grid/index.html">Portfolio Grid</a>
                                                                                    <ul class="sub-menu">
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2449">
                                                                                            <a href="portfolio-grid/index.html">Portfolio 3 Columns</a>
                                                                                        </li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2448">
                                                                                            <a href="portfolio-full-width/index.html">Portfolio 4 Columns</a>
                                                                                        </li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2450">
                                                                                            <a href="portfolio-no-gap/index.html">Portfolio No Gap</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-has-children menu-item-2033">
                                                                                    <a href="portfolio/ecommerce-website/index.html">Portfolio Details</a>
                                                                                    <ul class="sub-menu">
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2028">
                                                                                            <a href="portfolio/ecommerce-website/index.html">Single Layout 1</a>
                                                                                        </li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2027">
                                                                                            <a href="portfolio/mobile-coin-view-app/index.html">Single Layout 2</a>
                                                                                        </li>
                                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2032">
                                                                                            <a href="portfolio/corporate-website/index.html">Single Layout 3</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2014">
                                                                            <a href="blog/index.html">Blog</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2405"><a href="blog/index.html">Blog List</a></li>
                                                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2404">
                                                                                    <a href="plan-your-project-with-your-software/index.html">Blog Single</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="contacts/index.html">Contacts</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- #header-mobile-close -->
</header>
<!-- #site-header-close -->
