            <!-- #content -->
            <footer id="site-footer" class="site-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
                <div data-elementor-type="wp-post" data-elementor-id="1308" class="elementor elementor-1308" data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-d0f9bfa ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="d0f9bfa"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-89daaf1 ot-flex-column-vertical" data-id="89daaf1" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-6057167 elementor-widget elementor-widget-image" data-id="6057167" data-element_type="widget" data-widget_type="image.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="143"
                                                                    height="40"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20143%2040'%3E%3C/svg%3E"
                                                                    class="attachment-large size-large"
                                                                    alt=""
                                                                    data-lazy-src="wp-content/uploads/sites/logo/logo143x40.svg"
                                                                />
                                                                <noscript><img width="143" height="40" src="wp-content/uploads/sites/4/2019/12/logo-light.png" class="attachment-large size-large" alt="" /></noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-e89c64b elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="e89c64b"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8d8441e ot-flex-column-vertical"
                                                                    data-id="8d8441e"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-8914b33 elementor-widget elementor-widget-icontact_info"
                                                                                data-id="8914b33"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icontact_info.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="contact-info box-style2">
                                                                                        <div class="box-icon">
                                                                                            <i class="flaticon-world-globe"></i>
                                                                                        </div>
                                                                                        <p>New #: 65, CIT Nagar 1st main road, Chennai, 600 035, INDIA</p>
                                                                                        <h6>Our Address</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5ebca85 ot-flex-column-vertical"
                                                                    data-id="5ebca85"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-1a1bfdb elementor-widget elementor-widget-icontact_info"
                                                                                data-id="1a1bfdb"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icontact_info.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="contact-info box-style2">
                                                                                        <div class="box-icon">
                                                                                            <i class="flaticon-envelope"></i>
                                                                                        </div>
                                                                                        <p>contact@tecdata.net</p>
                                                                                        <h6>Our Mailbox</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-c9201b5 ot-flex-column-vertical"
                                                                    data-id="c9201b5"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-ad7e989 elementor-widget elementor-widget-icontact_info"
                                                                                data-id="ad7e989"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icontact_info.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="contact-info box-style2">
                                                                                        <div class="box-icon">
                                                                                            <i class="flaticon-phone-1"></i>
                                                                                        </div>
                                                                                        <p>+91 93846 65527</p>
                                                                                        <h6>Our Phone</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <div
                                                        class="elementor-element elementor-element-3b2a0b7 footer-menu elementor-hidden-phone elementor-widget elementor-widget-wp-widget-nav_menu"
                                                        data-id="3b2a0b7"
                                                        data-element_type="widget"
                                                        data-widget_type="wp-widget-nav_menu.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="menu-footer-menu-container">
                                                                <ul id="menu-footer-menu" class="menu">
                                                                    <li id="menu-item-1315" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1315">
                                                                        <a href="index.html" aria-current="page">Home</a>
                                                                    </li>
                                                                    <li id="menu-item-2676" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2676"><a href="it-services/index.html">Services</a></li>
                                                                    <li id="menu-item-2677" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2677"><a href="portfolio-grid/index.html">Portfolio</a></li>
                                                                    <li id="menu-item-1316" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1316"><a href="blog/index.html">Blog</a></li>
                                                                    <li id="menu-item-1320" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1320"><a href="#">Contacts</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-c208bbf elementor-widget elementor-widget-text-editor" data-id="c208bbf" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix"><p>Copyright © 2020 Developed By TecData. All Rights Reserved.</p></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-9438214 elementor-grid-4 elementor-shape-rounded elementor-widget elementor-widget-social-icons"
                                                        data-id="9438214"
                                                        data-element_type="widget"
                                                        data-widget_type="social-icons.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-social-icons-wrapper elementor-grid">
                                                                <div class="elementor-grid-item">
                                                                    <a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-animation-float elementor-repeater-item-a24d46e" href="https://twitter.com/vizzainsurance?lang=en" target="_blank">
                                                                        <span class="elementor-screen-only">Twitter</span>
                                                                        <i class="fab fa-twitter"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="elementor-grid-item">
                                                                    <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-animation-float elementor-repeater-item-4261f62" href="https://www.facebook.com/VizzaInsuranceBrokingServices/" target="_blank">
                                                                        <span class="elementor-screen-only">Facebook-f</span>
                                                                        <i class="fab fa-facebook-f"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="elementor-grid-item">
                                                                    <a class="elementor-icon elementor-social-icon elementor-social-icon-linkedin-in elementor-animation-float elementor-repeater-item-b0dbeaa" href="https://in.linkedin.com/company/vizza-insurance-services" target="_blank">
                                                                        <span class="elementor-screen-only">Linkedin-in</span>
                                                                        <i class="fab fa-linkedin-in"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="elementor-grid-item">
                                                                    <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-float elementor-repeater-item-6b456a7" href="https://www.instagram.com/vizzainsurancebroking/?hl=bn" target="_blank">
                                                                        <span class="elementor-screen-only">Instagram</span>
                                                                        <i class="fab fa-instagram"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- #page -->