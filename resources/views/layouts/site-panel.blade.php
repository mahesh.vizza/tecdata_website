<!-- #side-panel-open -->
<div id="side-panel" class="side-panel">
    <a href="#" class="side-panel-close"><i class="flaticon-close"></i></a>
    <div class="side-panel-block">
        <div data-elementor-type="wp-post" data-elementor-id="2856" class="elementor elementor-2856" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-97bff10 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="97bff10"
                        data-element_type="section"
                    >
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3b0fcfbf ot-flex-column-vertical" data-id="3b0fcfbf" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-5b0ca0a2 elementor-widget elementor-widget-ilogo" data-id="5b0ca0a2" data-element_type="widget" data-widget_type="ilogo.default">
                                                <div class="elementor-widget-container">
                                                    <div class="the-logo">
                                                        <a href="{{route('home.index')}}">
                                                            @include('layouts.logo')
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-10eebe7 elementor-widget elementor-widget-text-editor" data-id="10eebe7" data-element_type="widget" data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>Over 10 years we help companies reach their financial and branding goals. Engitech is a values-driven technology agency dedicated.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-36f1ca99 elementor-widget elementor-widget-heading" data-id="36f1ca99" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h4 class="elementor-heading-title elementor-size-default">Gallery</h4>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-a841864 elementor-widget elementor-widget-image-gallery" data-id="a841864" data-element_type="widget" data-widget_type="image-gallery.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image-gallery">
                                                        <div id="gallery-1" class="gallery galleryid-2274 gallery-columns-3 gallery-size-engitech-portfolio-thumbnail-grid">
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon portrait">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project11"
                                                                        href="wp-content/uploads/sites/4/2019/11/project11.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project11-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project11-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon portrait">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project10"
                                                                        href="wp-content/uploads/sites/4/2019/11/project10.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project10-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project10-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon portrait">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project4"
                                                                        href="wp-content/uploads/sites/4/2019/11/project4.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project4-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project4-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon portrait">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project6"
                                                                        href="wp-content/uploads/sites/4/2019/11/project6.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project6-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project6-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon portrait">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project2"
                                                                        href="wp-content/uploads/sites/4/2019/11/project2.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project2-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project2-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                            <figure class="gallery-item">
                                                                <div class="gallery-icon landscape">
                                                                    <a
                                                                        data-elementor-open-lightbox="yes"
                                                                        data-elementor-lightbox-slideshow="a841864"
                                                                        data-elementor-lightbox-title="project1"
                                                                        href="wp-content/uploads/sites/4/2019/11/project1.jpg"
                                                                    >
                                                                        <img
                                                                            width="720"
                                                                            height="720"
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                            alt=""
                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-70x70.jpg 70w"
                                                                            data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                            data-lazy-src="wp-content/uploads/sites/4/2019/11/project1-720x720.jpg"
                                                                        />
                                                                        <noscript>
                                                                            <img
                                                                                width="720"
                                                                                height="720"
                                                                                src="wp-content/uploads/sites/4/2019/11/project1-720x720.jpg"
                                                                                class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                alt=""
                                                                                srcset="
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-720x720.jpg 720w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-150x150.jpg 150w,
                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-70x70.jpg    70w
                                                                                "
                                                                                sizes="(max-width: 720px) 100vw, 720px"
                                                                            />
                                                                        </noscript>
                                                                    </a>
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-5f98745a elementor-widget elementor-widget-heading" data-id="5f98745a" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h4 class="elementor-heading-title elementor-size-default">Contacts</h4>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-101d8395 elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                data-id="101d8395"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-map-marker-alt"></i> </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span></span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">New #: 65, CIT Nagar 1st main road, Chennai, 600 035, INDIA</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-3c6a17b7 elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                data-id="3c6a17b7"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-envelope"></i> </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span></span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">contact@tecdata.net</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-38207ad elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                data-id="38207ad"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-phone-alt"></i> </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span></span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">+91 93846 65527</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-15ddcfde elementor-shape-circle elementor-grid-4 elementor-widget elementor-widget-social-icons"
                                                data-id="15ddcfde"
                                                data-element_type="widget"
                                                data-widget_type="social-icons.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-social-icons-wrapper elementor-grid">
                                                        <div class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-animation-float elementor-repeater-item-a24d46e"
                                                                href="https://twitter.com/vizzainsurance?lang=en"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Twitter</span>
                                                                <i class="fab fa-twitter"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-animation-float elementor-repeater-item-4261f62" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Facebook-f</span>
                                                                <i class="fab fa-facebook-f"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-linkedin-in elementor-animation-float elementor-repeater-item-b0dbeaa"
                                                                href="https://in.linkedin.com/company/vizza-insurance-services"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Linkedin-in</span>
                                                                <i class="fab fa-linkedin-in"></i>
                                                            </a>
                                                        </div>
                                                        <div class="elementor-grid-item">
                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-float elementor-repeater-item-6b456a7" href="#" target="_blank">
                                                                <span class="elementor-screen-only">Instagram</span>
                                                                <i class="fab fa-instagram"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #side-panel-close -->
