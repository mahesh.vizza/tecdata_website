          <!-- #site-header-open -->
          <header id="site-header" class="site-header header-static">
                <!-- #header-desktop-open -->
                <div class="header-desktop">
                    <div data-elementor-type="wp-post" data-elementor-id="2843" class="elementor elementor-2843" data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                    class="elementor-section elementor-top-section elementor-element elementor-element-f515cb0 elementor-section-content-middle elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                    data-id="f515cb0"
                                    data-element_type="section"
                                    data-settings='{"background_background":"classic"}'
                                >
                                    <div class="elementor-container elementor-column-gap-extended">
                                        <div class="elementor-row">
                                            <div
                                                class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2416e109 ot-column-items-center ot-flex-column-vertical"
                                                data-id="2416e109"
                                                data-element_type="column"
                                            >
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div
                                                            class="elementor-element elementor-element-2081dfd7 elementor-icon-list--layout-inline elementor-align-left elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list"
                                                            data-id="2081dfd7"
                                                            data-element_type="widget"
                                                            data-widget_type="icon-list.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <ul class="elementor-icon-list-items elementor-inline-items">
                                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                                        <a href="mailto:engitech@mail.com">
                                                                            <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="fas fa-envelope"></i> </span>
                                                                            <span class="elementor-icon-list-text">engitech@mail.com</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                                        <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="fas fa-clock"></i> </span>
                                                                        <span class="elementor-icon-list-text">Mon - Sat: 8.00 am - 7.00 pm</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3346d93d ot-flex-column-horizontal" data-id="3346d93d" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div
                                                            class="elementor-element elementor-element-11e4f792 elementor-widget elementor-widget-text-editor"
                                                            data-id="11e4f792"
                                                            data-element_type="widget"
                                                            data-widget_type="text-editor.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                    <p>
                                                                        We are creative, ambitious and ready for challenges! <span style="color: #43baff;"><a style="color: #43baff;" href="../contacts/index.html">Hire Us</a></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-31437729 elementor-shape-square elementor-grid-4 elementor-widget elementor-widget-social-icons"
                                                            data-id="31437729"
                                                            data-element_type="widget"
                                                            data-widget_type="social-icons.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-social-icons-wrapper elementor-grid">
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-4bf2890" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Twitter</span>
                                                                            <i class="fab fa-twitter"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-repeater-item-a79d483" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Facebook-f</span>
                                                                            <i class="fab fa-facebook-f"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-linkedin-in elementor-repeater-item-e6c93ca" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Linkedin-in</span>
                                                                            <i class="fab fa-linkedin-in"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-repeater-item-2faf370" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Instagram</span>
                                                                            <i class="fab fa-instagram"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section
                                    class="elementor-section elementor-top-section elementor-element elementor-element-755e8132 elementor-section-full_width is-fixed ot-traditional elementor-section-height-default elementor-section-height-default"
                                    data-id="755e8132"
                                    data-element_type="section"
                                >
                                    <div class="elementor-container elementor-column-gap-extended">
                                        <div class="elementor-row">
                                            <div
                                                class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-62c5fc44 ot-column-items-center ot-flex-column-vertical"
                                                data-id="62c5fc44"
                                                data-element_type="column"
                                            >
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-60d53692 elementor-widget elementor-widget-ilogo" data-id="60d53692" data-element_type="widget" data-widget_type="ilogo.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="the-logo">
                                                                    <a href="../index.html">
                                                                        <img
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                            alt="Engitech"
                                                                            data-lazy-src="../wp-content/uploads/sites/4/2020/09/logo.svg"
                                                                        />
                                                                        <noscript><img src="../wp-content/uploads/sites/4/2020/09/logo.svg" alt="Engitech" /></noscript>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            @include('layouts.head-title')

                                            <div
                                                class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1534b56a ot-flex-column-horizontal hidden-1200"
                                                data-id="1534b56a"
                                                data-element_type="column"
                                            >
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-56f061f0 elementor-widget elementor-widget-icart" data-id="56f061f0" data-element_type="widget" data-widget_type="icart.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="octf-cart octf-cta-header">
                                                                    <a class="cart-contents ot-minicart" href="../cart/index.html" title="View your shopping cart"><i class="flaticon-shopper"></i> <span class="count">0</span> </a>
                                                                    <div class="site-header-cart">
                                                                        <div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-22d1ee74 elementor-widget elementor-widget-isearch" data-id="22d1ee74" data-element_type="widget" data-widget_type="isearch.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="octf-search octf-btn-cta">
                                                                    <div class="toggle-search octf-cta-icons">
                                                                        <i class="flaticon-search"></i>
                                                                    </div>
                                                                    <!-- Form Search on Header -->
                                                                    <div class="h-search-form-field">
                                                                        <div class="h-search-form-inner">
                                                                            <form role="search" method="get" class="search-form" action="http://wpdemo.archiwp.com/engitech/">
                                                                                <label><span class="screen-reader-text">Search for:</span> <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" /></label>
                                                                                <button type="submit" class="search-submit"><i class="flaticon-search"></i></button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-2d3490c6 ot-column-items-center ot-flex-column-horizontal"
                                                data-id="2d3490c6"
                                                data-element_type="column"
                                            >
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div
                                                            class="elementor-element elementor-element-1bd34bdf elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                            data-id="1bd34bdf"
                                                            data-element_type="widget"
                                                            data-widget_type="icon-box.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-icon">
                                                                        <span class="elementor-icon elementor-animation-">
                                                                            <svg
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                id="Capa_1"
                                                                                x="0px"
                                                                                y="0px"
                                                                                viewBox="0 0 384 384"
                                                                                style="enable-background: new 0 0 384 384;"
                                                                                xml:space="preserve"
                                                                            >
                                                                                <path
                                                                                    d="M353.188,252.052c-23.51,0-46.594-3.677-68.469-10.906c-10.719-3.656-23.896-0.302-30.438,6.417l-43.177,32.594   c-50.073-26.729-80.917-57.563-107.281-107.26l31.635-42.052c8.219-8.208,11.167-20.198,7.635-31.448   c-7.26-21.99-10.948-45.063-10.948-68.583C132.146,13.823,118.323,0,101.333,0H30.813C13.823,0,0,13.823,0,30.813   C0,225.563,158.438,384,353.188,384c16.99,0,30.813-13.823,30.813-30.813v-70.323C384,265.875,370.177,252.052,353.188,252.052z"
                                                                                ></path>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <div class="elementor-icon-box-content">
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span>Have Any Questions?</span>
                                                                        </h3>
                                                                        <p class="elementor-icon-box-description">+1-800-456-478-23</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-11482d6c elementor-widget elementor-widget-ibutton" data-id="11482d6c" data-element_type="widget" data-widget_type="ibutton.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="ot-button">
                                                                    <a href="../contacts/index.html" class="octf-btn octf-btn-primary">free quote</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #header-desktop-close -->

                <!-- #header-mobile-open -->
                <div class="header-mobile">
                    <div data-elementor-type="wp-post" data-elementor-id="2854" class="elementor elementor-2854" data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                    class="elementor-section elementor-top-section elementor-element elementor-element-55212198 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="55212198"
                                    data-element_type="section"
                                    data-settings='{"background_background":"classic"}'
                                >
                                    <div class="elementor-container elementor-column-gap-extended">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-4cdb5f0b ot-flex-column-vertical" data-id="4cdb5f0b" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-524c330e elementor-widget elementor-widget-ilogo" data-id="524c330e" data-element_type="widget" data-widget_type="ilogo.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="the-logo">
                                                                    <a href="../index.html">
                                                                        <img
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                            alt="Engitech"
                                                                            data-lazy-src="../wp-content/uploads/sites/4/2020/09/logo.svg"
                                                                        />
                                                                        <noscript><img src="../wp-content/uploads/sites/4/2020/09/logo.svg" alt="Engitech" /></noscript>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2df0dc0f ot-flex-column-horizontal" data-id="2df0dc0f" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-17478cf2 elementor-widget elementor-widget-icart" data-id="17478cf2" data-element_type="widget" data-widget_type="icart.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="octf-cart octf-cta-header">
                                                                    <a class="cart-contents ot-minicart" href="../cart/index.html" title="View your shopping cart"><i class="flaticon-shopper"></i> <span class="count">0</span> </a>
                                                                    <div class="site-header-cart">
                                                                        <div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-6b4d4c31 elementor-widget elementor-widget-isearch" data-id="6b4d4c31" data-element_type="widget" data-widget_type="isearch.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="octf-search octf-btn-cta">
                                                                    <div class="toggle-search octf-cta-icons">
                                                                        <i class="flaticon-search"></i>
                                                                    </div>
                                                                    <!-- Form Search on Header -->
                                                                    <div class="h-search-form-field">
                                                                        <div class="h-search-form-inner">
                                                                            <form role="search" method="get" class="search-form" action="http://wpdemo.archiwp.com/engitech/">
                                                                                <label><span class="screen-reader-text">Search for:</span> <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" /></label>
                                                                                <button type="submit" class="search-submit"><i class="flaticon-search"></i></button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-38196644 elementor-widget elementor-widget-imenu_mobile"
                                                            data-id="38196644"
                                                            data-element_type="widget"
                                                            data-widget_type="imenu_mobile.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="octf-menu-mobile octf-cta-header">
                                                                    <div id="mmenu-toggle" class="mmenu-toggle">
                                                                        <button></button>
                                                                    </div>
                                                                    <div class="site-overlay mmenu-overlay"></div>
                                                                    <div id="mmenu-wrapper" class="mmenu-wrapper on-right">
                                                                        <div class="mmenu-inner">
                                                                            <a class="mmenu-close" href="#"><i class="flaticon-right-arrow-1"></i></a>
                                                                            <div class="mobile-nav">
                                                                                <ul id="menu-main-menu" class="mobile_mainmenu none-style">
                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-2315">
                                                                                        <a href="../index.html">Home</a>
                                                                                        <ul class="sub-menu">
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2702"><a href="../index.html">Home 1</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2695"><a href="../home-2/index.html">Home 2</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="../home-3/index.html">Home 3</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2693"><a href="../home-4/index.html">Home 4</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2692"><a href="../home-5/index.html">Home 5</a></li>
                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-2030">
                                                                                        <a href="#">Company</a>
                                                                                        <ul class="sub-menu">
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1579 current_page_item menu-item-2015">
                                                                                                <a href="index.html" aria-current="page">About Us</a>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2026"><a href="../why-choose-us/index.html">Why Choose Us</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021"><a href="../our-team/index.html">Our Team</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2458">
                                                                                                <a href="../our-team/single-team/index.html">Single Team</a>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2698">
                                                                                                <a href="../shop/index.html">Shop</a>
                                                                                                <ul class="sub-menu">
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2965"><a href="../shop/index.html">Catelog Page</a></li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-2700">
                                                                                                        <a href="../product/t-shirt-with-logo/index.html">Product Details</a>
                                                                                                    </li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2696"><a href="../checkout/index.html">Checkout Page</a></li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2697"><a href="../cart/index.html">Cart Page</a></li>
                                                                                                </ul>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2024"><a href="../typography/index.html">Typography</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="../elements/index.html">Elements</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2019"><a href="../faqs/index.html">FAQs</a></li>
                                                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2034"><a href="../404.html">404 Error</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016"><a href="../coming-soon/index.html">Coming Soon</a></li>
                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2424">
                                                                                        <a href="../it-services/index.html">Services</a>
                                                                                        <ul class="sub-menu">
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2425"><a href="../it-services/index.html">IT Services</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2025">
                                                                                                <a href="../it-services/web-development/index.html">Web Development</a>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2276">
                                                                                                <a href="../it-services/mobile-development/index.html">Mobile Development</a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2022">
                                                                                        <a href="../portfolio-grid/index.html">Projects</a>
                                                                                        <ul class="sub-menu">
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2023">
                                                                                                <a href="../portfolio-masonry/index.html">Portfolio Masonry</a>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2074">
                                                                                                <a href="../portfolio-slider/index.html">Portfolio Carousel</a>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2029">
                                                                                                <a href="../portfolio-grid/index.html">Portfolio Grid</a>
                                                                                                <ul class="sub-menu">
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2449">
                                                                                                        <a href="../portfolio-grid/index.html">Portfolio 3 Columns</a>
                                                                                                    </li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2448">
                                                                                                        <a href="../portfolio-full-width/index.html">Portfolio 4 Columns</a>
                                                                                                    </li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2450">
                                                                                                        <a href="../portfolio-no-gap/index.html">Portfolio No Gap</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-has-children menu-item-2033">
                                                                                                <a href="../portfolio/ecommerce-website/index.html">Portfolio Details</a>
                                                                                                <ul class="sub-menu">
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2028">
                                                                                                        <a href="../portfolio/ecommerce-website/index.html">Single Layout 1</a>
                                                                                                    </li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2027">
                                                                                                        <a href="../portfolio/mobile-coin-view-app/index.html">Single Layout 2</a>
                                                                                                    </li>
                                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2032">
                                                                                                        <a href="../portfolio/corporate-website/index.html">Single Layout 3</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2014">
                                                                                        <a href="../blog/index.html">Blog</a>
                                                                                        <ul class="sub-menu">
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2405"><a href="../blog/index.html">Blog List</a></li>
                                                                                            <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2404">
                                                                                                <a href="../plan-your-project-with-your-software/index.html">Blog Single</a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="../contacts/index.html">Contacts</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #header-mobile-close -->
            </header>
            <!-- #site-header-close -->
            <!-- #side-panel-open -->
            <div id="side-panel" class="side-panel">
                <a href="#" class="side-panel-close"><i class="flaticon-close"></i></a>
                <div class="side-panel-block">
                    <div data-elementor-type="wp-post" data-elementor-id="2856" class="elementor elementor-2856" data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                    class="elementor-section elementor-top-section elementor-element elementor-element-97bff10 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="97bff10"
                                    data-element_type="section"
                                >
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3b0fcfbf ot-flex-column-vertical" data-id="3b0fcfbf" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-5b0ca0a2 elementor-widget elementor-widget-ilogo" data-id="5b0ca0a2" data-element_type="widget" data-widget_type="ilogo.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="the-logo">
                                                                    <a href="../index.html">
                                                                        <img
                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                            alt="Engitech"
                                                                            data-lazy-src="../wp-content/uploads/sites/4/2020/09/logo.svg"
                                                                        />
                                                                        <noscript><img src="../wp-content/uploads/sites/4/2020/09/logo.svg" alt="Engitech" /></noscript>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-10eebe7 elementor-widget elementor-widget-text-editor"
                                                            data-id="10eebe7"
                                                            data-element_type="widget"
                                                            data-widget_type="text-editor.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                    <p>Over 10 years we help companies reach their financial and branding goals. Engitech is a values-driven technology agency dedicated.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-36f1ca99 elementor-widget elementor-widget-heading" data-id="36f1ca99" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                                <h4 class="elementor-heading-title elementor-size-default">Gallery</h4>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-a841864 elementor-widget elementor-widget-image-gallery"
                                                            data-id="a841864"
                                                            data-element_type="widget"
                                                            data-widget_type="image-gallery.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-image-gallery">
                                                                    <div id="gallery-1" class="gallery galleryid-1579 gallery-columns-3 gallery-size-engitech-portfolio-thumbnail-grid">
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon portrait">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project11"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project11.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project11-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project11-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project11-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon portrait">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project10"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project10.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project10-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project10-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project10-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon portrait">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project4"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project4.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project4-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project4-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project4-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon portrait">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project6"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project6.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project6-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project6-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project6-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon portrait">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project2"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project2.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project2-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project2-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project2-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                        <figure class="gallery-item">
                                                                            <div class="gallery-icon landscape">
                                                                                <a
                                                                                    data-elementor-open-lightbox="yes"
                                                                                    data-elementor-lightbox-slideshow="a841864"
                                                                                    data-elementor-lightbox-title="project1"
                                                                                    href="../wp-content/uploads/sites/4/2019/11/project1.jpg"
                                                                                >
                                                                                    <img
                                                                                        width="720"
                                                                                        height="720"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20720'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                        alt=""
                                                                                        data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-720x720.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-150x150.jpg 150w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-70x70.jpg 70w"
                                                                                        data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/project1-720x720.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="720"
                                                                                            height="720"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/project1-720x720.jpg"
                                                                                            class="attachment-engitech-portfolio-thumbnail-grid size-engitech-portfolio-thumbnail-grid"
                                                                                            alt=""
                                                                                            srcset="
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-720x720.jpg 720w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-150x150.jpg 150w,
                                                                                                http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/11/project1-70x70.jpg    70w
                                                                                            "
                                                                                            sizes="(max-width: 720px) 100vw, 720px"
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-5f98745a elementor-widget elementor-widget-heading" data-id="5f98745a" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                                <h4 class="elementor-heading-title elementor-size-default">Contacts</h4>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-101d8395 elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                            data-id="101d8395"
                                                            data-element_type="widget"
                                                            data-widget_type="icon-box.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-icon">
                                                                        <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-map-marker-alt"></i> </span>
                                                                    </div>
                                                                    <div class="elementor-icon-box-content">
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span></span>
                                                                        </h3>
                                                                        <p class="elementor-icon-box-description">411 University St, Seattle, USA</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-3c6a17b7 elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                            data-id="3c6a17b7"
                                                            data-element_type="widget"
                                                            data-widget_type="icon-box.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-icon">
                                                                        <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-envelope"></i> </span>
                                                                    </div>
                                                                    <div class="elementor-icon-box-content">
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span></span>
                                                                        </h3>
                                                                        <p class="elementor-icon-box-description">engitech@oceanthemes.net</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-38207ad elementor-position-left elementor-vertical-align-middle elementor-view-default elementor-widget elementor-widget-icon-box"
                                                            data-id="38207ad"
                                                            data-element_type="widget"
                                                            data-widget_type="icon-box.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-icon">
                                                                        <span class="elementor-icon elementor-animation-"> <i aria-hidden="true" class="fas fa-phone-alt"></i> </span>
                                                                    </div>
                                                                    <div class="elementor-icon-box-content">
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span></span>
                                                                        </h3>
                                                                        <p class="elementor-icon-box-description">+1 -800-456-478-23</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-15ddcfde elementor-shape-circle elementor-grid-4 elementor-widget elementor-widget-social-icons"
                                                            data-id="15ddcfde"
                                                            data-element_type="widget"
                                                            data-widget_type="social-icons.default"
                                                        >
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-social-icons-wrapper elementor-grid">
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-animation-float elementor-repeater-item-a24d46e" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Twitter</span>
                                                                            <i class="fab fa-twitter"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-animation-float elementor-repeater-item-4261f62" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Facebook-f</span>
                                                                            <i class="fab fa-facebook-f"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-pinterest-p elementor-animation-float elementor-repeater-item-b0dbeaa" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Pinterest-p</span>
                                                                            <i class="fab fa-pinterest-p"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="elementor-grid-item">
                                                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-float elementor-repeater-item-6b456a7" href="#" target="_blank">
                                                                            <span class="elementor-screen-only">Instagram</span>
                                                                            <i class="fab fa-instagram"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #side-panel-close -->