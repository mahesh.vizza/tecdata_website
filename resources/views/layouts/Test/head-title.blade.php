<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-224525d5 ot-flex-column-vertical" data-id="224525d5" data-element_type="column">
    <div class="elementor-column-wrap elementor-element-populated">
        <div class="elementor-widget-wrap">
            <div class="elementor-element elementor-element-2ff5b4f4 elementor-widget elementor-widget-imenu" data-id="2ff5b4f4" data-element_type="widget" data-widget_type="imenu.default">
                <div class="elementor-widget-container">
                    <nav id="site-navigation" class="main-navigation">
                        <ul id="primary-menu" class="menu">
                            <li id="menu-item-2017" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="{{route('home.index')}}">Home</a></li>
                            <li id="menu-item-2030" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-2030">
                                <a href="#">Company</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2015" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1579 current_page_item menu-item-2015">
                                        <a href="{{route('company.aboutus')}}" aria-current="page">About Us</a>
                                    </li>
                                    <li id="menu-item-2026" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2026">
                                        <a href="../why-choose-us/index.html">Why Choose Us</a>
                                    </li>
                                    <li id="menu-item-2021" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021"><a href="../our-team/index.html">Our Team</a></li>
                                    <li id="menu-item-2458" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2458">
                                        <a href="../our-team/single-team/index.html">Single Team</a>
                                    </li>
                                    <li id="menu-item-2698" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2698">
                                        <a href="../shop/index.html">Shop</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-2965" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2965">
                                                <a href="../shop/index.html">Catelog Page</a>
                                            </li>
                                            <li id="menu-item-2700" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-2700">
                                                <a href="../product/t-shirt-with-logo/index.html">Product Details</a>
                                            </li>
                                            <li id="menu-item-2696" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2696">
                                                <a href="../checkout/index.html">Checkout Page</a>
                                            </li>
                                            <li id="menu-item-2697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2697"><a href="../cart/index.html">Cart Page</a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-2024" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2024"><a href="../typography/index.html">Typography</a></li>
                                    <li id="menu-item-2018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="../elements/index.html">Elements</a></li>
                                    <li id="menu-item-2019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2019"><a href="../faqs/index.html">FAQs</a></li>
                                    <li id="menu-item-2034" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2034"><a href="../404.html">404 Error</a></li>
                                    <li id="menu-item-2016" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016"><a href="../coming-soon/index.html">Coming Soon</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-2424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2424">
                                <a href="../it-services/index.html">Services</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2425" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2425"><a href="../it-services/index.html">IT Services</a></li>
                                    <li id="menu-item-2025" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2025">
                                        <a href="../it-services/web-development/index.html">Web Development</a>
                                    </li>
                                    <li id="menu-item-2276" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2276">
                                        <a href="../it-services/mobile-development/index.html">Mobile Development</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-2022" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2022">
                                <a href="../portfolio-grid/index.html">Projects</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2023" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2023">
                                        <a href="../portfolio-masonry/index.html">Portfolio Masonry</a>
                                    </li>
                                    <li id="menu-item-2074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2074">
                                        <a href="../portfolio-slider/index.html">Portfolio Carousel</a>
                                    </li>
                                    <li id="menu-item-2029" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2029">
                                        <a href="../portfolio-grid/index.html">Portfolio Grid</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-2449" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2449">
                                                <a href="../portfolio-grid/index.html">Portfolio 3 Columns</a>
                                            </li>
                                            <li id="menu-item-2448" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2448">
                                                <a href="../portfolio-full-width/index.html">Portfolio 4 Columns</a>
                                            </li>
                                            <li id="menu-item-2450" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2450">
                                                <a href="../portfolio-no-gap/index.html">Portfolio No Gap</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-2033" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-has-children menu-item-2033">
                                        <a href="../portfolio/ecommerce-website/index.html">Portfolio Details</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-2028" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2028">
                                                <a href="../portfolio/ecommerce-website/index.html">Single Layout 1</a>
                                            </li>
                                            <li id="menu-item-2027" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2027">
                                                <a href="../portfolio/mobile-coin-view-app/index.html">Single Layout 2</a>
                                            </li>
                                            <li id="menu-item-2032" class="menu-item menu-item-type-post_type menu-item-object-ot_portfolio menu-item-2032">
                                                <a href="../portfolio/corporate-website/index.html">Single Layout 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-2014" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2014">
                                <a href="../blog/index.html">Blog</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2405" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2405"><a href="../blog/index.html">Blog List</a></li>
                                    <li id="menu-item-2404" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2404">
                                        <a href="../plan-your-project-with-your-software/index.html">Blog Single</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-2017" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2017"><a href="../contacts/index.html">Contacts</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
