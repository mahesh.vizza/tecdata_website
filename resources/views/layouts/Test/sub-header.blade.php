<head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />

        <title>About Us &#8211; Engitech</title>
        <style id="rocket-critical-css">
            :root {
                --wp-admin-theme-color: #007cba;
                --wp-admin-theme-color-darker-10: #006ba1;
                --wp-admin-theme-color-darker-20: #005a87;
            }
            .woocommerce-page img {
                height: auto;
                max-width: 100%;
            }
            .woocommerce form .form-row::after,
            .woocommerce form .form-row::before,
            .woocommerce-page form .form-row::after,
            .woocommerce-page form .form-row::before {
                content: " ";
                display: table;
            }
            .woocommerce form .form-row::after,
            .woocommerce-page form .form-row::after {
                clear: both;
            }
            .woocommerce form .form-row label,
            .woocommerce-page form .form-row label {
                display: block;
            }
            .woocommerce form .form-row .input-text,
            .woocommerce-page form .form-row .input-text {
                box-sizing: border-box;
                width: 100%;
            }
            .woocommerce form .form-row-wide,
            .woocommerce-page form .form-row-wide {
                clear: both;
            }
            .screen-reader-text {
                clip: rect(1px, 1px, 1px, 1px);
                height: 1px;
                overflow: hidden;
                position: absolute !important;
                width: 1px;
                word-wrap: normal !important;
            }
            .woocommerce form .form-row {
                padding: 3px;
                margin: 0 0 6px;
            }
            .woocommerce form .form-row label {
                line-height: 2;
            }
            .woocommerce form .form-row .required {
                color: red;
                font-weight: 700;
                border: 0 !important;
                text-decoration: none;
                visibility: hidden;
            }
            .woocommerce form .form-row input.input-text {
                box-sizing: border-box;
                width: 100%;
                margin: 0;
                outline: 0;
                line-height: normal;
            }
            .woocommerce form .form-row ::-webkit-input-placeholder {
                line-height: normal;
            }
            .woocommerce form .form-row :-moz-placeholder {
                line-height: normal;
            }
            .woocommerce form .form-row :-ms-input-placeholder {
                line-height: normal;
            }
            .woocommerce form.login {
                border: 1px solid #d3ced2;
                padding: 20px;
                margin: 2em 0;
                text-align: left;
                border-radius: 5px;
            }
            .woocommerce-no-js form.woocommerce-form-login {
                display: block !important;
            }
            .woocommerce-account .woocommerce::after,
            .woocommerce-account .woocommerce::before {
                content: " ";
                display: table;
            }
            .woocommerce-account .woocommerce::after {
                clear: both;
            }
            img {
                border: 0;
            }
            body,
            figure {
                margin: 0;
            }
            html {
                font-family: sans-serif;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            figure,
            header,
            main,
            nav,
            section {
                display: block;
            }
            a {
                background-color: transparent;
                color: #337ab7;
                text-decoration: none;
            }
            h1 {
                font-size: 2em;
                margin: 0.67em 0;
            }
            body {
                background-color: #fff;
            }
            img {
                vertical-align: middle;
            }
            svg:not(:root) {
                overflow: hidden;
            }
            button,
            input {
                color: inherit;
                font: inherit;
                margin: 0;
            }
            button {
                overflow: visible;
            }
            button {
                text-transform: none;
            }
            button {
                -webkit-appearance: button;
            }
            button::-moz-focus-inner,
            input::-moz-focus-inner {
                border: 0;
                padding: 0;
            }
            input[type="search"] {
                -webkit-appearance: textfield;
                -webkit-box-sizing: content-box;
                -moz-box-sizing: content-box;
                box-sizing: content-box;
            }
            input[type="search"]::-webkit-search-cancel-button,
            input[type="search"]::-webkit-search-decoration {
                -webkit-appearance: none;
            }
            *,
            :after,
            :before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            html {
                font-size: 10px;
            }
            body {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
                line-height: 1.42857143;
                color: #333;
            }
            button,
            input {
                font-family: inherit;
                font-size: inherit;
                line-height: inherit;
            }
            .container {
                margin-right: auto;
                margin-left: auto;
                padding-left: 15px;
                padding-right: 15px;
            }
            @media (min-width: 768px) {
                .container {
                    width: 750px;
                }
            }
            @media (min-width: 992px) {
                .container {
                    width: 970px;
                }
            }
            .row {
                margin-left: -15px;
                margin-right: -15px;
            }
            .col-md-12 {
                position: relative;
                min-height: 1px;
                padding-left: 15px;
                padding-right: 15px;
            }
            @media (min-width: 992px) {
                .col-md-12 {
                    float: left;
                }
                .col-md-12 {
                    width: 100%;
                }
            }
            .container:after,
            .container:before,
            .row:after,
            .row:before {
                content: " ";
                display: table;
            }
            .container:after,
            .row:after {
                clear: both;
            }
            .show {
                display: block !important;
            }
            @font-face {
                font-family: "Flaticon";
                src: url(../wp-content/themes/engitech/fonts/Flaticon.eot);
                src: url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/fonts/Flaticon.eot?#iefix) format("embedded-opentype"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/fonts/Flaticon.woff2) format("woff2"), url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/fonts/Flaticon.woff) format("woff"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/fonts/Flaticon.ttf) format("truetype"), url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/fonts/Flaticon.svg#Flaticon) format("svg");
                font-weight: normal;
                font-style: normal;
            }
            @media screen and (-webkit-min-device-pixel-ratio: 0) {
                @font-face {
                    font-family: "Flaticon";
                    src: url(http://wpdemo.archiwp.com/engitech/wp-content/themes/engitech/css/Flaticon.svg#Flaticon) format("svg");
                }
            }
            [class^="flaticon-"]:before,
            [class^="flaticon-"]:after {
                font-family: Flaticon;
                font-size: 20px;
                font-style: normal;
            }
            .flaticon-search:before {
                content: "\f101";
            }
            .flaticon-shopper:before {
                content: "\f103";
            }
            .flaticon-up-arrow:before {
                content: "\f109";
            }
            .flaticon-right-arrow-1:before {
                content: "\f10a";
            }
            .flaticon-close:before {
                content: "\f13a";
            }
            button::-moz-focus-inner {
                padding: 0;
                border: 0;
            }
            html {
                line-height: 1.15;
                -webkit-text-size-adjust: 100%;
                overflow-x: hidden;
            }
            body {
                margin: 0;
            }
            a {
                background-color: transparent;
            }
            img {
                border-style: none;
            }
            button,
            input {
                font-family: inherit;
                font-size: 100%;
                line-height: 1.15;
                margin: 0;
            }
            button,
            input {
                overflow: visible;
            }
            button {
                text-transform: none;
            }
            button,
            [type="submit"] {
                -webkit-appearance: button;
            }
            button::-moz-focus-inner,
            [type="submit"]::-moz-focus-inner {
                border-style: none;
                padding: 0;
            }
            button:-moz-focusring,
            [type="submit"]:-moz-focusring {
                outline: 1px dotted ButtonText;
            }
            [type="search"] {
                -webkit-appearance: textfield;
                outline-offset: -2px;
            }
            [type="search"]::-webkit-search-decoration {
                -webkit-appearance: none;
            }
            ::-webkit-file-upload-button {
                -webkit-appearance: button;
                font: inherit;
            }
            body,
            button,
            input {
                color: #6d6d6d;
                font-family: "Nunito Sans", sans-serif;
                font-size: 16px;
                line-height: 1.875;
                font-weight: 400;
                -ms-word-wrap: break-word;
                word-wrap: break-word;
                box-sizing: border-box;
            }
            h1,
            h2,
            h3,
            h4 {
                font-family: "Montserrat", sans-serif;
                font-weight: 800;
                line-height: 1.2;
                margin: 0 0 20px;
                color: #1b1d21;
            }
            h1 {
                font-size: 48px;
            }
            h2 {
                font-size: 36px;
            }
            h3 {
                font-size: 30px;
            }
            h4 {
                font-size: 24px;
                font-weight: bold;
            }
            p {
                margin: 0 0 20px;
            }
            i {
                font-style: italic;
            }
            .flex-middle {
                display: flex;
                align-items: center;
                justify-content: center;
            }
            html {
                box-sizing: border-box;
            }
            *,
            *:before,
            *:after {
                box-sizing: inherit;
            }
            body {
                background: #fff;
                overflow-x: hidden;
            }
            ul {
                margin: 0 0 20px;
                padding-left: 18px;
            }
            ul {
                list-style: disc;
            }
            li > ul {
                margin-bottom: 0;
                margin-left: 1.5em;
            }
            .none-style {
                list-style: none;
                padding-left: 0;
            }
            img {
                height: auto;
                max-width: 100%;
            }
            figure {
                margin: 1em 0;
            }
            .octf-btn {
                font-size: 14px;
                padding: 14px 30px 14px 30px;
                line-height: 1.42857143;
                display: inline-block;
                margin-bottom: 0;
                text-decoration: none;
                text-transform: uppercase;
                white-space: nowrap;
                vertical-align: middle;
                font-weight: bold;
                text-align: center;
                background: #43baff;
                border: 1px solid transparent;
                color: #fff;
                outline: none;
            }
            .octf-btn:visited {
                color: #fff;
            }
            input[type="text"],
            input[type="search"] {
                color: #b5b5b5;
                border: none;
                background: #f6f6f6;
                padding: 10px 20px;
                box-sizing: border-box;
                outline: none;
            }
            ::-webkit-input-placeholder {
                color: #6d6d6d;
            }
            ::-moz-placeholder {
                color: #6d6d6d;
            }
            :-ms-input-placeholder {
                color: #6d6d6d;
            }
            :-moz-placeholder {
                color: #6d6d6d;
            }
            a {
                color: #7141b1;
                text-decoration: none;
            }
            a:visited {
                color: #7141b1;
            }
            .main-navigation ul {
                list-style: none;
                padding-left: 0;
            }
            .site-header {
                background: #fff;
                position: relative;
                z-index: 10;
            }
            .main-navigation {
                position: relative;
                display: inline-block;
                vertical-align: middle;
                height: 100%;
                width: auto;
            }
            .main-navigation > ul {
                position: relative;
                display: flex;
                width: 100%;
                vertical-align: middle;
                height: 100%;
            }
            .main-navigation ul {
                font-weight: bold;
                list-style: none;
                margin: 0px 0px;
                padding: 0px 0px;
            }
            .main-navigation > ul > li {
                margin: 0px 20px;
                padding: 0px 0px;
                float: left;
                position: relative;
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                height: 100%;
            }
            .main-navigation > ul > li:before {
                position: absolute;
                height: 3px;
                width: 0;
                bottom: -1px;
                right: 0;
                background: #43baff;
                content: "";
                display: block;
            }
            .main-navigation ul ul.sub-menu {
                float: none;
                margin: 0px 0px;
                padding: 0px 0px;
                background-color: #fff;
                box-shadow: 15px 15px 38px 0px rgba(0, 0, 0, 0.1);
                -webkit-box-shadow: 15px 15px 38px 0px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 15px 15px 38px 0px rgba(0, 0, 0, 0.1);
            }
            .main-navigation ul > li > a {
                display: inline-block;
                padding: 32px 0px;
                line-height: 35px;
                text-decoration: none;
                text-align: center;
                outline: none;
                color: #1a1b1e;
                white-space: nowrap;
            }
            .main-navigation ul > li.menu-item-has-children > a {
                padding-right: 17px;
            }
            .main-navigation ul li li {
                display: block;
                position: relative;
                padding: 0px 40px;
            }
            .main-navigation ul li li a {
                font-size: 14px;
                line-height: 30px;
                font-weight: bold;
                color: #1a1b1e;
                text-align: left;
                display: block;
                padding: 5px 0px 5px 0px;
                position: relative;
                text-decoration: none;
                outline: none;
                text-transform: uppercase;
            }
            .main-navigation:not(.no-line) ul li li a:before {
                position: absolute;
                content: "" !important;
                top: 50%;
                left: 0;
                color: #43baff;
                -webkit-transform: translateY(-50%);
                -moz-transform: translateY(-50%);
                transform: translateY(-50%);
                visibility: hidden;
                opacity: 0;
            }
            .main-navigation ul > li.menu-item-has-children > a:after {
                position: absolute;
                right: 2px;
                top: 50%;
                margin-top: -4px;
                font-family: "Flaticon";
                content: "";
                font-size: 7px;
                line-height: 1;
                color: #c3c7c9;
                font-weight: 500;
                transform: rotate(90deg);
            }
            .main-navigation ul > li li.menu-item-has-children > a:after {
                position: absolute;
                content: "";
                font-family: Flaticon;
                top: 50%;
                right: 0;
                margin-top: -5px;
                font-size: 9px;
                color: #c3c7c9;
                transform: rotate(0deg);
            }
            .main-navigation ul li ul.sub-menu {
                min-width: 280px;
                white-space: nowrap;
                padding: 25px 0px;
                position: absolute;
                top: calc(100% + 1px);
                left: -40px;
                z-index: 10;
                visibility: hidden;
                opacity: 0;
                -webkit-transform: translateY(30px);
                -ms-transform: translateY(30px);
                transform: translateY(30px);
                box-sizing: border-box;
            }
            .main-navigation ul li ul ul.sub-menu {
                left: 100%;
                top: -25px;
            }
            .octf-btn-cta {
                display: inline-flex;
                vertical-align: middle;
                position: relative;
            }
            .octf-btn-cta .h-search-form-field {
                display: none;
                position: absolute;
                z-index: 99;
                top: -webkit-calc(100% + 1px);
                top: expression(100% + 1px);
                top: -moz-calc(100% + 1px);
                top: -o-calc(100% + 1px);
                top: calc(100% + 1px);
                right: -30px;
                width: 330px;
            }
            .octf-btn-cta .h-search-form-field .h-search-form-inner {
                padding: 30px;
                background-color: #fff;
                box-shadow: 15px 15px 34px 0px rgba(0, 0, 0, 0.1);
                -webkit-box-shadow: 15px 15px 34px 0px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 15px 15px 34px 0px rgba(0, 0, 0, 0.1);
            }
            .octf-btn-cta .octf-cta-icons i {
                color: #1b1d21;
                text-align: center;
                display: inline-block;
                vertical-align: middle;
                min-width: 22px;
            }
            .octf-btn-cta .octf-cta-icons i:before,
            .octf-btn-cta .octf-cta-icons i:after {
                font-size: 22px;
            }
            .site-header-cart {
                position: absolute;
                visibility: hidden;
                opacity: 0;
                z-index: 10;
                top: -webkit-calc(100% + 1px);
                top: expression(100% + 1px);
                top: -moz-calc(100% + 1px);
                top: -o-calc(100% + 1px);
                top: calc(100% + 1px);
                right: -30px;
                background: #fff;
                width: 330px;
                padding: 30px;
                box-shadow: 8px 8px 30px 0px rgba(42, 67, 113, 0.15);
                -webkit-box-shadow: 8px 8px 30px 0px rgba(42, 67, 113, 0.15);
                -moz-box-shadow: 8px 8px 30px 0px rgba(42, 67, 113, 0.15);
                -webkit-transform: translateY(30px);
                -ms-transform: translateY(30px);
                transform: translateY(30px);
            }
            .site-header-cart .widget_shopping_cart_content {
                text-align: left;
            }
            @media (max-width: 1199px) {
                .main-navigation > ul > li {
                    margin: 0 14px;
                }
            }
            @media only screen and (min-width: 1025px) {
                .header-mobile {
                    display: none;
                }
            }
            @media only screen and (max-width: 1024px) {
                .header-desktop {
                    display: none;
                }
            }
            #mmenu-toggle button {
                position: absolute;
                left: 0;
                top: 50%;
                margin: -2px 0 0;
                background: #1b1d21;
                height: 3px;
                padding: 0;
                border: none;
                width: 26px;
                outline: none;
            }
            #mmenu-toggle button:before {
                content: "";
                position: absolute;
                left: 0;
                top: -8px;
                width: 26px;
                height: 3px;
                background: #1b1d21;
                -webkit-transform-origin: 1.5px center;
                transform-origin: 1.5px center;
            }
            #mmenu-toggle button:after {
                content: "";
                position: absolute;
                left: 0;
                bottom: -8px;
                width: 26px;
                height: 3px;
                background: #1b1d21;
                -webkit-transform-origin: 1.5px center;
                transform-origin: 1.5px center;
            }
            .page-header {
                width: 100%;
                min-height: 350px;
                color: #43baff;
                font-weight: 500;
                background: #262051 center center no-repeat;
                background-size: cover;
            }
            .page-header .page-title {
                color: #fff;
                margin-bottom: 0;
                flex: 1;
                padding: 10px 20px 10px 0;
            }
            .page-header .breadcrumbs {
                margin-bottom: 0;
                font-size: 14px;
                text-transform: uppercase;
                font-weight: 800;
            }
            .page-header .breadcrumbs li {
                display: inline-block;
                color: #fff;
            }
            .page-header .breadcrumbs li:before {
                content: "";
                font-family: "FontAwesome";
                font-size: 7px;
                color: #43baff;
                margin: -3px 8px 0;
                display: inline-block;
                vertical-align: middle;
            }
            .page-header .breadcrumbs li:first-child:before {
                display: none;
            }
            .page-header .breadcrumbs li a {
                color: #aeaacb;
            }
            @media (max-width: 992px) {
                .page-header .inner {
                    display: block;
                }
            }
            @media (max-width: 767px) {
                .page-header .container {
                    width: 100%;
                }
                .page-header .page-title {
                    font-size: 36px;
                }
                .page-header .breadcrumbs {
                    font-size: 12px;
                }
            }
            @media (max-width: 600px) {
                .page-header {
                    min-height: 200px;
                    padding: 40px 0;
                }
                .page-header .page-title {
                    font-size: 30px;
                }
            }
            .screen-reader-text {
                border: 0;
                clip: rect(1px, 1px, 1px, 1px);
                clip-path: inset(50%);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute !important;
                width: 1px;
                word-wrap: normal !important;
            }
            .entry-content:before,
            .entry-content:after,
            .site-header:before,
            .site-header:after,
            .site-content:before,
            .site-content:after {
                content: "";
                display: table;
                table-layout: fixed;
            }
            .entry-content:after,
            .site-header:after,
            .site-content:after {
                clear: both;
            }
            .entry-content {
                padding: 90px 0 110px;
            }
            .search-form {
                position: relative;
            }
            .search-form .search-field {
                width: 100%;
                padding: 0 66px 0 20px;
                height: 46px;
                line-height: 1;
                border-color: #e6e6e6;
            }
            .search-form .search-submit {
                border: none;
                position: absolute;
                top: 0;
                right: 0;
                height: 100%;
                width: 46px;
                outline: none;
                text-align: center;
                vertical-align: middle;
                color: #b5b5b5;
                background: #43baff;
            }
            .search-form .search-submit i {
                color: #fff;
            }
            .search-form .search-submit i:before,
            .search-form .search-submit i:after {
                font-size: 16px;
            }
            .gallery {
                margin-bottom: 1.5em;
                margin-left: -7px;
                margin-right: -7px;
            }
            .gallery-item {
                display: inline-block;
                text-align: center;
                vertical-align: top;
                width: 100%;
                margin: 0;
                padding: 7px;
            }
            .gallery-columns-3 .gallery-item {
                max-width: 33.33%;
            }
            body .elementor-widget:not(:last-child) {
                margin-bottom: 0;
            }
            .elementor-text-editor p:last-child {
                margin-bottom: 0;
            }
            div.elementor-widget-heading.elementor-widget-heading .elementor-heading-title {
                color: #1b1d21;
                font-family: "Montserrat", sans-serif;
                font-weight: 800;
            }
            .elementor-widget-heading h4.elementor-heading-title {
                font-size: 24px;
            }
            div.elementor-widget-heading.elementor-widget-heading h4.elementor-heading-title {
                font-weight: bold;
            }
            .elementor .elementor-section.elementor-section-boxed > .elementor-container {
                padding: 0 15px;
            }
            .elementor .elementor-section .elementor-container {
                display: block;
            }
            .elementor .elementor-section .elementor-container > .elementor-row {
                width: auto;
            }
            .elementor-section > .elementor-column-gap-default > .elementor-row > .elementor-column > .elementor-element-populated,
            .elementor-section > .elementor-column-gap-extended > .elementor-row > .elementor-column > .elementor-element-populated {
                padding-top: 0;
                padding-bottom: 0;
            }
            .elementor .elementor-section-full_width:not(.elementor-inner-section) > .elementor-column-gap-extended > .elementor-row {
                margin-left: 0;
                margin-right: 0;
            }
            .elementor .elementor-section > .elementor-column-gap-extended > .elementor-row {
                margin-left: -15px;
                margin-right: -15px;
            }
            .elementor .elementor-section > .elementor-column-gap-default > .elementor-row {
                margin-left: -10px;
                margin-right: -10px;
            }
            .elementor .elementor-section-full_width:not(.elementor-inner-section) > .elementor-column-gap-extended > .elementor-row {
                margin-left: 0;
                margin-right: 0;
            }
            .ot-flex-column-horizontal .elementor-widget-wrap {
                flex-wrap: nowrap;
            }
            .ot-flex-column-horizontal .elementor-widget-wrap > .elementor-widget {
                display: flex;
                width: auto;
            }
            @media (min-width: 768px) {
                .elementor .elementor-section.elementor-section-boxed > .elementor-container {
                    max-width: 750px;
                }
            }
            @media (min-width: 992px) {
                .elementor .elementor-section.elementor-section-boxed > .elementor-container {
                    max-width: 970px;
                }
            }
            .octf-search,
            .octf-cta-header {
                position: relative;
                height: 100%;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                align-items: center;
            }
            .octf-cart {
                position: relative;
            }
            .ot-minicart {
                position: relative;
                display: inline-block;
                color: #1b1d21;
            }
            .ot-minicart:visited {
                color: #1b1d21;
            }
            .ot-minicart i:before {
                font-size: 24px;
            }
            .ot-minicart .count {
                position: absolute;
                bottom: 0;
                left: -7px;
                width: 20px;
                height: 20px;
                line-height: 21px;
                background: #43baff;
                color: #fff;
                text-align: center;
                border-radius: 50%;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                font-size: 11px;
                font-weight: bold;
            }
            @media only screen and (max-width: 1024px) {
                .site-header-cart {
                    display: none;
                }
            }
            .header-mobile {
                display: none;
            }
            .octf-menu-mobile {
                position: relative;
            }
            .mmenu-toggle {
                width: 26px;
                height: 20px;
            }
            .mmenu-toggle button {
                background: none;
                border: none;
                outline: none;
                padding: 0;
                color: #1b1d21;
            }
            .mmenu-wrapper {
                position: fixed;
                top: 0;
                bottom: 0;
                right: 0;
                width: 310px;
                height: 100vh;
                background: #fff;
                overflow-x: hidden;
                z-index: 9999;
                -webkit-transform: translateX(100%);
                -ms-transform: translateX(100%);
                transform: translateX(100%);
            }
            .mmenu-wrapper .mmenu-inner {
                top: 0;
                height: 100%;
                width: 327px;
                padding: 20px 52px 35px 35px;
                overflow-x: hidden;
                overflow-y: auto;
                box-sizing: border-box;
            }
            .mmenu-wrapper .mmenu-close {
                display: block;
                margin-bottom: 10px;
                color: #1b1d21;
                font-size: 18px;
            }
            .mmenu-wrapper .mobile_mainmenu {
                margin: 0;
                padding: 0;
                font-weight: 600;
            }
            .mmenu-wrapper .mobile-nav {
                min-width: 240px;
            }
            .mmenu-wrapper .mobile_mainmenu ul {
                position: relative;
                margin: 0;
                padding: 0;
                margin-left: 15px;
            }
            .mmenu-wrapper .mobile_mainmenu ul {
                display: none;
            }
            .mmenu-wrapper .mobile_mainmenu li {
                position: relative;
                list-style: none;
            }
            .mmenu-wrapper .mobile_mainmenu li a {
                padding: 10px 30px 10px 0;
                display: block;
                font-size: 14px;
                color: #1b1d21;
                border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            }
            .site-overlay {
                background: transparent;
                width: 100vw;
                height: 100vh;
                position: fixed;
                top: 0;
                left: 0;
                visibility: hidden;
                opacity: 0;
                z-index: -1;
            }
            @media only screen and (max-width: 1024px) {
                .header-desktop {
                    display: none;
                }
                .header-mobile {
                    display: block;
                }
            }
            .side-panel {
                width: 400px;
                position: fixed;
                top: 0;
                bottom: 0;
                right: 0;
                background: #fff;
                overflow-x: hidden;
                z-index: 9999;
                -webkit-transform: translateX(100%);
                -ms-transform: translateX(100%);
                transform: translateX(100%);
            }
            .side-panel .side-panel-block {
                height: 100%;
                width: calc(100% + 17px);
                overflow-x: hidden;
                overflow-y: auto;
                box-sizing: border-box;
            }
            .side-panel .side-panel-close {
                display: block;
                position: absolute;
                right: 30px;
                top: 30px;
                color: #1b1d21;
                font-size: 20px;
                width: 50px;
                height: 50px;
                line-height: 50px;
                font-size: 16px;
                z-index: 9999;
                background: #f5f5f5;
                text-align: center;
            }
            .side-panel .side-panel-close i:before {
                font-size: 16px;
            }
            #back-to-top {
                background: #fff;
                color: #43baff;
                border: 0 none;
                border-radius: 2px;
                width: 42px;
                height: 45px;
                line-height: 45px;
                opacity: 0;
                outline: medium none;
                position: fixed;
                right: 40px;
                bottom: -20px;
                text-align: center;
                text-decoration: none;
                z-index: 1000;
                box-shadow: 6px 6px 13px 0px rgba(42, 67, 113, 0.2);
                -webkit-box-shadow: 6px 6px 13px 0px rgba(42, 67, 113, 0.2);
                -moz-box-shadow: 6px 6px 13px 0px rgba(42, 67, 113, 0.2);
            }
            #back-to-top.show {
                opacity: 1;
                bottom: 40px;
            }
            #back-to-top i:after,
            #back-to-top i:before {
                font-size: 17px;
            }
            body.royal_preloader {
                background: none;
                visibility: hidden;
            }
            #royal_preloader {
                visibility: visible;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                height: auto;
                margin: 0;
                z-index: 9999999999;
            }
            .woocommerce form .form-row .input-text,
            .woocommerce-page form .form-row .input-text {
                width: 300px;
                padding: 20px;
                height: 47px;
                line-height: 47px;
                margin: 0 10px 0 0;
                color: #7e7e7e;
            }
            .elementor-column-gap-default > .elementor-row > .elementor-column > .elementor-element-populated {
                padding: 10px;
            }
            .elementor-column-gap-extended > .elementor-row > .elementor-column > .elementor-element-populated {
                padding: 15px;
            }
            @media (max-width: 767px) {
                .elementor-column {
                    width: 100%;
                }
            }
            .elementor-screen-only,
            .screen-reader-text {
                position: absolute;
                top: -10000em;
                width: 1px;
                height: 1px;
                margin: -1px;
                padding: 0;
                overflow: hidden;
                clip: rect(0, 0, 0, 0);
                border: 0;
            }
            .elementor-clearfix:after {
                content: "";
                display: block;
                clear: both;
                width: 0;
                height: 0;
            }
            .elementor {
                -webkit-hyphens: manual;
                -ms-hyphens: manual;
                hyphens: manual;
            }
            .elementor *,
            .elementor :after,
            .elementor :before {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .elementor a {
                -webkit-box-shadow: none;
                box-shadow: none;
                text-decoration: none;
            }
            .elementor img {
                height: auto;
                max-width: 100%;
                border: none;
                -webkit-border-radius: 0;
                border-radius: 0;
                -webkit-box-shadow: none;
                box-shadow: none;
            }
            .elementor .elementor-widget:not(.elementor-widget-text-editor):not(.elementor-widget-theme-post-content) figure {
                margin: 0;
            }
            .elementor-align-left {
                text-align: left;
            }
            :root {
                --page-title-display: block;
            }
            .elementor-section {
                position: relative;
            }
            .elementor-section .elementor-container {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                margin-right: auto;
                margin-left: auto;
                position: relative;
            }
            @media (max-width: 1024px) {
                .elementor-section .elementor-container {
                    -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                }
            }
            .elementor-section.elementor-section-boxed > .elementor-container {
                max-width: 1140px;
            }
            .elementor-row {
                width: 100%;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }
            @media (max-width: 1024px) {
                .elementor-row {
                    -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                }
            }
            .elementor-widget-wrap {
                position: relative;
                width: 100%;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -ms-flex-line-pack: start;
                align-content: flex-start;
            }
            .elementor:not(.elementor-bc-flex-widget) .elementor-widget-wrap {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }
            .elementor-widget-wrap > .elementor-element {
                width: 100%;
            }
            .elementor-widget {
                position: relative;
            }
            .elementor-widget:not(:last-child) {
                margin-bottom: 20px;
            }
            .elementor-column {
                min-height: 1px;
            }
            .elementor-column,
            .elementor-column-wrap {
                position: relative;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }
            .elementor-column-wrap {
                width: 100%;
            }
            @media (min-width: 768px) {
                .elementor-column.elementor-col-25 {
                    width: 25%;
                }
                .elementor-column.elementor-col-50 {
                    width: 50%;
                }
                .elementor-column.elementor-col-100 {
                    width: 100%;
                }
            }
            @media (max-width: 767px) {
                .elementor-column {
                    width: 100%;
                }
            }
            ul.elementor-icon-list-items.elementor-inline-items {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }
            ul.elementor-icon-list-items.elementor-inline-items .elementor-inline-item {
                word-break: break-word;
            }
            .elementor-grid .elementor-grid-item {
                word-break: break-word;
                min-width: 0;
            }
            .elementor-grid-4 .elementor-grid {
                display: grid;
                grid-column-gap: var(--grid-column-gap);
                grid-row-gap: var(--grid-row-gap);
            }
            .elementor-grid-4 .elementor-grid {
                grid-template-columns: repeat(4, 1fr);
            }
            .elementor-image-gallery .gallery-item {
                display: inline-block;
                text-align: center;
                vertical-align: top;
                width: 100%;
                max-width: 100%;
                margin: 0 auto;
            }
            .elementor-image-gallery .gallery-item img {
                margin: 0 auto;
            }
            @media (min-width: 768px) {
                .elementor-image-gallery .gallery-columns-3 .gallery-item {
                    max-width: 33.33%;
                }
            }
            @media (min-width: 480px) and (max-width: 767px) {
                .elementor-image-gallery .gallery.gallery-columns-3 .gallery-item {
                    max-width: 50%;
                }
            }
            @media (max-width: 479px) {
                .elementor-image-gallery .gallery.gallery-columns-3 .gallery-item {
                    max-width: 100%;
                }
            }
            .elementor-heading-title {
                padding: 0;
                margin: 0;
                line-height: 1;
            }
            .elementor-icon {
                display: inline-block;
                line-height: 1;
                color: #818a91;
                font-size: 50px;
                text-align: center;
            }
            .elementor-icon i,
            .elementor-icon svg {
                width: 1em;
                height: 1em;
                position: relative;
                display: block;
            }
            .elementor-icon i:before,
            .elementor-icon svg:before {
                position: absolute;
                left: 50%;
                -webkit-transform: translateX(-50%);
                -ms-transform: translateX(-50%);
                transform: translateX(-50%);
            }
            .elementor-shape-circle .elementor-icon {
                -webkit-border-radius: 50%;
                border-radius: 50%;
            }
            @media (min-width: 768px) {
                .elementor-widget-icon-box.elementor-position-left .elementor-icon-box-wrapper {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                }
                .elementor-widget-icon-box.elementor-position-left .elementor-icon-box-icon {
                    display: -webkit-inline-box;
                    display: -ms-inline-flexbox;
                    display: inline-flex;
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 auto;
                    flex: 0 0 auto;
                }
                .elementor-widget-icon-box.elementor-position-left .elementor-icon-box-wrapper {
                    text-align: left;
                    -webkit-box-orient: horizontal;
                    -webkit-box-direction: normal;
                    -ms-flex-direction: row;
                    flex-direction: row;
                }
                .elementor-widget-icon-box.elementor-vertical-align-middle .elementor-icon-box-wrapper {
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                }
            }
            @media (max-width: 767px) {
                .elementor-widget-icon-box .elementor-icon-box-icon {
                    margin-left: auto !important;
                    margin-right: auto !important;
                    margin-bottom: 15px;
                }
            }
            .elementor-widget-icon-box .elementor-icon-box-wrapper {
                text-align: center;
            }
            .elementor-widget-icon-box .elementor-icon-box-content {
                -webkit-box-flex: 1;
                -ms-flex-positive: 1;
                flex-grow: 1;
            }
            .elementor-widget-icon-box .elementor-icon-box-description {
                margin: 0;
            }
            .elementor-widget.elementor-icon-list--layout-inline .elementor-widget-container {
                overflow: hidden;
            }
            .elementor-widget .elementor-icon-list-items.elementor-inline-items {
                margin-right: -8px;
                margin-left: -8px;
            }
            .elementor-widget .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item {
                margin-right: 8px;
                margin-left: 8px;
            }
            .elementor-widget .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item:after {
                width: auto;
                left: auto;
                right: auto;
                position: relative;
                height: 100%;
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                border-top: 0;
                border-bottom: 0;
                border-right: 0;
                border-left-width: 1px;
                border-style: solid;
                right: -8px;
            }
            .elementor-widget .elementor-icon-list-items {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }
            .elementor-widget .elementor-icon-list-item {
                margin: 0;
                padding: 0;
                position: relative;
            }
            .elementor-widget .elementor-icon-list-item:after {
                position: absolute;
                bottom: 0;
                width: 100%;
            }
            .elementor-widget .elementor-icon-list-item,
            .elementor-widget .elementor-icon-list-item a {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: start;
                -ms-flex-align: start;
                align-items: flex-start;
            }
            .elementor-widget .elementor-icon-list-icon + .elementor-icon-list-text {
                -ms-flex-item-align: center;
                align-self: center;
                padding-left: 5px;
            }
            .elementor-widget .elementor-icon-list-icon {
                -ms-flex-negative: 0;
                flex-shrink: 0;
            }
            .elementor-widget .elementor-icon-list-icon i {
                width: 1.25em;
            }
            .elementor-widget.elementor-list-item-link-full_width a {
                width: 100%;
            }
            .elementor-widget.elementor-align-left .elementor-icon-list-item,
            .elementor-widget.elementor-align-left .elementor-icon-list-item a {
                -webkit-box-pack: start;
                -ms-flex-pack: start;
                justify-content: flex-start;
                text-align: left;
            }
            .elementor-widget.elementor-align-left .elementor-inline-items {
                -webkit-box-pack: start;
                -ms-flex-pack: start;
                justify-content: flex-start;
            }
            .elementor-widget:not(.elementor-align-right) .elementor-icon-list-item:after {
                left: 0;
            }
            @media (max-width: 1024px) {
                .elementor-widget:not(.elementor-tablet-align-right) .elementor-icon-list-item:after {
                    left: 0;
                }
                .elementor-widget:not(.elementor-tablet-align-left) .elementor-icon-list-item:after {
                    right: 0;
                }
            }
            @media (max-width: 767px) {
                .elementor-widget:not(.elementor-mobile-align-right) .elementor-icon-list-item:after {
                    left: 0;
                }
                .elementor-widget:not(.elementor-mobile-align-left) .elementor-icon-list-item:after {
                    right: 0;
                }
            }
            .elementor-image-gallery figure img {
                display: block;
            }
            .elementor-widget-social-icons:not(.elementor-grid-0) .elementor-grid {
                display: inline-grid;
                grid-template-columns: var(--grid-template-columns);
            }
            .elementor-widget-social-icons .elementor-widget-container {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
            }
            .elementor-social-icons-wrapper {
                font-size: 0;
            }
            .elementor-social-icon {
                background-color: #818a91;
                font-size: 25px;
                text-align: center;
                padding: 0.5em;
                margin-right: 5px;
            }
            .elementor-social-icon i {
                color: #fff;
            }
            .elementor-social-icon:last-child {
                margin: 0;
            }
            .elementor-social-icon-facebook-f {
                background-color: #3b5998;
            }
            .elementor-social-icon-instagram {
                background-color: #262626;
            }
            .elementor-social-icon-linkedin-in {
                background-color: #0077b5;
            }
            .elementor-social-icon-twitter {
                background-color: #1da1f2;
            }
            .elementor-shape-circle .elementor-icon.elementor-social-icon {
                -webkit-border-radius: 50%;
                border-radius: 50%;
            }
            .elementor .elementor-element ul.elementor-icon-list-items {
                padding: 0;
            }
            .elementor-2843 .elementor-element.elementor-element-f515cb0 > .elementor-container > .elementor-row > .elementor-column > .elementor-column-wrap > .elementor-widget-wrap {
                align-content: center;
                align-items: center;
            }
            .elementor-2843 .elementor-element.elementor-element-f515cb0:not(.elementor-motion-effects-element-type-background) {
                background-color: #141d38;
            }
            .elementor-2843 .elementor-element.elementor-element-f515cb0 {
                padding: 0px 50px 0px 50px;
            }
            .elementor-2843 .elementor-element.elementor-element-2416e109.elementor-column.elementor-element[data-element_type="column"] > .elementor-column-wrap.elementor-element-populated > .elementor-widget-wrap {
                align-content: center;
                align-items: center;
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item {
                margin-right: calc(40px / 2);
                margin-left: calc(40px / 2);
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-items.elementor-inline-items {
                margin-right: calc(-40px / 2);
                margin-left: calc(-40px / 2);
            }
            body:not(.rtl) .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item:after {
                right: calc(-40px / 2);
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-icon i {
                color: #ffffff;
                font-size: 13px;
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-icon {
                text-align: left;
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-text {
                color: #b5b5b5;
                padding-left: 7px;
            }
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-item,
            .elementor-2843 .elementor-element.elementor-element-2081dfd7 .elementor-icon-list-item a {
                font-family: "Nunito Sans", Sans-serif;
                font-size: 14px;
                font-weight: 600;
                line-height: 41px;
            }
            .elementor-2843 .elementor-element.elementor-element-3346d93d.elementor-column > .elementor-column-wrap > .elementor-widget-wrap {
                justify-content: flex-end;
            }
            .elementor-2843 .elementor-element.elementor-element-11e4f792 {
                color: #b5b5b5;
                font-size: 14px;
                font-weight: 400;
            }
            .elementor-2843 .elementor-element.elementor-element-31437729 {
                --grid-template-columns: repeat(4, auto);
                --grid-column-gap: 25px;
                --grid-side-margin: 25px;
            }
            .elementor-2843 .elementor-element.elementor-element-31437729 .elementor-widget-container {
                justify-content: flex-start;
            }
            .elementor-2843 .elementor-element.elementor-element-31437729 .elementor-social-icon {
                background-color: #02010100;
                font-size: 12px;
                padding: 0em;
            }
            .elementor-2843 .elementor-element.elementor-element-31437729 .elementor-social-icon i {
                color: #b5b5b5;
            }
            .elementor-2843 .elementor-element.elementor-element-31437729 > .elementor-widget-container {
                margin: 0px 0px 0px 35px;
            }
            .elementor-2843 .elementor-element.elementor-element-755e8132 {
                border-style: solid;
                border-width: 0px 0px 1px 0px;
                border-color: #141d3826;
                padding: 0px 50px 0px 50px;
            }
            .elementor-2843 .elementor-element.elementor-element-62c5fc44.elementor-column.elementor-element[data-element_type="column"] > .elementor-column-wrap.elementor-element-populated > .elementor-widget-wrap {
                align-content: center;
                align-items: center;
            }
            .elementor-2843 .elementor-element.elementor-element-62c5fc44 {
                min-width: 175px;
            }
            .elementor-2843 .elementor-element.elementor-element-60d53692 .the-logo img {
                width: 145px;
            }
            .elementor-2843 .elementor-element.elementor-element-1534b56a.elementor-column > .elementor-column-wrap > .elementor-widget-wrap {
                justify-content: flex-end;
            }
            .elementor-2843 .elementor-element.elementor-element-1534b56a {
                min-width: 100px;
            }
            .elementor-2843 .elementor-element.elementor-element-22d1ee74 .toggle-search i {
                color: #1b1d21;
            }
            .elementor-2843 .elementor-element.elementor-element-22d1ee74 > .elementor-widget-container {
                margin: 0px 10px 0px 28px;
            }
            .elementor-2843 .elementor-element.elementor-element-2d3490c6.elementor-column.elementor-element[data-element_type="column"] > .elementor-column-wrap.elementor-element-populated > .elementor-widget-wrap {
                align-content: center;
                align-items: center;
            }
            .elementor-2843 .elementor-element.elementor-element-2d3490c6.elementor-column > .elementor-column-wrap > .elementor-widget-wrap {
                justify-content: flex-end;
            }
            .elementor-2843 .elementor-element.elementor-element-2d3490c6 {
                min-width: 445px;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf.elementor-view-default .elementor-icon {
                fill: #43baff;
                color: #43baff;
                border-color: #43baff;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf.elementor-position-left .elementor-icon-box-icon {
                margin-right: 13px;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon {
                font-size: 22px;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon-box-title {
                margin-bottom: 0px;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon-box-content .elementor-icon-box-title {
                color: #6d6d6d;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon-box-content .elementor-icon-box-title {
                font-family: "Nunito", Sans-serif;
                font-size: 14px;
                font-weight: 600;
                line-height: 1.6em;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon-box-content .elementor-icon-box-description {
                color: #1b1d21;
                font-family: "Montserrat", Sans-serif;
                font-size: 16px;
                font-weight: 600;
            }
            .elementor-2843 .elementor-element.elementor-element-1bd34bdf > .elementor-widget-container {
                padding: 0px 0px 0px 40px;
                border-style: solid;
                border-width: 0px 0px 0px 1px;
                border-color: #141d383b;
            }
            .elementor-2843 .elementor-element.elementor-element-11482d6c > .elementor-widget-container {
                margin: 0px 0px 0px 40px;
            }
            @media (max-width: 767px) {
                .elementor-2843 .elementor-element.elementor-element-1bd34bdf .elementor-icon-box-icon {
                    margin-bottom: 13px;
                }
            }
            @media (min-width: 768px) {
                .elementor-2843 .elementor-element.elementor-element-2416e109 {
                    width: 39%;
                }
                .elementor-2843 .elementor-element.elementor-element-3346d93d {
                    width: 61%;
                }
                .elementor-2843 .elementor-element.elementor-element-62c5fc44 {
                    width: 18.487%;
                }
                .elementor-2843 .elementor-element.elementor-element-224525d5 {
                    width: 53.618%;
                }
                .elementor-2843 .elementor-element.elementor-element-1534b56a {
                    width: 6.316%;
                }
                .elementor-2843 .elementor-element.elementor-element-2d3490c6 {
                    width: 21.579%;
                }
            }
            .elementor-2854 .elementor-element.elementor-element-55212198:not(.elementor-motion-effects-element-type-background) {
                background-color: #ffffff;
            }
            .elementor-2854 .elementor-element.elementor-element-55212198 {
                border-style: solid;
                border-width: 0px 0px 1px 0px;
                border-color: #141d3826;
            }
            .elementor-2854 .elementor-element.elementor-element-524c330e .the-logo img {
                width: 145px;
            }
            .elementor-2854 .elementor-element.elementor-element-524c330e > .elementor-widget-container {
                padding: 13px 0px 13px 0px;
            }
            .elementor-2854 .elementor-element.elementor-element-2df0dc0f.elementor-column > .elementor-column-wrap > .elementor-widget-wrap {
                justify-content: flex-end;
            }
            .elementor-2854 .elementor-element.elementor-element-6b4d4c31 .toggle-search i {
                color: #1b1d21;
            }
            .elementor-2854 .elementor-element.elementor-element-6b4d4c31 > .elementor-widget-container {
                margin: 0px 0px 0px 25px;
            }
            .elementor-2854 .elementor-element.elementor-element-38196644 > .elementor-widget-container {
                margin: 0px 0px 0px 25px;
            }
            @media (max-width: 767px) {
                .elementor-2854 .elementor-element.elementor-element-4cdb5f0b {
                    width: 60%;
                }
                .elementor-2854 .elementor-element.elementor-element-2df0dc0f {
                    width: 40%;
                }
            }
            .elementor-2856 .elementor-element.elementor-element-97bff10 {
                padding: 40px 40px 40px 40px;
            }
            .elementor-2856 .elementor-element.elementor-element-5b0ca0a2 .the-logo img {
                width: 150px;
            }
            .elementor-2856 .elementor-element.elementor-element-5b0ca0a2 > .elementor-widget-container {
                padding: 0px 0px 30px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-10eebe7 > .elementor-widget-container {
                padding: 0px 0px 20px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-36f1ca99 > .elementor-widget-container {
                padding: 0px 0px 15px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-5f98745a > .elementor-widget-container {
                padding: 0px 0px 25px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395.elementor-view-default .elementor-icon {
                fill: #43baff;
                color: #43baff;
                border-color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395.elementor-position-left .elementor-icon-box-icon {
                margin-right: 15px;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon {
                font-size: 21px;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon i {
                transform: rotate(0deg);
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon-box-title {
                margin-bottom: 2px;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon-box-content .elementor-icon-box-title {
                color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon-box-content .elementor-icon-box-description {
                color: #606060;
                font-family: "Nunito Sans", Sans-serif;
                font-weight: 600;
            }
            .elementor-2856 .elementor-element.elementor-element-101d8395 > .elementor-widget-container {
                padding: 0px 0px 15px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7.elementor-view-default .elementor-icon {
                fill: #43baff;
                color: #43baff;
                border-color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7.elementor-position-left .elementor-icon-box-icon {
                margin-right: 15px;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon {
                font-size: 21px;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon i {
                transform: rotate(0deg);
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon-box-title {
                margin-bottom: 2px;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon-box-content .elementor-icon-box-title {
                color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon-box-content .elementor-icon-box-description {
                color: #606060;
                font-family: "Nunito Sans", Sans-serif;
                font-weight: 600;
            }
            .elementor-2856 .elementor-element.elementor-element-3c6a17b7 > .elementor-widget-container {
                padding: 0px 0px 15px 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad.elementor-view-default .elementor-icon {
                fill: #43baff;
                color: #43baff;
                border-color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad.elementor-position-left .elementor-icon-box-icon {
                margin-right: 15px;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon {
                font-size: 21px;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon i {
                transform: rotate(0deg);
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon-box-title {
                margin-bottom: 2px;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon-box-content .elementor-icon-box-title {
                color: #43baff;
            }
            .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon-box-content .elementor-icon-box-description {
                color: #606060;
                font-family: "Nunito Sans", Sans-serif;
                font-weight: 600;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-repeater-item-a24d46e.elementor-social-icon {
                background-color: #4ccef9;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-repeater-item-4261f62.elementor-social-icon {
                background-color: #4661c5;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-repeater-item-b0dbeaa.elementor-social-icon {
                background-color: #ff2e2e;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-repeater-item-6b456a7.elementor-social-icon {
                background-color: #ff753b;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde {
                --grid-template-columns: repeat(4, auto);
                --grid-column-gap: 10px;
                --grid-side-margin: 10px;
                --grid-row-gap: 0px;
                --grid-bottom-margin: 0px;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-widget-container {
                justify-content: flex-start;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde .elementor-social-icon {
                font-size: 13px;
                padding: 0.6em;
            }
            .elementor-2856 .elementor-element.elementor-element-15ddcfde > .elementor-widget-container {
                padding: 30px 0px 0px 0px;
            }
            @media (max-width: 767px) {
                .elementor-2856 .elementor-element.elementor-element-101d8395 .elementor-icon-box-icon {
                    margin-bottom: 15px;
                }
                .elementor-2856 .elementor-element.elementor-element-3c6a17b7 .elementor-icon-box-icon {
                    margin-bottom: 15px;
                }
                .elementor-2856 .elementor-element.elementor-element-38207ad .elementor-icon-box-icon {
                    margin-bottom: 15px;
                }
            }
            .elementor-kit-2821 {
                --e-global-color-primary: #6ec1e4;
                --e-global-color-secondary: #54595f;
                --e-global-color-text: #7a7a7a;
                --e-global-color-accent: #61ce70;
                --e-global-color-443ecd79: #4054b2;
                --e-global-color-66db0de: #23a455;
                --e-global-color-65c8bd80: #000;
                --e-global-color-2125187a: #fff;
                --e-global-typography-primary-font-family: "Roboto";
                --e-global-typography-primary-font-weight: 600;
                --e-global-typography-secondary-font-family: "Roboto Slab";
                --e-global-typography-secondary-font-weight: 400;
                --e-global-typography-text-font-family: "Nunito Sans";
                --e-global-typography-text-font-weight: 400;
                --e-global-typography-accent-font-family: "Roboto";
                --e-global-typography-accent-font-weight: 500;
            }
            .elementor-section.elementor-section-boxed > .elementor-container {
                max-width: 1140px;
            }
            @media (max-width: 1024px) {
                .elementor-section.elementor-section-boxed > .elementor-container {
                    max-width: 1025px;
                }
            }
            @media (max-width: 767px) {
                .elementor-section.elementor-section-boxed > .elementor-container {
                    max-width: 768px;
                }
            }
            .elementor-widget-heading .elementor-heading-title {
                color: var(--e-global-color-primary);
                font-family: var(--e-global-typography-primary-font-family);
                font-weight: var(--e-global-typography-primary-font-weight);
            }
            .elementor-widget-text-editor {
                color: var(--e-global-color-text);
                font-family: var(--e-global-typography-text-font-family);
                font-weight: var(--e-global-typography-text-font-weight);
            }
            .elementor-widget-icon-box.elementor-view-default .elementor-icon {
                fill: var(--e-global-color-primary);
                color: var(--e-global-color-primary);
                border-color: var(--e-global-color-primary);
            }
            .elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-title {
                color: var(--e-global-color-primary);
            }
            .elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-title {
                font-family: var(--e-global-typography-primary-font-family);
                font-weight: var(--e-global-typography-primary-font-weight);
            }
            .elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-description {
                color: var(--e-global-color-text);
                font-family: var(--e-global-typography-text-font-family);
                font-weight: var(--e-global-typography-text-font-weight);
            }
            .elementor-widget-icon-list .elementor-icon-list-item:not(:last-child):after {
                border-color: var(--e-global-color-text);
            }
            .elementor-widget-icon-list .elementor-icon-list-icon i {
                color: var(--e-global-color-primary);
            }
            .elementor-widget-icon-list .elementor-icon-list-text {
                color: var(--e-global-color-secondary);
            }
            .elementor-widget-icon-list .elementor-icon-list-item,
            .elementor-widget-icon-list .elementor-icon-list-item a {
                font-family: var(--e-global-typography-text-font-family);
                font-weight: var(--e-global-typography-text-font-weight);
            }
            .fab,
            .fas {
                -moz-osx-font-smoothing: grayscale;
                -webkit-font-smoothing: antialiased;
                display: inline-block;
                font-style: normal;
                font-variant: normal;
                text-rendering: auto;
                line-height: 1;
            }
            .fa-clock:before {
                content: "\f017";
            }
            .fa-envelope:before {
                content: "\f0e0";
            }
            .fa-facebook-f:before {
                content: "\f39e";
            }
            .fa-instagram:before {
                content: "\f16d";
            }
            .fa-linkedin-in:before {
                content: "\f0e1";
            }
            .fa-map-marker-alt:before {
                content: "\f3c5";
            }
            .fa-phone-alt:before {
                content: "\f879";
            }
            .fa-pinterest-p:before {
                content: "\f231";
            }
            .fa-twitter:before {
                content: "\f099";
            }
            @font-face {
                font-family: "Font Awesome 5 Free";
                font-style: normal;
                font-weight: 900;
                font-display: auto;
                src: url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.eot);
                src: url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.eot?#iefix) format("embedded-opentype"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.woff2) format("woff2"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.woff) format("woff"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.ttf) format("truetype"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-solid-900.svg#fontawesome) format("svg");
            }
            .fas {
                font-family: "Font Awesome 5 Free";
                font-weight: 900;
            }
            @font-face {
                font-family: "Font Awesome 5 Brands";
                font-style: normal;
                font-weight: normal;
                font-display: auto;
                src: url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.eot);
                src: url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.eot?#iefix) format("embedded-opentype"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.woff2) format("woff2"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.woff) format("woff"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.ttf) format("truetype"),
                    url(http://wpdemo.archiwp.com/engitech/wp-content/plugins/elementor/assets/lib/font-awesome/webfonts/fa-brands-400.svg#fontawesome) format("svg");
            }
            .fab {
                font-family: "Font Awesome 5 Brands";
            }
        </style>
        <link
            rel="preload"
            href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100i%2C200%2C200i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CNunito%20Sans%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%20Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin%2Clatin-ext&#038;display=swap"
            data-rocket-async="style"
            as="style"
            onload="this.onload=null;this.rel='stylesheet'"
        />
        <link rel="preload" href="../wp-content/cache/min/4/7f24009bd438f5be7b9a8d5e8fcd1e8d.css" data-rocket-async="style" as="style" onload="this.onload=null;this.rel='stylesheet'" media="all" data-minify="1" />
        <link rel="dns-prefetch" href="http://fonts.googleapis.com/" />
        <link href="https://fonts.gstatic.com/" crossorigin rel="preconnect" />
        <link rel="alternate" type="application/rss+xml" title="Engitech &raquo; Feed" href="../feed/index.html" />
        <link rel="alternate" type="application/rss+xml" title="Engitech &raquo; Comments Feed" href="../comments/feed/index.html" />
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 0.07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>

        <style id="rs-plugin-settings-inline-css" type="text/css">
            #rs-demo-id {
            }
        </style>

        <link
            rel="preload"
            href="../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen1c9b.css?ver=4.6.1"
            data-rocket-async="style"
            as="style"
            onload="this.onload=null;this.rel='stylesheet'"
            type="text/css"
            media="only screen and (max-width: 768px)"
        />

        <style id="woocommerce-inline-inline-css" type="text/css">
            .woocommerce form .form-row .required {
                visibility: visible;
            }
        </style>

        <script type="text/javascript" src="../wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp" id="jquery-core-js"></script>

        <link rel="https://api.w.org/" href="../wp-json/index.html" />
        <link rel="alternate" type="application/json" href="../wp-json/wp/v2/pages/1579.json" />
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.php?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
        <meta name="generator" content="WordPress 5.5.3" />
        <meta name="generator" content="WooCommerce 4.6.1" />
        <link rel="canonical" href="index.html" />
        <link rel="shortlink" href="../indexade3.html?p=1579" />
        <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed2c2a.json?url=http%3A%2F%2Fwpdemo.archiwp.com%2Fengitech%2Fabout-us%2F" />
        <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embede88c?url=http%3A%2F%2Fwpdemo.archiwp.com%2Fengitech%2Fabout-us%2F&amp;format=xml" />

        <noscript>
            <style>
                .woocommerce-product-gallery {
                    opacity: 1 !important;
                }
            </style>
        </noscript>
        <meta name="generator" content="Powered by Slider Revolution 6.2.23 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <link rel="icon" href="../wp-content/uploads/sites/4/2020/03/favicon.png" sizes="32x32" />
        <link rel="icon" href="../wp-content/uploads/sites/4/2020/03/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon" href="../wp-content/uploads/sites/4/2020/03/favicon.png" />
        <meta name="msapplication-TileImage" content="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/03/favicon.png" />
        <script type="text/javascript">
            function setREVStartSize(e) {
                //window.requestAnimationFrame(function() {
                window.RSIW = window.RSIW === undefined ? window.innerWidth : window.RSIW;
                window.RSIH = window.RSIH === undefined ? window.innerHeight : window.RSIH;
                try {
                    var pw = document.getElementById(e.c).parentNode.offsetWidth,
                        newh;
                    pw = pw === 0 || isNaN(pw) ? window.RSIW : pw;
                    e.tabw = e.tabw === undefined ? 0 : parseInt(e.tabw);
                    e.thumbw = e.thumbw === undefined ? 0 : parseInt(e.thumbw);
                    e.tabh = e.tabh === undefined ? 0 : parseInt(e.tabh);
                    e.thumbh = e.thumbh === undefined ? 0 : parseInt(e.thumbh);
                    e.tabhide = e.tabhide === undefined ? 0 : parseInt(e.tabhide);
                    e.thumbhide = e.thumbhide === undefined ? 0 : parseInt(e.thumbhide);
                    e.mh = e.mh === undefined || e.mh == "" || e.mh === "auto" ? 0 : parseInt(e.mh, 0);
                    if (e.layout === "fullscreen" || e.l === "fullscreen") newh = Math.max(e.mh, window.RSIH);
                    else {
                        e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                        for (var i in e.rl) if (e.gw[i] === undefined || e.gw[i] === 0) e.gw[i] = e.gw[i - 1];
                        e.gh = e.el === undefined || e.el === "" || (Array.isArray(e.el) && e.el.length == 0) ? e.gh : e.el;
                        e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                        for (var i in e.rl) if (e.gh[i] === undefined || e.gh[i] === 0) e.gh[i] = e.gh[i - 1];

                        var nl = new Array(e.rl.length),
                            ix = 0,
                            sl;
                        e.tabw = e.tabhide >= pw ? 0 : e.tabw;
                        e.thumbw = e.thumbhide >= pw ? 0 : e.thumbw;
                        e.tabh = e.tabhide >= pw ? 0 : e.tabh;
                        e.thumbh = e.thumbhide >= pw ? 0 : e.thumbh;
                        for (var i in e.rl) nl[i] = e.rl[i] < window.RSIW ? 0 : e.rl[i];
                        sl = nl[0];
                        for (var i in nl)
                            if (sl > nl[i] && nl[i] > 0) {
                                sl = nl[i];
                                ix = i;
                            }
                        var m = pw > e.gw[ix] + e.tabw + e.thumbw ? 1 : (pw - (e.tabw + e.thumbw)) / e.gw[ix];
                        newh = e.gh[ix] * m + (e.tabh + e.thumbh);
                    }
                    if (window.rs_init_css === undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
                    document.getElementById(e.c).height = newh + "px";
                    window.rs_init_css.innerHTML += "#" + e.c + "_wrapper { height: " + newh + "px }";
                } catch (e) {
                    console.log("Failure at Presize of Slider:" + e);
                }
                //});
            }
        </script>
        <style id="kirki-inline-styles">
            .page-header {
                background-image: url("../wp-content/uploads/sites/4/2019/12/bg-pheader.jpg");
            }
            #royal_preloader.royal_preloader_logo .royal_preloader_percentage {
                font-family: Roboto;
                font-size: 13px;
                font-weight: 400;
                letter-spacing: 2px;
                line-height: 40px;
                text-align: center;
                text-transform: none;
            }
            @media (max-width: 767px) {
            }
            @media (min-width: 768px) and (max-width: 1024px) {
            }
            @media (min-width: 1024px) {
            } /* cyrillic-ext */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu72xMKTU1Kvnz.woff) format("woff");
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu5mxMKTU1Kvnz.woff) format("woff");
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu7mxMKTU1Kvnz.woff) format("woff");
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu4WxMKTU1Kvnz.woff) format("woff");
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu7WxMKTU1Kvnz.woff) format("woff");
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu7GxMKTU1Kvnz.woff) format("woff");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: "Roboto";
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: url(../wp-content/fonts/roboto/KFOmCnqEu92Fr1Mu4mxMKTU1Kg.woff) format("woff");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
        </style>
        <noscript>
            <style id="rocket-lazyload-nojs-css">
                .rll-youtube-player,
                [data-lazy-src] {
                    display: none !important;
                }
            </style>
        </noscript>
        <script>
            /*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
            (function (w) {
                "use strict";
                if (!w.loadCSS) {
                    w.loadCSS = function () {};
                }
                var rp = (loadCSS.relpreload = {});
                rp.support = (function () {
                    var ret;
                    try {
                        ret = w.document.createElement("link").relList.supports("preload");
                    } catch (e) {
                        ret = !1;
                    }
                    return function () {
                        return ret;
                    };
                })();
                rp.bindMediaToggle = function (link) {
                    var finalMedia = link.media || "all";
                    function enableStylesheet() {
                        link.media = finalMedia;
                    }
                    if (link.addEventListener) {
                        link.addEventListener("load", enableStylesheet);
                    } else if (link.attachEvent) {
                        link.attachEvent("onload", enableStylesheet);
                    }
                    setTimeout(function () {
                        link.rel = "stylesheet";
                        link.media = "only x";
                    });
                    setTimeout(enableStylesheet, 3000);
                };
                rp.poly = function () {
                    if (rp.support()) {
                        return;
                    }
                    var links = w.document.getElementsByTagName("link");
                    for (var i = 0; i < links.length; i++) {
                        var link = links[i];
                        if (link.rel === "preload" && link.getAttribute("as") === "style" && !link.getAttribute("data-loadcss")) {
                            link.setAttribute("data-loadcss", !0);
                            rp.bindMediaToggle(link);
                        }
                    }
                };
                if (!rp.support()) {
                    rp.poly();
                    var run = w.setInterval(rp.poly, 500);
                    if (w.addEventListener) {
                        w.addEventListener("load", function () {
                            rp.poly();
                            w.clearInterval(run);
                        });
                    } else if (w.attachEvent) {
                        w.attachEvent("onload", function () {
                            rp.poly();
                            w.clearInterval(run);
                        });
                    }
                }
                if (typeof exports !== "undefined") {
                    exports.loadCSS = loadCSS;
                } else {
                    w.loadCSS = loadCSS;
                }
            })(typeof global !== "undefined" ? global : this);
        </script>
    </head>