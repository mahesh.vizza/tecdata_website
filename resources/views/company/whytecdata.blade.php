<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    @include('layouts.header')

    <body class="home page-template-default page page-id-2274 theme-engitech woocommerce-no-js royal_preloader woocommerce-active elementor-default elementor-kit-2821 elementor-page elementor-page-2274 engitech-theme-ver-1.0.6.2 wordpress-version-5.5.3">
        <div id="royal_preloader" data-width="185" data-height="90" data-url="http://127.0.0.1:8000/wp-content/uploads/sites/logo/Logo_671x3634_final.svg" data-color="#0a0f2b" data-bgcolor="#fff"></div>
        <div id="page" class="site">
        
        @include('layouts.header-menus')

        @include('layouts.site-panel')

            <div id="content" class="site-content">
                <div data-bg="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/12/bg-pheader.jpg" class="page-header flex-middle rocket-lazyload" style="">
                    <div class="container">
                        <div class="inner flex-middle">
                            <h1 class="page-title">Why TecData</h1>
                            <ul id="breadcrumbs" class="breadcrumbs none-style">
                                <li><a href="{{route('home.index')}}">Home</a></li>
                                <li class="active">Why TecData</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div data-elementor-type="wp-page" data-elementor-id="1496" class="elementor elementor-1496" data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-f7ffdaf ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="f7ffdaf"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div
                                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-28a66d7 elementor-hidden-tablet elementor-hidden-phone ot-flex-column-vertical"
                                            data-id="28a66d7"
                                            data-element_type="column"
                                        >
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-13dc5b4 elementor-widget elementor-widget-image" data-id="13dc5b4" data-element_type="widget" data-widget_type="image.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="750"
                                                                    height="980"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20750%20980'%3E%3C/svg%3E"
                                                                    class="attachment-full size-full"
                                                                    alt=""
                                                                    data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1.png 750w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-230x300.png 230w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-720x941.png 720w"
                                                                    data-lazy-sizes="(max-width: 750px) 100vw, 750px"
                                                                    data-lazy-src="../wp-content/uploads/sites/4/2020/01/man1.png"
                                                                />
                                                                <noscript>
                                                                    <img
                                                                        width="750"
                                                                        height="980"
                                                                        src="../wp-content/uploads/sites/4/2020/01/man1.png"
                                                                        class="attachment-full size-full"
                                                                        alt=""
                                                                        srcset="
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1.png         750w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-230x300.png 230w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-720x941.png 720w
                                                                        "
                                                                        sizes="(max-width: 750px) 100vw, 750px"
                                                                    />
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-6665190 ot-flex-column-vertical" data-id="6665190" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-5c56f12 elementor-widget elementor-widget-iheading" data-id="5c56f12" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span> Why choose us</span>
                                                                <h2 class="main-heading">
                                                                    Your Partner for <br />
                                                                    Software Innovation
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-ae59a69 elementor-widget elementor-widget-text-editor" data-id="ae59a69" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                <p>
                                                                    TECDATA is choice of many leading enterprices,we help businesses elevate their value through custom software development, product design, QA and consultancy services.We take pride in challenging ourselves and extending boundaries to create new products and novel solutions for traditional and new age business.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-57af9e5 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="57af9e5"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-a8a9a48 ot-flex-column-vertical"
                                                                    data-id="a8a9a48"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-a3f7242 elementor-widget elementor-widget-iiconbox1"
                                                                                data-id="a3f7242"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-medal"></span>
                                                                                        </div>
                                                                                        <h5>Experience</h5>
                                                                                        <div class="line-box"></div>
                                                                                        <p>Our great team of more than 1400 software experts.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e0a9c03 ot-flex-column-vertical"
                                                                    data-id="e0a9c03"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-b90ddea elementor-widget elementor-widget-iiconbox1"
                                                                                data-id="b90ddea"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s1">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-gear"></span>
                                                                                        </div>
                                                                                        <h5>Quick Support</h5>
                                                                                        <div class="line-box"></div>
                                                                                        <p>We’ll help you test bold new ideas while sharing your.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <div
                                                        class="elementor-element elementor-element-1142a57 elementor-hidden-desktop elementor-widget elementor-widget-image"
                                                        data-id="1142a57"
                                                        data-element_type="widget"
                                                        data-widget_type="image.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="750"
                                                                    height="980"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20750%20980'%3E%3C/svg%3E"
                                                                    class="attachment-full size-full"
                                                                    alt=""
                                                                    data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1.png 750w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-230x300.png 230w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-720x941.png 720w"
                                                                    data-lazy-sizes="(max-width: 750px) 100vw, 750px"
                                                                    data-lazy-src="../wp-content/uploads/sites/4/2020/01/man1.png"
                                                                />
                                                                <noscript>
                                                                    <img
                                                                        width="750"
                                                                        height="980"
                                                                        src="../wp-content/uploads/sites/4/2020/01/man1.png"
                                                                        class="attachment-full size-full"
                                                                        alt=""
                                                                        srcset="
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1.png         750w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-230x300.png 230w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/man1-720x941.png 720w
                                                                        "
                                                                        sizes="(max-width: 750px) 100vw, 750px"
                                                                    />
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-96895f0 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="96895f0"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7071f49 ot-flex-column-vertical" data-id="7071f49" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-9eb53af elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="9eb53af"
                                                        data-element_type="section"
                                                        data-settings='{"background_background":"gradient"}'
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-f2320c3 ot-flex-column-vertical"
                                                                    data-id="f2320c3"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-25d45a4 elementor-widget elementor-widget-icounter"
                                                                                data-id="25d45a4"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="330" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>active Clients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-c36cdfc ot-flex-column-vertical"
                                                                    data-id="c36cdfc"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-de2796d elementor-widget elementor-widget-icounter"
                                                                                data-id="de2796d"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="850" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>projects done</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-873ef61 ot-flex-column-vertical"
                                                                    data-id="873ef61"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3298a41 elementor-widget elementor-widget-icounter"
                                                                                data-id="3298a41"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="25" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>team advisors</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-b651bbb ot-flex-column-vertical"
                                                                    data-id="b651bbb"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-77bd780 elementor-widget elementor-widget-icounter"
                                                                                data-id="77bd780"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="10" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>Glorious Years</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <div class="elementor-element elementor-element-4adef98 elementor-widget elementor-widget-iheading" data-id="4adef98" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// our services</span>
                                                                <h2 class="main-heading">
                                                                    We Offer a Wide <br />
                                                                    Variety of IT Services
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-e342bf4 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="e342bf4"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-1cfb801 ot-flex-column-vertical"
                                                                    data-id="1cfb801"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-c876987 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="c876987"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-code"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5><a href="../web-development/index.html">Web Development</a></h5>
                                                                                            <p>We carry more than just good coding skills. Our experience makes us stand out from other web development.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-ea2cd59 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="ea2cd59"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-browser"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>QA & Testing</h5>
                                                                                            <p>Turn to our experts to perform comprehensive, multi-stage testing and auditing of your software.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b85c0ec ot-flex-column-vertical"
                                                                    data-id="b85c0ec"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-ba8b82b elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="ba8b82b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-app"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5><a href="../mobile-development/index.html">Mobile Development</a></h5>
                                                                                            <p>Create complex enterprise software, ensure reliable software integration, modernise your legacy system.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-358f8b3 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="358f8b3"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-report-1"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>IT Counsultancy</h5>
                                                                                            <p>Trust our top minds to eliminate workflow pain points, implement new tech, and consolidate app.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-96cbacd elementor-hidden-tablet elementor-hidden-phone ot-flex-column-vertical"
                                                                    data-id="96cbacd"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-7deadcf elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="7deadcf"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-monitor"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>UI/UX Design</h5>
                                                                                            <p>Build the product you need on time with an experienced team that uses a clear and effective design.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-f973fe7 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="f973fe7"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-best"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Dedicated Team</h5>
                                                                                            <p>Over the past decade, our customers succeeded by leveraging Intellectsoft’s process of building, motivating.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-1b91252 elementor-section-full_width elementor-hidden-desktop ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="1b91252"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-935e5ae ot-flex-column-vertical"
                                                                    data-id="935e5ae"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-70e548a elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="70e548a"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-monitor"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>UI/UX Design</h5>
                                                                                            <p>Build the product you need on time with an experienced team that uses a clear and effective design.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-fbb0d20 ot-flex-column-vertical"
                                                                    data-id="fbb0d20"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-ad1e2a4 elementor-widget elementor-widget-iiconbox2"
                                                                                data-id="ad1e2a4"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iiconbox2.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="icon-box-s2 s2">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-best"></span>
                                                                                        </div>
                                                                                        <div class="content-box">
                                                                                            <h5>Dedicated Team</h5>
                                                                                            <p>Over the past decade, our customers succeeded by leveraging Intellectsoft’s process of building, motivating.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-f4944d0 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                data-id="f4944d0"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-62b776e ot-flex-column-vertical" data-id="62b776e" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-588eb19 elementor-section-content-middle ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                        data-id="588eb19"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3c11d20 ot-flex-column-vertical"
                                                                    data-id="3c11d20"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-1a4ed6f elementor-widget elementor-widget-iheading"
                                                                                data-id="1a4ed6f"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>Latest case studies</span>
                                                                                        <h2 class="main-heading">Enterprice Resource Mangement, Vizza Insuance</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-11deb46 elementor-hidden-tablet ot-flex-column-vertical"
                                                                    data-id="11deb46"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-736d02f elementor-widget elementor-widget-text-editor"
                                                                                data-id="736d02f"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        <p>We’ve exceled our experience in a wide range of industries to bring valuable insights and provide our customers.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-59fee8c ot-flex-column-vertical"
                                                                    data-id="59fee8c"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-5a5bef3 elementor-align-right elementor-mobile-align-left elementor-widget elementor-widget-ibutton"
                                                                                data-id="5a5bef3"
                                                                                data-element_type="widget"
                                                                                data-widget_type="ibutton.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-button">
                                                                                        <a href="../portfolio-grid/index.html" class="btn-details"><i class="flaticon-right-arrow-1"></i> VIEW ALL PROJECTS</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-d003d76 elementor-section-full_width elementor-section-content-bottom ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="d003d76"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ad09ce1 ot-flex-column-vertical"
                                                                    data-id="ad09ce1"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-d207715 elementor-widget elementor-widget-irprojects"
                                                                                data-id="d207715"
                                                                                data-element_type="widget"
                                                                                data-widget_type="irprojects.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="project-slider" data-center="true" data-show="2" data-scroll="2" data-arrow="false" data-dots="true">
                                                                                        <div class="project-item projects-style-2">
                                                                                            <div class="projects-box">
                                                                                                <div class="projects-thumbnail">
                                                                                                    <a href="../portfolio/app-for-virtual-reality/index.html">
                                                                                                        <img
                                                                                                            width="720"
                                                                                                            height="520"
                                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                                            class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                            alt=""
                                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2019/11/project1-720x520.jpg"
                                                                                                        />
                                                                                                        <noscript>
                                                                                                            <img
                                                                                                                width="720"
                                                                                                                height="520"
                                                                                                                src="../wp-content/uploads/sites/4/2019/11/project1-720x520.jpg"
                                                                                                                class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                                alt=""
                                                                                                            />
                                                                                                        </noscript>
                                                                                                        <span class="overlay"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="portfolio-info">
                                                                                                    <div class="portfolio-info-inner">
                                                                                                        <a class="btn-link" href="../portfolio/app-for-virtual-reality/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                                        <h5><a href="../portfolio/app-for-virtual-reality/index.html">App for Virtual Reality</a></h5>
                                                                                                        <p class="portfolio-cates">
                                                                                                            <a href="../portfolio-cat/design/index.html">Design</a><span>/</span><a href="../portfolio-cat/ideas/index.html">Ideas</a>
                                                                                                            <span>/</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="project-item projects-style-2">
                                                                                            <div class="projects-box">
                                                                                                <div class="projects-thumbnail">
                                                                                                    <a href="../portfolio/analysis-of-security/index.html">
                                                                                                        <img
                                                                                                            width="720"
                                                                                                            height="520"
                                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                                            class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                            alt=""
                                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2019/11/project3-720x520.jpg"
                                                                                                        />
                                                                                                        <noscript>
                                                                                                            <img
                                                                                                                width="720"
                                                                                                                height="520"
                                                                                                                src="../wp-content/uploads/sites/4/2019/11/project3-720x520.jpg"
                                                                                                                class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                                alt=""
                                                                                                            />
                                                                                                        </noscript>
                                                                                                        <span class="overlay"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="portfolio-info">
                                                                                                    <div class="portfolio-info-inner">
                                                                                                        <a class="btn-link" href="../portfolio/analysis-of-security/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                                        <h5><a href="../portfolio/analysis-of-security/index.html">Analysis of Security</a></h5>
                                                                                                        <p class="portfolio-cates">
                                                                                                            <a href="../portfolio-cat/ideas/index.html">Ideas</a><span>/</span><a href="../portfolio-cat/technology/index.html">Technology</a>
                                                                                                            <span>/</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="project-item projects-style-2">
                                                                                            <div class="projects-box">
                                                                                                <div class="projects-thumbnail">
                                                                                                    <a href="../portfolio/ecommerce-website/index.html">
                                                                                                        <img
                                                                                                            width="720"
                                                                                                            height="520"
                                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                                            class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                            alt=""
                                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2019/11/project4-720x520.jpg"
                                                                                                        />
                                                                                                        <noscript>
                                                                                                            <img
                                                                                                                width="720"
                                                                                                                height="520"
                                                                                                                src="../wp-content/uploads/sites/4/2019/11/project4-720x520.jpg"
                                                                                                                class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                                alt=""
                                                                                                            />
                                                                                                        </noscript>
                                                                                                        <span class="overlay"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="portfolio-info">
                                                                                                    <div class="portfolio-info-inner">
                                                                                                        <a class="btn-link" href="../portfolio/ecommerce-website/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                                        <h5><a href="../portfolio/ecommerce-website/index.html">eCommerce Website</a></h5>
                                                                                                        <p class="portfolio-cates">
                                                                                                            <a href="../portfolio-cat/design/index.html">Design</a><span>/</span><a href="../portfolio-cat/ideas/index.html">Ideas</a>
                                                                                                            <span>/</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="project-item projects-style-2">
                                                                                            <div class="projects-box">
                                                                                                <div class="projects-thumbnail">
                                                                                                    <a href="../portfolio/basics-project/index.html">
                                                                                                        <img
                                                                                                            width="720"
                                                                                                            height="520"
                                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                                            class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                            alt=""
                                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2019/11/project8-720x520.jpg"
                                                                                                        />
                                                                                                        <noscript>
                                                                                                            <img
                                                                                                                width="720"
                                                                                                                height="520"
                                                                                                                src="../wp-content/uploads/sites/4/2019/11/project8-720x520.jpg"
                                                                                                                class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                                alt=""
                                                                                                            />
                                                                                                        </noscript>
                                                                                                        <span class="overlay"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="portfolio-info">
                                                                                                    <div class="portfolio-info-inner">
                                                                                                        <a class="btn-link" href="../portfolio/basics-project/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                                        <h5><a href="../portfolio/basics-project/index.html">Basics Project</a></h5>
                                                                                                        <p class="portfolio-cates">
                                                                                                            <a href="../portfolio-cat/design/index.html">Design</a><span>/</span>
                                                                                                            <a href="../portfolio-cat/development/index.html">Development</a><span>/</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="project-item projects-style-2">
                                                                                            <div class="projects-box">
                                                                                                <div class="projects-thumbnail">
                                                                                                    <a href="../portfolio/social-media-app/index.html">
                                                                                                        <img
                                                                                                            width="720"
                                                                                                            height="520"
                                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20520'%3E%3C/svg%3E"
                                                                                                            class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                            alt=""
                                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2019/11/project7-720x520.jpg"
                                                                                                        />
                                                                                                        <noscript>
                                                                                                            <img
                                                                                                                width="720"
                                                                                                                height="520"
                                                                                                                src="../wp-content/uploads/sites/4/2019/11/project7-720x520.jpg"
                                                                                                                class="attachment-engitech-portfolio-thumbnail-carousel size-engitech-portfolio-thumbnail-carousel wp-post-image"
                                                                                                                alt=""
                                                                                                            />
                                                                                                        </noscript>
                                                                                                        <span class="overlay"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="portfolio-info">
                                                                                                    <div class="portfolio-info-inner">
                                                                                                        <a class="btn-link" href="../portfolio/social-media-app/index.html"><i class="flaticon-right-arrow-1"></i></a>
                                                                                                        <h5><a href="../portfolio/social-media-app/index.html">Social Media App</a></h5>
                                                                                                        <p class="portfolio-cates">
                                                                                                            <a href="../portfolio-cat/design/index.html">Design</a><span>/</span><a href="../portfolio-cat/technology/index.html">Technology</a>
                                                                                                            <span>/</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-7af70df ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="7af70df"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-edd21c1 ot-flex-column-vertical" data-id="edd21c1" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-a5301d3 elementor-widget elementor-widget-iheading" data-id="a5301d3" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// TECHNOLOGY INDEX</span>
                                                                <h2 class="main-heading">
                                                                    We Deliver Solution with <br />
                                                                    the Goal of Trusting Relationships
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-923b4fc elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="923b4fc"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-204b95c ot-flex-column-vertical"
                                                                    data-id="204b95c"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-e1d97ec elementor-widget elementor-widget-itechbox"
                                                                                data-id="e1d97ec"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#" target="_blank">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-code-1"></span>
                                                                                        </div>
                                                                                        <h5>WEB</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-48691cb ot-flex-column-vertical"
                                                                    data-id="48691cb"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-02c974a elementor-widget elementor-widget-itechbox"
                                                                                data-id="02c974a"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-android"></span>
                                                                                        </div>
                                                                                        <h5>Android</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-c766038 ot-flex-column-vertical"
                                                                    data-id="c766038"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-08728df elementor-widget elementor-widget-itechbox"
                                                                                data-id="08728df"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-apple"></span>
                                                                                        </div>
                                                                                        <h5>IOS</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-531e02e ot-flex-column-vertical"
                                                                    data-id="531e02e"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-347385b elementor-widget elementor-widget-itechbox"
                                                                                data-id="347385b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-iot"></span>
                                                                                        </div>
                                                                                        <h5>IOT</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-0b20857 ot-flex-column-vertical"
                                                                    data-id="0b20857"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-982d0c3 elementor-widget elementor-widget-itechbox"
                                                                                data-id="982d0c3"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-time-and-date"></span>
                                                                                        </div>
                                                                                        <h5>Wearalables</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-2d5c19f ot-flex-column-vertical"
                                                                    data-id="2d5c19f"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3f9ff29 elementor-widget elementor-widget-itechbox"
                                                                                data-id="3f9ff29"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-tv"></span>
                                                                                        </div>
                                                                                        <h5>TV</h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-4bb0005 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="4bb0005"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2c12b34 ot-flex-column-vertical" data-id="2c12b34" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-a7ba84b elementor-section-full_width elementor-section-content-bottom ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="a7ba84b"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-default">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d2a241c ot-flex-column-vertical"
                                                                    data-id="d2a241c"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-497d10b elementor-widget elementor-widget-iheading"
                                                                                data-id="497d10b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>// our recent news</span>
                                                                                        <h2 class="main-heading">Read Our Latest News</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-924c8b3 ot-flex-column-vertical"
                                                                    data-id="924c8b3"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-a3dee96 elementor-align-right elementor-mobile-align-left elementor-widget elementor-widget-ibutton"
                                                                                data-id="a3dee96"
                                                                                data-element_type="widget"
                                                                                data-widget_type="ibutton.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-button">
                                                                                        <a href="../blog/index.html" class="octf-btn octf-btn-primary">All News</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <div class="elementor-element elementor-element-6d104f0 elementor-widget elementor-widget-ipost_grid" data-id="6d104f0" data-element_type="widget" data-widget_type="ipost_grid.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="post-grid pgrid row">
                                                                <div class="pgrid-box">
                                                                    <article class="post-box blog-item">
                                                                        <div class="post-inner">
                                                                            <div class="entry-media">
                                                                                <div class="post-cat">
                                                                                    <span class="posted-in">
                                                                                        <a href="../category/design/index.html" rel="category tag">Design</a> <a href="../category/development/index.html" rel="category tag">Development</a>
                                                                                    </span>
                                                                                </div>
                                                                                <a href="../plan-your-project-with-your-software/index.html">
                                                                                    <img
                                                                                        width="601"
                                                                                        height="520"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20601%20520'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                        alt=""
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/blog1-601x520.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="601"
                                                                                            height="520"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/blog1-601x520.jpg"
                                                                                            class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                            alt=""
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                            <div class="inner-post">
                                                                                <div class="entry-header">
                                                                                    <div class="entry-meta">
                                                                                        <span class="posted-on">
                                                                                            _
                                                                                            <a href="../plan-your-project-with-your-software/index.html" rel="bookmark">
                                                                                                <time class="entry-date published" datetime="2019-11-21T04:46:58+00:00">November 21, 2019</time>
                                                                                            </a>
                                                                                        </span>
                                                                                        <span class="byline">_ <a class="url fn n" href="../author/admin/index.html">Tom Black</a></span>
                                                                                        <span class="comment-num">_ <a href="../plan-your-project-with-your-software/index.html#comments">3 Comments</a></span>
                                                                                    </div>
                                                                                    <!-- .entry-meta -->

                                                                                    <h3 class="entry-title"><a href="../plan-your-project-with-your-software/index.html" rel="bookmark">Plan Your Project with Your Software</a></h3>
                                                                                </div>
                                                                                <!-- .entry-header -->

                                                                                <div class="btn-readmore">
                                                                                    <a href="../plan-your-project-with-your-software/index.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </article>
                                                                </div>

                                                                <div class="pgrid-box">
                                                                    <article class="post-box blog-item">
                                                                        <div class="post-inner">
                                                                            <div class="entry-media">
                                                                                <div class="post-cat">
                                                                                    <span class="posted-in">
                                                                                        <a href="../category/design/index.html" rel="category tag">Design</a> <a href="../category/development/index.html" rel="category tag">Development</a>
                                                                                    </span>
                                                                                </div>
                                                                                <a href="../you-have-a-great-business-idea-2/index.html">
                                                                                    <img
                                                                                        width="601"
                                                                                        height="520"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20601%20520'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                        alt=""
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/11/blog4-601x520.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="601"
                                                                                            height="520"
                                                                                            src="../wp-content/uploads/sites/4/2019/11/blog4-601x520.jpg"
                                                                                            class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                            alt=""
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                            <div class="inner-post">
                                                                                <div class="entry-header">
                                                                                    <div class="entry-meta">
                                                                                        <span class="posted-on">
                                                                                            _
                                                                                            <a href="../you-have-a-great-business-idea-2/index.html" rel="bookmark">
                                                                                                <time class="entry-date published" datetime="2019-11-21T04:46:03+00:00">November 21, 2019</time>
                                                                                            </a>
                                                                                        </span>
                                                                                        <span class="byline">_ <a class="url fn n" href="../author/admin/index.html">Tom Black</a></span>
                                                                                        <span class="comment-num">_ <a href="../you-have-a-great-business-idea-2/index.html#respond">0 Comments</a></span>
                                                                                    </div>
                                                                                    <!-- .entry-meta -->

                                                                                    <h3 class="entry-title"><a href="../you-have-a-great-business-idea-2/index.html" rel="bookmark">You have a Great Business Idea?</a></h3>
                                                                                </div>
                                                                                <!-- .entry-header -->

                                                                                <div class="btn-readmore">
                                                                                    <a href="../you-have-a-great-business-idea-2/index.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </article>
                                                                </div>

                                                                <div class="pgrid-box">
                                                                    <article class="post-box blog-item">
                                                                        <div class="post-inner">
                                                                            <div class="entry-media">
                                                                                <div class="post-cat">
                                                                                    <span class="posted-in"><a href="../category/development/index.html" rel="category tag">Development</a></span>
                                                                                </div>
                                                                                <a href="../building-data-analytics-software/index.html">
                                                                                    <img
                                                                                        width="601"
                                                                                        height="520"
                                                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20601%20520'%3E%3C/svg%3E"
                                                                                        class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                        alt=""
                                                                                        data-lazy-src="../wp-content/uploads/sites/4/2019/09/blog3-601x520.jpg"
                                                                                    />
                                                                                    <noscript>
                                                                                        <img
                                                                                            width="601"
                                                                                            height="520"
                                                                                            src="../wp-content/uploads/sites/4/2019/09/blog3-601x520.jpg"
                                                                                            class="attachment-engitech-slider-post-thumbnail size-engitech-slider-post-thumbnail wp-post-image"
                                                                                            alt=""
                                                                                        />
                                                                                    </noscript>
                                                                                </a>
                                                                            </div>
                                                                            <div class="inner-post">
                                                                                <div class="entry-header">
                                                                                    <div class="entry-meta">
                                                                                        <span class="posted-on">
                                                                                            _
                                                                                            <a href="../building-data-analytics-software/index.html" rel="bookmark">
                                                                                                <time class="entry-date published" datetime="2019-09-24T09:23:05+00:00">September 24, 2019</time>
                                                                                            </a>
                                                                                        </span>
                                                                                        <span class="byline">_ <a class="url fn n" href="../author/admin/index.html">Tom Black</a></span>
                                                                                        <span class="comment-num">_ <a href="../building-data-analytics-software/index.html#comments">3 Comments</a></span>
                                                                                    </div>
                                                                                    <!-- .entry-meta -->

                                                                                    <h3 class="entry-title"><a href="../building-data-analytics-software/index.html" rel="bookmark">Building Data Analytics Software</a></h3>
                                                                                </div>
                                                                                <!-- .entry-header -->

                                                                                <div class="btn-readmore">
                                                                                    <a href="../building-data-analytics-software/index.html"><i class="flaticon-right-arrow-1"></i>LEARN MORE</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </article>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #content -->
            @include('layouts.footer')
        </div>
        <!-- #page -->

        <a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>

        <script type="text/javascript" id="wc-add-to-cart-js-extra">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "View cart",
                cart_url: "http:\/\/wpdemo.archiwp.com\/engitech\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="woocommerce-js-extra">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/engitech\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>

        <script type="text/javascript" id="wc-cart-fragments-js-extra">
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                cart_hash_key: "wc_cart_hash_c8d57f1cd4add6020c9933c2e3c64aa9",
                fragment_name: "wc_fragments_c8d57f1cd4add6020c9933c2e3c64aa9",
                request_timeout: "5000",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                version: "3.0.13",
                is_static: false,
                legacyMode: { elementWrappers: true },
                urls: { assets: "http:\/\/wpdemo.archiwp.com\/engitech\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                },
                post: { id: 1496, title: "Why%20Choose%20Us%20%E2%80%93%20Engitech", excerpt: "", featuredImage: false },
            };
        </script>

        <script>
            window.lazyLoadOptions = {
                elements_selector: "img[data-lazy-src],.rocket-lazyload",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_loaded: function (element) {
                    if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                        if (element.classList.contains("lazyloaded")) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                },
            };
            window.addEventListener(
                "LazyLoad::Initialized",
                function (e) {
                    var lazyLoadInstance = e.detail.instance;
                    if (window.MutationObserver) {
                        var observer = new MutationObserver(function (mutations) {
                            var image_count = 0;
                            var iframe_count = 0;
                            var rocketlazy_count = 0;
                            mutations.forEach(function (mutation) {
                                for (i = 0; i < mutation.addedNodes.length; i++) {
                                    if (typeof mutation.addedNodes[i].getElementsByTagName !== "function") {
                                        return;
                                    }
                                    if (typeof mutation.addedNodes[i].getElementsByClassName !== "function") {
                                        return;
                                    }
                                    images = mutation.addedNodes[i].getElementsByTagName("img");
                                    is_image = mutation.addedNodes[i].tagName == "IMG";
                                    iframes = mutation.addedNodes[i].getElementsByTagName("iframe");
                                    is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                                    rocket_lazy = mutation.addedNodes[i].getElementsByClassName("rocket-lazyload");
                                    image_count += images.length;
                                    iframe_count += iframes.length;
                                    rocketlazy_count += rocket_lazy.length;
                                    if (is_image) {
                                        image_count += 1;
                                    }
                                    if (is_iframe) {
                                        iframe_count += 1;
                                    }
                                }
                            });
                            if (image_count > 0 || iframe_count > 0 || rocketlazy_count > 0) {
                                lazyLoadInstance.update();
                            }
                        });
                        var b = document.getElementsByTagName("body")[0];
                        var config = { childList: !0, subtree: !0 };
                        observer.observe(b, config);
                    }
                },
                !1
            );
        </script>
        <script data-no-minify="1" async src="../wp-content/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js"></script>
        <script>
            "use strict";
            var wprRemoveCPCSS = function wprRemoveCPCSS() {
                var elem;
                document.querySelector('link[data-rocket-async="style"][rel="preload"]') ? setTimeout(wprRemoveCPCSS, 200) : (elem = document.getElementById("rocket-critical-css")) && "remove" in elem && elem.remove();
            };
            window.addEventListener ? window.addEventListener("load", wprRemoveCPCSS) : window.attachEvent && window.attachEvent("onload", wprRemoveCPCSS);
        </script>
        <script src="../wp-content/cache/min/4/ca52a6bf14680e6b18f41d1eb26c0d24.js" data-minify="1" defer></script>
        <noscript>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100i%2C200%2C200i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CNunito%20Sans%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%20Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin%2Clatin-ext&#038;display=swap"
            />
            <link rel="stylesheet" href="../wp-content/cache/min/4/1d6b651439dadd8fbfb6a8fac9a9cdd6.css" media="all" data-minify="1" />
            <link rel="stylesheet" id="woocommerce-smallscreen-css" href="../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen1c9b.css?ver=4.6.1" type="text/css" media="only screen and (max-width: 768px)" />
        </noscript>
    </body>

