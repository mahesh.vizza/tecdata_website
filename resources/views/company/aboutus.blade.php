<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    @include('layouts.header')

    <body class="home page-template-default page page-id-2274 theme-engitech woocommerce-no-js royal_preloader woocommerce-active elementor-default elementor-kit-2821 elementor-page elementor-page-2274 engitech-theme-ver-1.0.6.2 wordpress-version-5.5.3">
        <div id="royal_preloader" data-width="185" data-height="90" data-url="http://127.0.0.1:8000/wp-content/uploads/sites/logo/Logo_671x3634_final.svg" data-color="#0a0f2b" data-bgcolor="#fff"></div>
        <div id="page" class="site">
        
        @include('layouts.header-menus')

        @include('layouts.site-panel')

        <div id="content" class="site-content">
                <div data-bg="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/12/bg-pheader.jpg" class="page-header flex-middle rocket-lazyload" style="">
                    <div class="container">
                        <div class="inner flex-middle">
                            <h1 class="page-title">About Us</h1>
                            <ul id="breadcrumbs" class="breadcrumbs none-style">
                                <li><a href="{{route('home.index')}}">Home</a></li>
                                <li class="active">About Us</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div data-elementor-type="wp-page" data-elementor-id="1579" class="elementor elementor-1579" data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-7f3cbd8 elementor-section-content-middle ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="7f3cbd8"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-9fc2569 ot-flex-column-vertical" data-id="9fc2569" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-5f326aa elementor-widget elementor-widget-text-editor" data-id="5f326aa" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix">
                                                            <p>
                                                                <em class="text-dark">
                                                                    <strong>
                                                                        We don't  believe in a "one size fits all" policy and have seen the difference that a tailor-made offering can bring to businesses. Exactly why we love building custom solutions for all our clients. We take pride in challenging ourselves and extending boundaries to create new products and novel solutions for traditional and new age business. 
                                                                    </strong>
                                                                </em>
                                                            </p>
                                                                <p>
                                                                    <em class="text-dark">
                                                                        <strong>Our suite of tools are high on performance but low on costs, and put your existing infrastructure to judicious use. Our customer-centricity, ability to take on challenges and financial risks, and go-getter DNA bring to you a partner who is easy to work with, and who delivers beyond promises. No wonder we boast of a marquee list of Fortune  clients, and many more, each being 100% reference-able.</strong>
                                                                    </em>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-7564da8 ot-flex-column-vertical" data-id="7564da8" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-c97a89f elementor-section-full_width ot-layout_block elementor-section-height-default elementor-section-height-default"
                                                        data-id="c97a89f"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ee166a8 ot-flex-column-vertical"
                                                                    data-id="ee166a8"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3bd0bf1 elementor-widget elementor-widget-iimagebox1"
                                                                                data-id="3bd0bf1"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iimagebox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="ot-image-box" href="#">
                                                                                        <div class="overlay">
                                                                                            <h4 style="font-size:16px !important;">To significantly improve operations efficiency, and reduce the cost of operations for our customers.</h4>
                                                                                        </div>
                                                                                        <img
                                                                                            src="http://127.0.0.1:8000/wp-content/images/image-box1.jpg"
                                                                                            alt="To significantly improve operations efficiency, and reduce the cost of operations for our customers."
                                                                                            data-lazy-src="http://127.0.0.1:8000/wp-content/images/image-box1.jpg"
                                                                                        />
                                                                                        <noscript><img src="http://127.0.0.1:8000/wp-content/images/image-box1.jpg" alt="Our Mission" /></noscript>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-f732a9b elementor-widget elementor-widget-iimagebox1"
                                                                                data-id="f732a9b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iimagebox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="ot-image-box" href="#">
                                                                                        <div class="overlay">
                                                                                            <h4 style="font-size:16px !important;">Our Philosophy</h4>
                                                                                        </div>
                                                                                        <img
                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                            alt="Our Philosophy"
                                                                                            data-lazy-src="http://127.0.0.1:8000/wp-content/images/image-box3.jpg"
                                                                                        />
                                                                                        <noscript><img src="http://127.0.0.1:8000/wp-content/images/image-box3.jpg" alt="Our Philosophy" /></noscript>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d9c942d ot-flex-column-vertical"
                                                                    data-id="d9c942d"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-0d52049 elementor-widget elementor-widget-iimagebox1"
                                                                                data-id="0d52049"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iimagebox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="ot-image-box" href="#">
                                                                                        <div class="overlay">
                                                                                            <h4 style="font-size:16px !important;">To build a global corporation renowned for its relentless focus on optimization, distinguished by its repertoire of inventive processes, enterprising staff, and cutting edge tools.</h4>
                                                                                        </div>
                                                                                        <img
                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                            alt="Our Vision"
                                                                                            data-lazy-src="http://127.0.0.1:8000/wp-content/images/image-box2.jpg"
                                                                                        />
                                                                                        <noscript><img src="http://127.0.0.1:8000/wp-content/images/image-box2.jpg" alt="Our Vision" /></noscript>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-cf54c53 elementor-widget elementor-widget-iimagebox1"
                                                                                data-id="cf54c53"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iimagebox1.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="ot-image-box" href="#">
                                                                                        <div class="overlay">
                                                                                            <h4 style="font-size:16px !important;">We expand global Footprints Enhance digital experience Enhance ITIL to improve IT Support team</h4>
                                                                                        </div>
                                                                                        <img
                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"
                                                                                            alt="Our Strategy"
                                                                                            data-lazy-src="http://127.0.0.1:8000/wp-content/images/image-box4.jpg"
                                                                                        />
                                                                                        <noscript><img src="http://127.0.0.1:8000/wp-content/images/image-box4.jpg" alt="Our Strategy" /></noscript>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-170a49a ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="170a49a"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-43f1d1e ot-flex-column-vertical" data-id="43f1d1e" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-d41fd46 elementor-widget elementor-widget-image" data-id="d41fd46" data-element_type="widget" data-widget_type="image.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-image">
                                                                <img
                                                                    width="665"
                                                                    height="493"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20665%20493'%3E%3C/svg%3E"
                                                                    class="attachment-full size-full"
                                                                    alt=""
                                                                    data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-about.png 665w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-about-300x222.png 300w"
                                                                    data-lazy-sizes="(max-width: 665px) 100vw, 665px"
                                                                    data-lazy-src="../wp-content/uploads/sites/4/2020/02/image1-about.png"
                                                                />
                                                                <noscript>
                                                                    <img
                                                                        width="665"
                                                                        height="493"
                                                                        src="../wp-content/uploads/sites/4/2020/02/image1-about.png"
                                                                        class="attachment-full size-full"
                                                                        alt=""
                                                                        srcset="
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-about.png         665w,
                                                                            http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image1-about-300x222.png 300w
                                                                        "
                                                                        sizes="(max-width: 665px) 100vw, 665px"
                                                                    />
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-6a613e7 ot-flex-column-vertical" data-id="6a613e7" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-2a85ab8 elementor-widget elementor-widget-iheading" data-id="2a85ab8" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>Experience. Execution. Excellence.</span>
                                                                <h2 class="main-heading">What We Actually Do</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-bba99d0 elementor-section-full_width ot-layout_inline elementor-section-height-default elementor-section-height-default"
                                                        data-id="bba99d0"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-default">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-db3d6e9 ot-flex-column-vertical"
                                                                    data-id="db3d6e9"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-fe59a98 elementor-widget elementor-widget-itechbox"
                                                                                data-id="fe59a98"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-php"></span>
                                                                                        </div>
                                                                                        <h5></h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-9a6af8c elementor-widget elementor-widget-itechbox"
                                                                                data-id="9a6af8c"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-electron"></span>
                                                                                        </div>
                                                                                        <h5></h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-0a9c4dd elementor-widget elementor-widget-itechbox"
                                                                                data-id="0a9c4dd"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-java"></span>
                                                                                        </div>
                                                                                        <h5></h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-43f744c elementor-widget elementor-widget-itechbox"
                                                                                data-id="43f744c"
                                                                                data-element_type="widget"
                                                                                data-widget_type="itechbox.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <a class="tech-box" href="#">
                                                                                        <div class="icon-main">
                                                                                            <span class="flaticon-css"></span>
                                                                                        </div>
                                                                                        <h5></h5>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-3c9f961 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="3c9f961"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-default">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-75497dc ot-flex-column-vertical"
                                                                    data-id="75497dc"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-1c89921 elementor-widget elementor-widget-heading"
                                                                                data-id="1c89921"
                                                                                data-element_type="widget"
                                                                                data-widget_type="heading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <h5 class="elementor-heading-title elementor-size-default">Our Web & Application Services</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-d3c7251 elementor-widget elementor-widget-text-editor"
                                                                                data-id="d3c7251"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        <p>
                                                                                            Impactful and industry-specific PHP solutions built with performance, stability, security, scalability, and maintainability in mind. With its versatility, extended support, and the extensive range of powerful allied technologies and frameworks, PHP is one of the best solutions for web applications.
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-eb4d4c4 elementor-widget elementor-widget-ibutton"
                                                                                data-id="eb4d4c4"
                                                                                data-element_type="widget"
                                                                                data-widget_type="ibutton.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-button">
                                                                                        <a href="#" class="btn-details"><i class="flaticon-right-arrow-1"></i> LEARN MORE</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-db45bce ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="db45bce"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-aa78013 ot-flex-column-vertical" data-id="aa78013" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-924103c elementor-widget elementor-widget-iheading" data-id="924103c" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>// our services</span>
                                                                <h2 class="main-heading">Our Leadership Team</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-cb963fe elementor-widget elementor-widget-text-editor" data-id="cb963fe" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                We help businesses elevate their value through custom software development,<br />
                                                                product design, QA and consultancy services.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-00948eb elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="00948eb"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-no">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-6ec44f4 ot-flex-column-vertical"
                                                                    data-id="6ec44f4"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-4d2cc84 elementor-widget elementor-widget-imember"
                                                                                data-id="4d2cc84"
                                                                                data-element_type="widget"
                                                                                data-widget_type="imember.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="team-wrap">
                                                                                        <div class="team-thumb">
                                                                                            <img
                                                                                                width="720"
                                                                                                height="987"
                                                                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20987'%3E%3C/svg%3E"
                                                                                                class="attachment-full size-full"
                                                                                                alt=""
                                                                                                data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member1.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member1-219x300.jpg 219w"
                                                                                                data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                                data-lazy-src="../wp-content/uploads/sites/4/2020/01/member1.jpg"
                                                                                            />
                                                                                            <noscript>
                                                                                                <img
                                                                                                    width="720"
                                                                                                    height="987"
                                                                                                    src="../wp-content/uploads/sites/4/2020/01/member1.jpg"
                                                                                                    class="attachment-full size-full"
                                                                                                    alt=""
                                                                                                    srcset="
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member1.jpg         720w,
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member1-219x300.jpg 219w
                                                                                                    "
                                                                                                    sizes="(max-width: 720px) 100vw, 720px"
                                                                                                />
                                                                                            </noscript>
                                                                                            <div class="team-social flex-middle">
                                                                                                <div>
                                                                                                    <a rel="nofollow" href="#" class="twitter">
                                                                                                        <i class="fab fa-twitter"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="linkedin">
                                                                                                        <i class="fab fa-linkedin-in"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="pinterest">
                                                                                                        <i class="fab fa-pinterest-p"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="instagram">
                                                                                                        <i class="fab fa-instagram"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="team-info">
                                                                                            <h4>David Ferry</h4>
                                                                                            <span>Co-Founder of company</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-99bea18 ot-flex-column-vertical"
                                                                    data-id="99bea18"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-e50944d elementor-widget elementor-widget-imember"
                                                                                data-id="e50944d"
                                                                                data-element_type="widget"
                                                                                data-widget_type="imember.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="team-wrap">
                                                                                        <div class="team-thumb">
                                                                                            <img
                                                                                                width="720"
                                                                                                height="986"
                                                                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20986'%3E%3C/svg%3E"
                                                                                                class="attachment-full size-full"
                                                                                                alt=""
                                                                                                data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member2.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member2-219x300.jpg 219w"
                                                                                                data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                                data-lazy-src="../wp-content/uploads/sites/4/2020/01/member2.jpg"
                                                                                            />
                                                                                            <noscript>
                                                                                                <img
                                                                                                    width="720"
                                                                                                    height="986"
                                                                                                    src="../wp-content/uploads/sites/4/2020/01/member2.jpg"
                                                                                                    class="attachment-full size-full"
                                                                                                    alt=""
                                                                                                    srcset="
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member2.jpg         720w,
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member2-219x300.jpg 219w
                                                                                                    "
                                                                                                    sizes="(max-width: 720px) 100vw, 720px"
                                                                                                />
                                                                                            </noscript>
                                                                                            <div class="team-social flex-middle">
                                                                                                <div>
                                                                                                    <a rel="nofollow" href="#" class="twitter">
                                                                                                        <i class="fab fa-twitter"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="linkedin">
                                                                                                        <i class="fab fa-linkedin-in"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="pinterest">
                                                                                                        <i class="fab fa-pinterest-p"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="instagram">
                                                                                                        <i class="fab fa-instagram"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="team-info">
                                                                                            <h4>Christina Torres</h4>
                                                                                            <span>Co-Founder of company</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-56cd4a7 ot-flex-column-vertical"
                                                                    data-id="56cd4a7"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-06c329b elementor-widget elementor-widget-imember"
                                                                                data-id="06c329b"
                                                                                data-element_type="widget"
                                                                                data-widget_type="imember.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="team-wrap">
                                                                                        <div class="team-thumb">
                                                                                            <img
                                                                                                width="720"
                                                                                                height="986"
                                                                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20986'%3E%3C/svg%3E"
                                                                                                class="attachment-full size-full"
                                                                                                alt=""
                                                                                                data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member3.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member3-219x300.jpg 219w"
                                                                                                data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                                data-lazy-src="../wp-content/uploads/sites/4/2020/01/member3.jpg"
                                                                                            />
                                                                                            <noscript>
                                                                                                <img
                                                                                                    width="720"
                                                                                                    height="986"
                                                                                                    src="../wp-content/uploads/sites/4/2020/01/member3.jpg"
                                                                                                    class="attachment-full size-full"
                                                                                                    alt=""
                                                                                                    srcset="
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member3.jpg         720w,
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member3-219x300.jpg 219w
                                                                                                    "
                                                                                                    sizes="(max-width: 720px) 100vw, 720px"
                                                                                                />
                                                                                            </noscript>
                                                                                            <div class="team-social flex-middle">
                                                                                                <div>
                                                                                                    <a rel="nofollow" href="#" class="twitter">
                                                                                                        <i class="fab fa-twitter"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="linkedin">
                                                                                                        <i class="fab fa-linkedin-in"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="pinterest">
                                                                                                        <i class="fab fa-pinterest-p"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="instagram">
                                                                                                        <i class="fab fa-instagram"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="team-info">
                                                                                            <h4>Amalia Bruno</h4>
                                                                                            <span>CTO of company</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-a87737f ot-flex-column-vertical"
                                                                    data-id="a87737f"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-4d3c6d6 elementor-widget elementor-widget-imember"
                                                                                data-id="4d3c6d6"
                                                                                data-element_type="widget"
                                                                                data-widget_type="imember.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="team-wrap">
                                                                                        <div class="team-thumb">
                                                                                            <img
                                                                                                width="720"
                                                                                                height="986"
                                                                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20720%20986'%3E%3C/svg%3E"
                                                                                                class="attachment-full size-full"
                                                                                                alt=""
                                                                                                data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member4.jpg 720w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member4-219x300.jpg 219w"
                                                                                                data-lazy-sizes="(max-width: 720px) 100vw, 720px"
                                                                                                data-lazy-src="../wp-content/uploads/sites/4/2020/01/member4.jpg"
                                                                                            />
                                                                                            <noscript>
                                                                                                <img
                                                                                                    width="720"
                                                                                                    height="986"
                                                                                                    src="../wp-content/uploads/sites/4/2020/01/member4.jpg"
                                                                                                    class="attachment-full size-full"
                                                                                                    alt=""
                                                                                                    srcset="
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member4.jpg         720w,
                                                                                                        http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/01/member4-219x300.jpg 219w
                                                                                                    "
                                                                                                    sizes="(max-width: 720px) 100vw, 720px"
                                                                                                />
                                                                                            </noscript>
                                                                                            <div class="team-social flex-middle">
                                                                                                <div>
                                                                                                    <a rel="nofollow" href="#" class="twitter">
                                                                                                        <i class="fab fa-twitter"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="linkedin">
                                                                                                        <i class="fab fa-linkedin-in"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="pinterest">
                                                                                                        <i class="fab fa-pinterest-p"></i>
                                                                                                    </a>
                                                                                                    <a rel="nofollow" href="#" class="instagram">
                                                                                                        <i class="fab fa-instagram"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="team-info">
                                                                                            <h4>Robert Cooper</h4>
                                                                                            <span>CEO of company</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-1f9d730 ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="1f9d730"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e4365bc ot-flex-column-vertical" data-id="e4365bc" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                <!--     <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-36fd49a elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="36fd49a"
                                                        data-element_type="section"
                                                        data-settings='{"background_background":"gradient"}'
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-c5d554e ot-flex-column-vertical"
                                                                    data-id="c5d554e"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-8c9666f elementor-widget elementor-widget-icounter"
                                                                                data-id="8c9666f"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="330" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>active Clients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-c067dd1 ot-flex-column-vertical"
                                                                    data-id="c067dd1"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-393086e elementor-widget elementor-widget-icounter"
                                                                                data-id="393086e"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="850" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>projects done</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-de7eb04 ot-flex-column-vertical"
                                                                    data-id="de7eb04"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-fa8b523 elementor-widget elementor-widget-icounter"
                                                                                data-id="fa8b523"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="25" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>team advisors</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-21a5960 ot-flex-column-vertical"
                                                                    data-id="21a5960"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-348781d elementor-widget elementor-widget-icounter"
                                                                                data-id="348781d"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icounter.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-counter">
                                                                                        <div>
                                                                                            <span class="num" data-to="10" data-time="2000"></span>
                                                                                            <span>+</span>
                                                                                        </div>
                                                                                        <h6>Glorious Years</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section> -->
                                                    <section
                                                        class="elementor-section elementor-inner-section elementor-element elementor-element-5d24f2d elementor-section-full_width elementor-section-content-middle ot-traditional elementor-section-height-default elementor-section-height-default"
                                                        data-id="5d24f2d"
                                                        data-element_type="section"
                                                    >
                                                        <div class="elementor-container elementor-column-gap-extended">
                                                            <div class="elementor-row">
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5b83572 ot-flex-column-vertical"
                                                                    data-id="5b83572"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-3287393 elementor-widget elementor-widget-iheading"
                                                                                data-id="3287393"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iheading.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-heading">
                                                                                        <span>Technologies</span>
                                                                                        <h2 class="main-heading">
                                                                                            Improve and Innovate <br />
                                                                                            with the Tech Trends
                                                                                        </h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-880c7e9 elementor-widget elementor-widget-text-editor"
                                                                                data-id="880c7e9"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                                        <p>We hire and build your own remote dedicated development teams tailored to your specific needs.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-56f8caf elementor-widget elementor-widget-iprogress"
                                                                                data-id="56f8caf"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iprogress.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-progress">
                                                                                        <div class="overflow"><span class="pname f-left">mobile development</span> <span class="ppercent f-right">70%</span></div>
                                                                                        <div class="iprogress">
                                                                                            <div class="progress-bar" data-percent="70%"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-8ef5774 elementor-widget elementor-widget-iprogress"
                                                                                data-id="8ef5774"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iprogress.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-progress">
                                                                                        <div class="overflow"><span class="pname f-left">web development</span> <span class="ppercent f-right">90%</span></div>
                                                                                        <div class="iprogress">
                                                                                            <div class="progress-bar" data-percent="90%"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-element elementor-element-8a360f2 elementor-widget elementor-widget-iprogress"
                                                                                data-id="8a360f2"
                                                                                data-element_type="widget"
                                                                                data-widget_type="iprogress.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="ot-progress">
                                                                                        <div class="overflow"><span class="pname f-left">ui/ux design</span> <span class="ppercent f-right">60%</span></div>
                                                                                        <div class="iprogress">
                                                                                            <div class="progress-bar" data-percent="60%"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f3ab4de ot-flex-column-vertical"
                                                                    data-id="f3ab4de"
                                                                    data-element_type="column"
                                                                >
                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                class="elementor-element elementor-element-db682f8 elementor-widget elementor-widget-image"
                                                                                data-id="db682f8"
                                                                                data-element_type="widget"
                                                                                data-widget_type="image.default"
                                                                            >
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-image">
                                                                                        <img
                                                                                            width="535"
                                                                                            height="455"
                                                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20535%20455'%3E%3C/svg%3E"
                                                                                            class="attachment-full size-full"
                                                                                            alt=""
                                                                                            data-lazy-srcset="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image2-about.png 535w, http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image2-about-300x255.png 300w"
                                                                                            data-lazy-sizes="(max-width: 535px) 100vw, 535px"
                                                                                            data-lazy-src="../wp-content/uploads/sites/4/2020/02/image2-about.png"
                                                                                        />
                                                                                        <noscript>
                                                                                            <img
                                                                                                width="535"
                                                                                                height="455"
                                                                                                src="../wp-content/uploads/sites/4/2020/02/image2-about.png"
                                                                                                class="attachment-full size-full"
                                                                                                alt=""
                                                                                                srcset="
                                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image2-about.png         535w,
                                                                                                    http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2020/02/image2-about-300x255.png 300w
                                                                                                "
                                                                                                sizes="(max-width: 535px) 100vw, 535px"
                                                                                            />
                                                                                        </noscript>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- #content -->
            @include('layouts.footer')
        </div>
        <!-- #page -->

        <a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>

        <script type="text/javascript" id="wc-add-to-cart-js-extra">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "View cart",
                cart_url: "http:\/\/wpdemo.archiwp.com\/engitech\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="woocommerce-js-extra">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/engitech\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>

        <script type="text/javascript" id="wc-cart-fragments-js-extra">
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                cart_hash_key: "wc_cart_hash_c8d57f1cd4add6020c9933c2e3c64aa9",
                fragment_name: "wc_fragments_c8d57f1cd4add6020c9933c2e3c64aa9",
                request_timeout: "5000",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                version: "3.0.13",
                is_static: false,
                legacyMode: { elementWrappers: true },
                urls: { assets: "http:\/\/wpdemo.archiwp.com\/engitech\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                },
                post: { id: 1579, title: "About%20Us%20%E2%80%93%20Engitech", excerpt: "", featuredImage: false },
            };
        </script>

        <script>
            window.lazyLoadOptions = {
                elements_selector: "img[data-lazy-src],.rocket-lazyload",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_loaded: function (element) {
                    if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                        if (element.classList.contains("lazyloaded")) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                },
            };
            window.addEventListener(
                "LazyLoad::Initialized",
                function (e) {
                    var lazyLoadInstance = e.detail.instance;
                    if (window.MutationObserver) {
                        var observer = new MutationObserver(function (mutations) {
                            var image_count = 0;
                            var iframe_count = 0;
                            var rocketlazy_count = 0;
                            mutations.forEach(function (mutation) {
                                for (i = 0; i < mutation.addedNodes.length; i++) {
                                    if (typeof mutation.addedNodes[i].getElementsByTagName !== "function") {
                                        return;
                                    }
                                    if (typeof mutation.addedNodes[i].getElementsByClassName !== "function") {
                                        return;
                                    }
                                    images = mutation.addedNodes[i].getElementsByTagName("img");
                                    is_image = mutation.addedNodes[i].tagName == "IMG";
                                    iframes = mutation.addedNodes[i].getElementsByTagName("iframe");
                                    is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                                    rocket_lazy = mutation.addedNodes[i].getElementsByClassName("rocket-lazyload");
                                    image_count += images.length;
                                    iframe_count += iframes.length;
                                    rocketlazy_count += rocket_lazy.length;
                                    if (is_image) {
                                        image_count += 1;
                                    }
                                    if (is_iframe) {
                                        iframe_count += 1;
                                    }
                                }
                            });
                            if (image_count > 0 || iframe_count > 0 || rocketlazy_count > 0) {
                                lazyLoadInstance.update();
                            }
                        });
                        var b = document.getElementsByTagName("body")[0];
                        var config = { childList: !0, subtree: !0 };
                        observer.observe(b, config);
                    }
                },
                !1
            );
        </script>
        <script data-no-minify="1" async src="../wp-content/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js"></script>
        <script>
            "use strict";
            var wprRemoveCPCSS = function wprRemoveCPCSS() {
                var elem;
                document.querySelector('link[data-rocket-async="style"][rel="preload"]') ? setTimeout(wprRemoveCPCSS, 200) : (elem = document.getElementById("rocket-critical-css")) && "remove" in elem && elem.remove();
            };
            window.addEventListener ? window.addEventListener("load", wprRemoveCPCSS) : window.attachEvent && window.attachEvent("onload", wprRemoveCPCSS);
        </script>
        <script src="../wp-content/cache/min/4/ca52a6bf14680e6b18f41d1eb26c0d24.js" data-minify="1" defer></script>
        <noscript>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100i%2C200%2C200i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CNunito%20Sans%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%20Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin%2Clatin-ext&#038;display=swap"
            />
            <link rel="stylesheet" href="../wp-content/cache/min/4/7f24009bd438f5be7b9a8d5e8fcd1e8d.css" media="all" data-minify="1" />
            <link rel="stylesheet" id="woocommerce-smallscreen-css" href="../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen1c9b.css?ver=4.6.1" type="text/css" media="only screen and (max-width: 768px)" />
        </noscript>
    </body>
</html>



