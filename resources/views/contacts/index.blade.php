<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    @include('layouts.header')

    <body class="home page-template-default page page-id-2274 theme-engitech woocommerce-no-js royal_preloader woocommerce-active elementor-default elementor-kit-2821 elementor-page elementor-page-2274 engitech-theme-ver-1.0.6.2 wordpress-version-5.5.3">
        <div id="royal_preloader" data-width="185" data-height="90" data-url="http://127.0.0.1:8000/wp-content/uploads/sites/logo/Logo_671x3634_final.svg" data-color="#0a0f2b" data-bgcolor="#fff"></div>
        <div id="page" class="site">
        
        @include('layouts.header-menus')

        @include('layouts.site-panel')

            <div id="content" class="site-content">
                <div data-bg="http://wpdemo.archiwp.com/engitech/wp-content/uploads/sites/4/2019/12/bg-pheader.jpg" class="page-header flex-middle rocket-lazyload" style="">
                    <div class="container">
                        <div class="inner flex-middle">
                            <h1 class="page-title">Contacts</h1>
                            <ul id="breadcrumbs" class="breadcrumbs none-style">
                                <li><a href="{{route('home.index')}}">Home</a></li>
                                <li class="active">Contacts</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div data-elementor-type="wp-page" data-elementor-id="1370" class="elementor elementor-1370" data-elementor-settings="[]">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-8c1b6b2 elementor-section-content-middle ot-traditional elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="8c1b6b2"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-extended">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-8416bc3 ot-flex-column-vertical" data-id="8416bc3" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-d735292 elementor-widget elementor-widget-iheading" data-id="d735292" data-element_type="widget" data-widget_type="iheading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ot-heading">
                                                                <span>Contact details</span>
                                                                <h2 class="main-heading">Contact us</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-d1b9e95 elementor-widget elementor-widget-text-editor" data-id="d1b9e95" data-element_type="widget" data-widget_type="text-editor.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. We will be happy to answer your questions.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-b59a336 elementor-widget elementor-widget-icontact_info"
                                                        data-id="b59a336"
                                                        data-element_type="widget"
                                                        data-widget_type="icontact_info.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="contact-info box-style1">
                                                                <i class="flaticon-world-globe"></i>
                                                                <div class="info-text">
                                                                    <h6>Our Address:</h6>
                                                                    <p>New #: 65, CIT Nagar 1st main road, Chennai, 600 035, INDIA</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-4c90f57 elementor-widget elementor-widget-icontact_info"
                                                        data-id="4c90f57"
                                                        data-element_type="widget"
                                                        data-widget_type="icontact_info.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="contact-info box-style1">
                                                                <i class="flaticon-envelope"></i>
                                                                <div class="info-text">
                                                                    <h6>Our Mailbox:</h6>
                                                                    <p>contact@tecdata.net</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-a59e635 elementor-widget elementor-widget-icontact_info"
                                                        data-id="a59e635"
                                                        data-element_type="widget"
                                                        data-widget_type="icontact_info.default"
                                                    >
                                                        <div class="elementor-widget-container">
                                                            <div class="contact-info box-style1">
                                                                <i class="flaticon-phone-1"></i>
                                                                <div class="info-text">
                                                                    <h6>Our Phone:</h6>
                                                                    <p>+91 93846 65527</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-add1e18 ot-flex-column-vertical" data-id="add1e18" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-5c60825 elementor-widget elementor-widget-shortcode" data-id="5c60825" data-element_type="widget" data-widget_type="shortcode.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-shortcode">
                                                                <div role="form" class="wpcf7" id="wpcf7-f1379-p1370-o1" lang="en-US" dir="ltr">
                                                                    <div class="screen-reader-response">
                                                                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                                        <ul></ul>
                                                                    </div>
                                                                    <form action="http://wpdemo.archiwp.com/engitech/contacts/#wpcf7-f1379-p1370-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
                                                                        <div style="display: none;">
                                                                            <input type="hidden" name="_wpcf7" value="1379" />
                                                                            <input type="hidden" name="_wpcf7_version" value="5.3" />
                                                                            <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1379-p1370-o1" />
                                                                            <input type="hidden" name="_wpcf7_container_post" value="1370" />
                                                                            <input type="hidden" name="_wpcf7_posted_data_hash" value="" />
                                                                        </div>
                                                                        <div class="main-form">
                                                                            <h2>Ready to Get Started?</h2>
                                                                            <p class="font14">Your email address will not be published. Required fields are marked *</p>
                                                                            <p>
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                    <input
                                                                                        type="text"
                                                                                        name="your-name"
                                                                                        value=""
                                                                                        size="40"
                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                        aria-required="true"
                                                                                        aria-invalid="false"
                                                                                        placeholder="Your Name *"
                                                                                    />
                                                                                </span>
                                                                            </p>
                                                                            <p>
                                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                                    <input
                                                                                        type="email"
                                                                                        name="your-email"
                                                                                        value=""
                                                                                        size="40"
                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                        aria-required="true"
                                                                                        aria-invalid="false"
                                                                                        placeholder="Your Email *"
                                                                                    />
                                                                                </span>
                                                                            </p>
                                                                            <p>
                                                                                <span class="wpcf7-form-control-wrap your-message">
                                                                                    <textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message..."></textarea>
                                                                                </span>
                                                                            </p>
                                                                            <p><button type="submit" class="octf-btn octf-btn-light">Send Message</button></p>
                                                                        </div>

                                                                        <div class="wpcf7-response-output" aria-hidden="true"></div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-aba0bc8 elementor-section-full_width ot-traditional elementor-section-height-default elementor-section-height-default"
                                data-id="aba0bc8"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-aa84c84 ot-flex-column-vertical" data-id="aa84c84" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-1952363 elementor-widget elementor-widget-google_maps" data-id="1952363" data-element_type="widget" data-widget_type="google_maps.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-custom-embed">
                                                                <iframe
                                                                    frameborder="0"
                                                                    scrolling="no"
                                                                    marginheight="0"
                                                                    marginwidth="0"
                                                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.1341601919903!2d80.2292449509365!3d13.027127417135807!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5267076beed485%3A0x3bdd955cf2eb3cdd!2sVizza%20Insurance%20Broking%20Services%20Pvt.Ltd.!5e0!3m2!1sen!2sin!4v1606909928420!5m2!1sen!2sin"
                                                                    title="New #: 65, CIT Nagar 1st main road, Chennai, 600 035, INDIA"
                                                                    aria-label="New #: 65, CIT Nagar 1st main road, Chennai, 600 035, INDIA"
                                                                ></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #content -->
            @include('layouts.footer')
        </div>
        <!-- #page -->

        <a id="back-to-top" href="#" class="show"><i class="flaticon-up-arrow"></i></a>

        <script type="text/javascript" id="wc-add-to-cart-js-extra">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "View cart",
                cart_url: "http:\/\/wpdemo.archiwp.com\/engitech\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="woocommerce-js-extra">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/engitech\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>

        <script type="text/javascript" id="wc-cart-fragments-js-extra">
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                ajax_url: "\/engitech\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/engitech\/?wc-ajax=%%endpoint%%",
                cart_hash_key: "wc_cart_hash_c8d57f1cd4add6020c9933c2e3c64aa9",
                fragment_name: "wc_fragments_c8d57f1cd4add6020c9933c2e3c64aa9",
                request_timeout: "5000",
            };
            /* ]]> */
        </script>

        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                version: "3.0.13",
                is_static: false,
                legacyMode: { elementWrappers: true },
                urls: { assets: "http:\/\/wpdemo.archiwp.com\/engitech\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                },
                post: { id: 1370, title: "Contacts%20%E2%80%93%20Engitech", excerpt: "", featuredImage: false },
            };
        </script>

        <script>
            window.lazyLoadOptions = {
                elements_selector: "img[data-lazy-src],.rocket-lazyload",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_loaded: function (element) {
                    if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                        if (element.classList.contains("lazyloaded")) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                },
            };
            window.addEventListener(
                "LazyLoad::Initialized",
                function (e) {
                    var lazyLoadInstance = e.detail.instance;
                    if (window.MutationObserver) {
                        var observer = new MutationObserver(function (mutations) {
                            var image_count = 0;
                            var iframe_count = 0;
                            var rocketlazy_count = 0;
                            mutations.forEach(function (mutation) {
                                for (i = 0; i < mutation.addedNodes.length; i++) {
                                    if (typeof mutation.addedNodes[i].getElementsByTagName !== "function") {
                                        return;
                                    }
                                    if (typeof mutation.addedNodes[i].getElementsByClassName !== "function") {
                                        return;
                                    }
                                    images = mutation.addedNodes[i].getElementsByTagName("img");
                                    is_image = mutation.addedNodes[i].tagName == "IMG";
                                    iframes = mutation.addedNodes[i].getElementsByTagName("iframe");
                                    is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                                    rocket_lazy = mutation.addedNodes[i].getElementsByClassName("rocket-lazyload");
                                    image_count += images.length;
                                    iframe_count += iframes.length;
                                    rocketlazy_count += rocket_lazy.length;
                                    if (is_image) {
                                        image_count += 1;
                                    }
                                    if (is_iframe) {
                                        iframe_count += 1;
                                    }
                                }
                            });
                            if (image_count > 0 || iframe_count > 0 || rocketlazy_count > 0) {
                                lazyLoadInstance.update();
                            }
                        });
                        var b = document.getElementsByTagName("body")[0];
                        var config = { childList: !0, subtree: !0 };
                        observer.observe(b, config);
                    }
                },
                !1
            );
        </script>
        <script data-no-minify="1" async src="../wp-content/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js"></script>
        <script>
            "use strict";
            var wprRemoveCPCSS = function wprRemoveCPCSS() {
                var elem;
                document.querySelector('link[data-rocket-async="style"][rel="preload"]') ? setTimeout(wprRemoveCPCSS, 200) : (elem = document.getElementById("rocket-critical-css")) && "remove" in elem && elem.remove();
            };
            window.addEventListener ? window.addEventListener("load", wprRemoveCPCSS) : window.attachEvent && window.attachEvent("onload", wprRemoveCPCSS);
        </script>
        <script src="../wp-content/cache/min/4/ca52a6bf14680e6b18f41d1eb26c0d24.js" data-minify="1" defer></script>
        <noscript>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100i%2C200%2C200i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CNunito%20Sans%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%20Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin%2Clatin-ext&#038;display=swap"
            />
            <link rel="stylesheet" href="../wp-content/cache/min/4/c06e59bad3bee541f4023f103c37663f.css" media="all" data-minify="1" />
            <link rel="stylesheet" id="woocommerce-smallscreen-css" href="../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen1c9b.css?ver=4.6.1" type="text/css" media="only screen and (max-width: 768px)" />
        </noscript>
    </body>

    <!-- Mirrored from wpdemo.archiwp.com/engitech/contacts/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Dec 2020 05:36:01 GMT -->
</html>

<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1606854605 -->
