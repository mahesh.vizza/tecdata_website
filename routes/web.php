<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */


Route::get('/', 'HomeController@index')->name('home.index');

Route::get('company/index', 'CompanyController@index');

Route::get('company/about-us', 'CompanyController@aboutus')->name('company.aboutus');

Route::get('company/why-tecdata', 'CompanyController@whytecdata')->name('company.whytecdata');

Route::get('services/index', 'ServicesController@index');

Route::get('projects/index', 'ProjectsController@index');

Route::get('blog/index', 'BlogController@index');

Route::get('contacts', 'ContactsController@index')->name('contacts.index');


